## Assets

### POST /asset
```
Allowed asset-type = car,driver,obdDevice,beacon
```
payload:
```

{
    "accommodation" :String,
    "dateOfJoin" : String,
    "mobileNumber" :String,
    "licenceNo" : String,
    "jdeDepartment" :String,
    "department" :String,
    "gender" :String,
    "driverName" : String,
    "badgeNo" :String,
    "assetType":String
}
or
{
    "uniqueIdentifier" : String,
    "type" : String,
    "make" : String,
    "assetType":String
}
or
{ 
    "vinNumber" :String, 
    "registrationNumber" :String, 
    "fleetNo" :String, 
    "carNo" :String, 
    "make" :String, 
    "modelNo" :String, 
    "dutyType" :String, 
    "assetName" :String,
    "assetType":String
}
or 
{ 
    "vinNumber" :String, 
    "registrationNumber" :String, 
    "fleetNo" :String, 
    "carNo" :String, 
    "make" :String, 
    "modelNo" :String, 
    "dutyType" :String, 
    "assetName" :String
}
```

response:
```
saved asset object

```
### POST /asset/multiple
```
Allowed asset-type = car,driver,obdDevice,beacon
```
payload:
```
[Array Of Assets]
```

response:
```
[saved asset object]

```

### PUT /asset/:id

payload: can be of any Asset Type
```
Asset Object
```

response: update Asset Object
```
updated Asset object
```

### GET /asset/count?type={{asset-type}}
asset-type pass comma separated values
asset-type = car, driver, obdDevice,beacon

response:
```
{
    "count": 6
}
```
### GET /asset/count

asset-type pass comma separated values

response:
```
{
    "count": 6
}
```

### GET /asset?type={{asset-type}}&limit=1&skip=1

asset-type pass comma separated values

Allowed asset-type = car,driver,obdDevice,beacon
```
returns List of Specified Assets by type
```

### GET /asset?limit=1&skip=1

asset-type pass comma separated values
```
return List of Assets irrespective of type
```

### GET /asset/:id

```
return Asset Object
```

### GET /asset/countByType

```
{
    "count": [
        {
            "count": 2,
            "type": "car"
        },
        {
            "count": 2,
            "type": "beacon"
        },
        {
            "count": 1,
            "type": "obdDevice"
        },
        {
            "count": 1,
            "type": "driver"
        }
    ]
}
```


### DELETE /asset/:id

response: deleted Asset object
```
{
    "_id": "5b27bbba0824120d0c1e525b",
    "vinNumber": "test2",
    "registrationNumber": "test",
    "fleetNo": "test",
    "carNo": "test",
    "make": "test",
    "modelNo": "test",
    "dutyType": "test",
    "assetName": "test",
    "assetType": "car",
    "updated": "2018-06-18T14:03:38.461Z",
    "created": "2018-06-18T14:03:38.462Z",
    "__v": 0
}
```

## Beacons

####deprecated
### GET /beacons/nearby

response: get near by beacons details
```
[
    {
        "_id": "5b3093aee7d51339aac584c0",
        "rssi": -55,
        "address": "dc:f7:6e:f4:a9:e1",
        "txPowerLevel": null,
        "localName": "SKYBEACON           ",
        "beaconId": "dcf76ef4a9e1",
        "beaconType": "iBeacon",
        "uuid": "dcf76ef4a9e1",
        "iBeacon": {
            "uuid": "FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
            "major": 1,
            "minor": 1,
            "txPower": 205
        },
        "txPower": 205,
        "updated": "2018-06-25T07:03:10.925Z",
        "created": "2018-06-25T07:03:10.929Z",
        "__v": 0,
        "createdInMilliSeconds": 1529910190929,
        "isRegistered": true or false
    }
]
```

####deprecated
### GET /beacons/isRegistered/{{id}}
id = beaconId required
response: deleted Asset object
```
{
    "isRegistered": true or false
}
```


## Asset Configuration

### GET /assetConfig
response: return all Asset configuration object
```
[Asset Configuration]
```
### GET /assetConfig?type={{assetType}}

response: return  Asset configuration object of type passed

```
{Asset Configuration}
```
### GET /assetConfig/types

response:return all Assets types

```
[
    {
        "label": "Beacon",
        "type": "beacon"
    },
    {
        "label": "Car",
        "type": "car"
    },
    {
        "label": "Obd Device",
        "type": "obdDevice"
    },
    {
        "label": "Driver",
        "type": "driver"
    },
    {
        "label": "RFID Tag",
        "type": "rfIdTag"
    }
]
```
## Asset Group

### GET /assetGroup
response: return all Asset Group object
```
[
    {
        "label": "Fire Extinguisher",
        "type": "fireExtinguisher"
    },
    {
        "label": "Medical Kit",
        "type": "medicalKit"
    },
    {
        "label": "Spare Tyre",
        "type": "spareTyre"
    },
    {
        "label": "Emergency Warning Triangle",
        "type": "emergencyWarningTriangle"
    }
]
```

## Uploading Image For Assets


### GET /uploads/{{assetType}}/{{type}}/{{id}}
    Allowed asset-type = car,driver,obdDevice,beacon..etc..
            id = assetId    *required
            type = profile,regNo,vinNo  *required
    response: deleted Asset object
    
 ```
 {
     "isRegistered": true or false
 }
```

### POST /uploads
id = beaconId required
response: deleted Asset object
payload: can be of any Asset Type
```
{ 
    "assetType" :String, //car,driver,obdDevice
    "type" :String, //profile,regNo,vinNo
    "id" :String //assetId
}
```

## Asset Linking-DeLinking

### POST /assets/link
id = Asset Id
primaryAsset = main asset to which to be linked
assetsToBeLinked = asset to which primary asset to be linked
response: deleted Asset object
payload: can be of any Asset Type

```
{
    "primaryAsset":"5b36779a52e339365226aa85",//primary id asset to be linked
    "assetsToBeLinked":["5b36779a52e339365226aa86"]//array of asset id's to be linked
}
```
### POST /assets/deLink
id = Asset Id
primaryAsset = main asset to which to be linked
assetsToBeLinked = asset to which primary asset to be linked
response: deleted Asset object
payload: can be of any Asset Type
```
{
    "primaryAsset":"5b36779a52e339365226aa85",//primary id asset to be linked
    "assetsToBeDeLinked":["5b36779a52e339365226aa86"]//array of asset id's to be de-linked
}
```

## Asset Group Linking-DeLinking

### POST /assetGroup/linkToAsset
id = Asset Id
primaryAsset = main asset to which to be linked
assetsToBeLinked = asset to which primary asset to be linked
response: deleted Asset object
payload: can be of any Asset Type

```
{
 "assets":["5b36779a52e339365226aa85","5b3668cdba08b12a583f1598"],//array of asset id's 
 "groups":[ "5b34866ab507ca776fba90d7","5b34866ab507ca776fba90d9"]//array of asssetGroup id's to be removed
}
```
### POST /assetGroup/deLinkToAsset
id = Asset Id
primaryAsset = main asset to which to be linked
assetsToBeLinked = asset to which primary asset to be linked
response: deleted Asset object
payload: can be of any Asset Type
```
{
 "assets":["5b3668cdba08b12a583f1597"], //array of asset id's 
 "groups":["5b34866ab507ca776fba90d9","5b34866ab507ca776fba90d7"] //array of asssetGroup id's to be added
}
```

## Asset Driver

### GET /driver/driverId/{{driverId}}

```
driver asset
```


## NEAR By 

### GET /nearBy

response: get near by beacons details
```
    type = rfId,beacon,obd<device
    Optional Query Parameter ?type={{rfId or beacons}}&duration={{in minutes}}&registered={{true or false}}
```

```
[
    {
        "_id": "5b373f1105ec2637d0b26ef2",
        "RFIDId": "5d0101cc5c1abd1ef63b4d0485105234e1340c1e0073",
        "tagName": "tag1",
        "updated": "2018-06-30T08:28:01.846Z",
        "created": "2018-06-30T08:28:01.849Z",
        "__v": 0,
        "createdInMilliSeconds": 1530347281849,
        "isRegistered": true or false
    }
]
```

### GET /nearBy/assetsByRfId

response: get all near by beacons ,rfId details along with Linked assets
```
    type = rfId,beacon,obd<device
    Optional Query Parameter ?type={{rfId or beacons}}&duration={{in minutes}}&registered={{true or false}}
```

```
{
    "beacons": [],//all registered near by beacons
    "rfId": {},//last detected rf tag with in 1 min
    "car": {}//car linked to rf Tag detected
}
```

## TEXT SEARCH

### GET /assets/search/{{searchText}}?type={{asset-type}}&skip=10&limit=10
#### If Type Is Not Passed All types will be selected 
```
example:
/assets/search/ob?type=car,obdDevice&skip=1&limit=10
```
Allowed asset-type = car,driver,obdDevice,beacon
```
[
    asset objects
]

```



### GET /assets/search/count/{{searchText}}?type={{asset-type}}
#### If Type Is Not Passed All types will be selected 
```
example:
/assets/search/ob?type=car,obdDevice&skip=1&limit=10
```
Allowed asset-type = car,driver,obdDevice,beacon
```
{
    "count":0
}

```

## Inspection Bay

### GET /inspectionBay/active

response: Get Active Car At Inspection Bay


```
{
    "beacons": [],//all registered near by beacons
    "rfId": {},//if no rfId detected it will be null
    "car": {}//if no car linked to  rfId it will be null
}
```
### GET /inspectionBay/stop

response: Stops Current Inspection


```
{
    "beacons": [],//all registered near by beacons
    "rfId": {},//if no rfId detected it will be null
    "car": {}//if no car linked to  rfId it will be null
}
```

### GET /inspectionBay/

response: Stops Current Inspection

```
example:
/inspectionBay?skip=1&limit=10
```

```
[{
    "beacons": [],//all registered near by beacons
    "rfId": {},//if no rfId detected it will be null
    "car": {}//if no car linked to  rfId it will be null
}]
```

### GET /inspectionBay/count

response: Stops Current Inspection

```
example:
/inspectionBay?skip=1&limit=10
```

```
[{
    "beacons": [],//all registered near by beacons
    "rfId": {},//if no rfId detected it will be null
    "car": {}//if no car linked to  rfId it will be null
}]
```

### POST /inspectionBay/driverFeedBack

response: Stops Current Inspection

```
example:
/inspectionBay?skip=1&limit=10
```
response
```
[{
    "beacons": [],//all registered near by beacons
    "rfId": {},//if no rfId detected it will be null
    "car": {}//if no car linked to  rfId it will be null,
    "driverFeedBack": {}//driver feed back
}]
```


### POST /inspectionBay/driver/login

response: To Authenticate Driver Details
request body
```
{
    id:"driverId"
}
```
response
```
    driverDetails
```



### POST /inspectionBay/avds

response: To Authenticate Driver Details
request body
```
XML PAYLOAD
```
response
```
 STATUS
```



## Beacon Exception

### GET /beaconException/

response: Get Active Car At Inspection Bay


```
[
    {
        "_id": "5b6811235ff4b54c76bf2fef",
        "beaconId": "f85fd07fd191",
        "updated": "2018-08-06T09:13:07.418Z",
        "__v": 0
    },
    {
        "_id": "5b6811235ff4b54c76bf2ff0",
        "beaconId": "ce451ad22244",
        "updated": "2018-08-06T09:13:07.418Z",
        "__v": 0
    }
]
```

### POST /beaconException/



request body :

```
{"id":["f85fd07fd191","ce451ad22244"]}
```

response: Get All Exception Beacons


```
[
    {
        "_id": "5b6811235ff4b54c76bf2fef",
        "beaconId": "f85fd07fd191",
        "updated": "2018-08-06T09:13:07.418Z",
        "__v": 0
    },
    {
        "_id": "5b6811235ff4b54c76bf2ff0",
        "beaconId": "ce451ad22244",
        "updated": "2018-08-06T09:13:07.418Z",
        "__v": 0
    }
]
```


### DELETE /beaconException/{{beaconId}}



example :

```
 /beaconException/f85fd07fd191,ce451ad22244
```

response: Get All Exception Beacons


```
{
    "ok": 1,
    "n": 2
}
```




### DELETE /beaconException/{{beaconId}}



example :

```
 /beaconException/f85fd07fd191,ce451ad22244
```

response: Get All Exception Beacons


```
{
    "ok": 1,
    "n": 2
}
```
