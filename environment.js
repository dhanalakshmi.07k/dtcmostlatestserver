/**
 * Created by suhas on 27/11/18.
 */
let path = require('path'),
    rootPath = path.normalize(__dirname),
    env = process.env.NODE_ENV || 'dtc_binary';
console.log("Using Environment "+env);
let config = {
    production: {
        root: rootPath,
        app: {
            name: 'IOT-Server'
        },
        port: 4500,
        db: 'mongodb://10.2.12.209:29019/iotZen',
        mongoConn:{
            url:'mongodb://10.2.12.209:29019/iotZen',
            options:{}
        },
        mqtt:{
            broker:'mqtt://localhost'
        },SOCKET:{
            TOPIC:{
                INSPECTION_BAY_UPDATE:"activeInspectionData",
                INSPECTION_BAY_REPORT_UPDATE:"reportUpdate"
            },
            KEEP_ALIVE_TIME:3//in seconds
        },
        asset:{

        },
        BEACON:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            }
        },
        RF_ID:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            },
            INSPECTION_BAY:{
                RETRIEVE_SCANNED_DURATION:1, //in minutes
                TIME_BETWEEN_SAME_RF_ID_TAG_SCAN:2
            }
        },
        imageUploadFolder:path.join(rootPath, 'uploads'),
        inspectionBay:{
            maxTimeForInspection:15, //in mins
            reportsPath:path.join(__dirname+"/../",'..', 'reports'),
            maxTimeForPreviousInspection:15
        },
        logger:{
            basePath:path.join(rootPath, 'logs'),
            level:"error"
        },
        uvis:{
            imageBasePath:path.join(rootPath, 'images/uvis/')
        },
        licence:{
            publicKey:path.join(__dirname, 'serverPublicKey.pem'),
            licenceFile:path.join(__dirname+"/../", 'serverLicence.lic')
        },
        auth:{
            publicKeyPath:path.join(__dirname,"..","app","controllers",'auth_public_key.key'),
        },
        avdds:{
            imageBasePath:path.join(rootPath, 'images/avdds/'),
            host:"51.15.254.32",
            port:"4400",
            allowAccess:true,
            path:"/server/avddsSimulator"
        },
        reportGenMiroService:{
            port:4900,
            host:"localhost",
            END_POINT_PDF_GENERATOR:"generatePdf"
        },
        bulkUploadFileStorage:{
            path:path.join(rootPath, 'fileBulkUpload')
        },
        errorTracker:{
            sentry:{
                enabled:false,
                dsn:'https://26b725c820a74c84b795185a74a4d72c@sentry.io/1377444'
            }
        }
    },
    localToProduction: {
        root: rootPath,
        app: {
            name: 'IOT-Server'
        },
        port: 4500,
        db: 'mongodb://163.172.131.83:29019/iotZen',
        mongoConn:{
            url:'mongodb://163.172.131.83:29019/iotZen',
            options:{}
        },
        mqtt:{
            broker:'mqtt://212.47.249.75'
        },SOCKET:{
            TOPIC:{
                INSPECTION_BAY_UPDATE:"activeInspectionData",
                INSPECTION_BAY_REPORT_UPDATE:"reportUpdate"
            },
            KEEP_ALIVE_TIME:3//in seconds
        },
        asset:{

        },
        BEACON:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            }
        },
        RF_ID:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            },
            INSPECTION_BAY:{
                RETRIEVE_SCANNED_DURATION:1, //in minutes
                TIME_BETWEEN_SAME_RF_ID_TAG_SCAN:2
            }
        },
        imageUploadFolder:path.join(rootPath, 'uploads'),
        inspectionBay:{
            maxTimeForInspection:15, //in mins
            reportsPath:path.join(__dirname,'..', 'reports'),
            maxTimeForPreviousInspection:15
        },
        logger:{
            basePath:path.join(rootPath, 'logs'),
            level:"error"
        },
        uvis:{
            imageBasePath:path.join(rootPath, 'images/uvis/')
        },
        licence:{
            publicKey:path.join(__dirname, 'serverPublicKey.pem'),
            licenceFile:path.join(__dirname,'serverLicence.lic')
        },
        auth:{
            publicKeyPath:path.join(__dirname,"..","app","controllers",'auth_public_key.key'),
        },
        avdds:{
            imageBasePath:path.join(rootPath, 'images/avdds/'),
            host:"51.15.254.32",
            port:"4400",
            allowAccess:true,
            path:"/server/avddsSimulator"
        },
        reportGenMiroService:{
            port:4900,
            host:"localhost",
            END_POINT_PDF_GENERATOR:"generatePdf"
        },
        bulkUploadFileStorage:{
            path:path.join(rootPath, 'fileBulkUpload')
        },
        errorTracker:{
            sentry:{
                enabled:false,
                dsn:'https://d4be914bd4734e159a6b20e387111ff5@sentry.io/1377464'
            }
        }
    },
    production_binary: {
        root: path.dirname(process.execPath),
        app: {
            name: 'IOT-Server'
        },
        port: 4500,
        db: 'mongodb://10.2.12.209:29019/iotZen',
        mongoConn:{
            url:'mongodb://10.2.12.209:29019/iotZen',
            options:{}
        },
        mqtt:{
            broker:'mqtt://212.47.249.75'
        },SOCKET:{
            TOPIC:{
                INSPECTION_BAY_UPDATE:"activeInspectionData",
                INSPECTION_BAY_REPORT_UPDATE:"reportUpdate"
            },
            KEEP_ALIVE_TIME:3//in seconds
        },
        asset:{

        },
        BEACON:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            }
        },
        RF_ID:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            },
            INSPECTION_BAY:{
                RETRIEVE_SCANNED_DURATION:1, //in minutes
                TIME_BETWEEN_SAME_RF_ID_TAG_SCAN:2
            }
        },
        imageUploadFolder:path.join(path.dirname(process.execPath), 'uploads'),
        inspectionBay:{
            maxTimeForInspection:15,
            reportsPath:path.join(path.dirname(process.execPath),'..', 'reports'),
            maxTimeForPreviousInspection:15
        },
        logger:{
            basePath:path.join(path.dirname(process.execPath), 'logs'),
            level:"error"
        },
        uvis:{
            imageBasePath:path.join(path.dirname(process.execPath), 'images/uvis/')
        },
        licence:{
            publicKey:"./serverPublicKey.pem",
            licenceFile:path.join(path.dirname(process.execPath), 'serverLicence.lic')
        },
        auth:{
            publicKeyPath:path.join(path.dirname(process.execPath), 'auth_public_key.key'),
        },
        avdds:{
            imageBasePath:path.join(path.dirname(process.execPath), 'images/avdds/'),
            host:"localhost",
            port:"4400",
            allowAccess:true,
            path:"/server/avddsSimulator"
        },
        reportGenMiroService:{
            port:4900,
            host:"localhost",
            END_POINT_PDF_GENERATOR:"generatePdf"
        },
        bulkUploadFileStorage:{
            path:path.join(path.dirname(process.execPath),'fileBulkUpload')
        },
        errorTracker:{
            sentry:{
                enabled:true,
                dsn:'https://d4be914bd4734e159a6b20e387111ff5@sentry.io/1377464'
            }
        }
    },
    dtc: {
        root: rootPath,
        app: {
            name: 'IOT-Server'
        },
        port: 4500,
        mongoConn:{
            url:'mongodb://192.168.50.100:37017/iotZen',
            options:{
                auth:{
                    user: "iotZenDbAdmin",
                    password: "iotZenDbAdminForSure"
                },
                authSource:"admin"
            }
        },
        mqtt:{
            broker:'mqtt://192.168.0.165'
        },SOCKET:{
            TOPIC:{
                INSPECTION_BAY_UPDATE:"activeInspectionData",
                INSPECTION_BAY_REPORT_UPDATE:"reportUpdate"
            },
            KEEP_ALIVE_TIME:3//in seconds
        },
        asset:{

        },
        BEACON:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            }
        },
        RF_ID:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            },
            INSPECTION_BAY:{
                RETRIEVE_SCANNED_DURATION:1, //in minutes
                TIME_BETWEEN_SAME_RF_ID_TAG_SCAN:2
            }
        },
        imageUploadFolder:path.join(rootPath, 'uploads'),
        inspectionBay:{
            maxTimeForInspection:15, //in mins
            reportsPath:path.join(__dirname,'..', 'reports'),
            maxTimeForPreviousInspection:15
        },
        logger:{
            basePath:path.join(rootPath, 'logs'),
            level:"error"
        },
        uvis:{
            imageBasePath:path.join(rootPath, 'images/uvis/')
        },
        licence:{
            publicKey:path.join(__dirname+"/../", 'serverPublicKey.pem'),
            licenceFile:path.join(__dirname+"/../", 'serverLicence.lic')
        },
        auth:{
            publicKeyPath:path.join(__dirname, 'auth_public_key.key'),
        },
        avdds:{
            imageBasePath:path.join(path.dirname(process.execPath), 'images/avdds/'),
            host:"192.168.0.198",/*19890:192.168.0.198:8090*/
            port:"8090",
            allowAccess:true,
            path:"/"
        },
        bulkUploadFileStorage:{
            path:path.join(rootPath, 'fileBulkUpload')
        },
        errorTracker:{
            sentry:{
                enabled:false,
                dsn:'https://ae3273b7b8564939a624e92ff8ebb9c8@sentry.io/1377780'
            }
        }
    },
    dtc_binary: {
        root: path.dirname(process.execPath),
        app: {
            name: 'IOT-Server'
        },
        port: 4500,
        mongoConn:{
            url:'mongodb://localhost:37017/iotZen',
            options:{
                auth:{
                    user: "iotZenDbAdmin",
                    password: "iotZenDbAdminForSure"
                },
                authSource:"admin"
            }
        },
        mqtt:{
            broker:'mqtt://localhost'
        },SOCKET:{
            TOPIC:{
                INSPECTION_BAY_UPDATE:"activeInspectionData",
                INSPECTION_BAY_REPORT_UPDATE:"reportUpdate"
            },
            KEEP_ALIVE_TIME:3//in seconds
        },
        asset:{

        },
        BEACON:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            }
        },
        RF_ID:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            },
            INSPECTION_BAY:{
                RETRIEVE_SCANNED_DURATION:1, //in minutes
                TIME_BETWEEN_SAME_RF_ID_TAG_SCAN:2
            }
        },
        imageUploadFolder:path.join(path.dirname(process.execPath), 'uploads'),
        inspectionBay:{
            maxTimeForInspection:1.2, //in mins
            reportsPath:path.join(path.dirname(process.execPath), 'reports'),
            maxTimeForPreviousInspection:15
        },
        logger:{
            basePath:path.join(path.dirname(process.execPath), 'logs'),
            level:"error"
        },
        uvis:{
            imageBasePath:path.join(path.dirname(process.execPath), 'images/uvis/')
        },
        licence:{
            publicKey:path.join(path.dirname(process.execPath), 'serverPublicKey.pem'),
            licenceFile:path.join(path.dirname(process.execPath), 'serverLicence.lic')
        },
        auth:{
            publicKeyPath:path.join(path.dirname(process.execPath), 'auth_public_key.key'),
        },
        avdds:{
            imageBasePath:path.join(path.dirname(process.execPath), 'images/avdds/'),
            host:"192.168.50.51",/*19890:192.168.0.198:8090*/
            port:"8090",
            allowAccess:true,
            path:"/"
        },
        reportGenMiroService:{
            port:4900,
            host:"192.168.50.100",
            END_POINT_PDF_GENERATOR:"generatePdf"
        },
        bulkUploadFileStorage:{
            path:path.join(path.dirname(process.execPath),'fileBulkUpload')
        },
        errorTracker:{
            sentry:{
                enabled:true,
                dsn:'https://ae3273b7b8564939a624e92ff8ebb9c8@sentry.io/1377780'
            }
        }
    },
    local: {
        root: rootPath,
        app: {
            name: 'IOT-Server'
        },
        port: 4500,
        db: 'mongodb://localhost:27017/iotZen',
        mongoConn:{
            url:'mongodb://localhost:27017/iotZen',
            options:{}
        },
        mqtt:{
            broker:'mqtt://localhost'
        },SOCKET:{
            TOPIC:{
                INSPECTION_BAY_UPDATE:"activeInspectionData",
                INSPECTION_BAY_REPORT_UPDATE:"reportUpdate"
            },
            KEEP_ALIVE_TIME:3//in seconds
        },
        asset:{

        },
        BEACON:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            }
        },
        RF_ID:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            },
            INSPECTION_BAY:{
                RETRIEVE_SCANNED_DURATION:1, //in minutes
                TIME_BETWEEN_SAME_RF_ID_TAG_SCAN:2
            }
        },
        imageUploadFolder:path.join(rootPath, 'uploads'),
        inspectionBay:{
            maxTimeForInspection:15, //in mins
            reportsPath:path.join(__dirname,'..', 'reports'),
            maxTimeForPreviousInspection:15
        },
        logger:{
            basePath:path.join(rootPath, 'logs'),
            level:"error"
        },
        uvis:{
            imageBasePath:path.join(rootPath, 'images/uvis/')
        },
        licence:{
            licenceFile:path.join(__dirname, 'serverLicence.lic')
        },
        auth:{
            publicKeyPath:path.join(__dirname,"..","app","controllers",'auth_public_key.key'),
        },
        avdds:{
            imageBasePath:path.join(rootPath, 'images/avdds/'),
            host:"localhost",/*19890:192.168.0.198:8090*/
            port:"19890",
            allowAccess:true,
            path:"/"
        },
        reportGenMiroService:{
            port:4900,
            host:"localhost",
            END_POINT_PDF_GENERATOR:"generatePdf"
        },
        bulkUploadFileStorage:{
            path:path.join(rootPath, 'fileBulkUpload')
        },
        errorTracker:{
            sentry:{
                enabled:false,
                dsn:'https://d4be914bd4734e159a6b20e387111ff5@sentry.io/1377464'
            }
        }
    },
    local_production_binary: {
        root: path.dirname(process.execPath),
        app: {
            name: 'IOT-Server'
        },
        port: 4500,
        db: 'mongodb://localhost:27017/iotZen',
        mongoConn:{
            url:'mongodb://localhost:27017/iotZen',
            options:{}
        },
        mqtt:{
            broker:'mqtt://212.47.249.75'
        },SOCKET:{
            TOPIC:{
                INSPECTION_BAY_UPDATE:"activeInspectionData",
                INSPECTION_BAY_REPORT_UPDATE:"reportUpdate"
            },
            KEEP_ALIVE_TIME:3//in seconds
        },
        BEACON:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            }
        },
        RF_ID:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            },
            INSPECTION_BAY:{
                RETRIEVE_SCANNED_DURATION:1, //in minutes
                TIME_BETWEEN_SAME_RF_ID_TAG_SCAN:2
            }
        },
        imageUploadFolder:path.join(path.dirname(process.execPath), 'uploads'),
        inspectionBay:{
            maxTimeForInspection:15,
            reportsPath:path.join(path.dirname(process.execPath),'..','..', 'reports'),
            maxTimeForPreviousInspection:15
        },
        logger:{
            basePath:path.join(path.dirname(process.execPath), 'logs'),
            level:"error"
        },
        uvis:{
            imageBasePath:path.join(path.dirname(process.execPath), 'images/uvis/')
        },
        licence:{
            publicKey:"./serverPublicKey.pem",
            licenceFile:path.join(path.dirname(process.execPath), 'serverLicence.lic')
        },
        auth:{
            publicKeyPath:path.join(path.dirname(process.execPath), 'auth_public_key.key'),
        },
        avdds:{
            imageBasePath:path.join(path.dirname(process.execPath), 'images/avdds/'),
            host:"192.168.0.198",
            port:"8090",
            allowAccess:true,
            path:"/"
        },
        reportGenMiroService:{
            port:4900,
            host:"localhost",
            END_POINT_PDF_GENERATOR:"generatePdf"
        },
        bulkUploadFileStorage:{
            path:path.join(path.dirname(process.execPath),'fileBulkUpload')
        },
        errorTracker:{
            sentry:{
                enabled:false,
                dsn:'https://d4be914bd4734e159a6b20e387111ff5@sentry.io/1377464'
            }
        }
    },
    localTodtc: {
        root: rootPath,
        app: {
            name: 'IOT-Server'
        },
        port: 4500,
        mongoConn:{
            url:'mongodb://192.168.255.100:37017/iotZen',
            options:{
                auth:{
                    user: "iotZenDbAdmin",
                    password: "iotZenDbAdminForSure"
                },
                authSource:"admin"
            }
        },
        mqtt:{
            broker:'mqtt://192.168.0.165'
        },SOCKET:{
            TOPIC:{
                INSPECTION_BAY_UPDATE:"activeInspectionData",
                INSPECTION_BAY_REPORT_UPDATE:"reportUpdate"
            },
            KEEP_ALIVE_TIME:3//in seconds
        },
        asset:{

        },
        BEACON:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            }
        },
        RF_ID:{
            LOG:{
                RETRIEVE_SCANNED_DURATION:1 //in minutes
            },
            INSPECTION_BAY:{
                RETRIEVE_SCANNED_DURATION:1, //in minutes
                TIME_BETWEEN_SAME_RF_ID_TAG_SCAN:2
            }
        },
        imageUploadFolder:path.join(rootPath, 'uploads'),
        inspectionBay:{
            maxTimeForInspection:15, //in mins
            reportsPath:path.join(__dirname,'..', 'reports'),
            maxTimeForPreviousInspection:15
        },
        logger:{
            basePath:path.join(rootPath, 'logs'),
            level:"error"
        },
        uvis:{
            imageBasePath:path.join(rootPath, 'images/uvis/')
        },
        licence:{
            publicKey:path.join(__dirname+"/../", 'serverPublicKey.pem'),
            licenceFile:path.join(__dirname+"/../", 'serverLicence.lic')
        },
        auth:{
            publicKeyPath:path.join(__dirname, 'auth_public_key.key'),
        },
        avdds:{
            imageBasePath:path.join(path.dirname(process.execPath), 'images/avdds/'),
            host:"192.168.0.198",/*19890:192.168.0.198:8090*/
            port:"8090",
            allowAccess:true,
            path:"/"
        },
        bulkUploadFileStorage:{
            path:path.join(rootPath, 'fileBulkUpload')
        },
        errorTracker:{
            sentry:{
                enabled:false,
                dsn:'https://ae3273b7b8564939a624e92ff8ebb9c8@sentry.io/1377780'
            }
        }
    },
};

module.exports = {
    getEnvironment:function(env){
        return config[env]
    }
};
