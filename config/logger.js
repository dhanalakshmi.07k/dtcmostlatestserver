/**
 * Created by suhas on 18/8/18.
 */
let winston = require('winston');
let path = require('path');
let config = require('./config');

// Set this to whatever, by default the path of the script.
let logPath = config.logger.basePath;
const loggerConfig = {
    levels: {
        error: 0,
        debug: 1,
        warn: 2,
        data: 3,
        info: 4,
        verbose: 5,
        silly: 6,
        custom: 7
    },
    colors: {
        error: 'red',
        debug: 'blue',
        warn: 'yellow',
        data: 'grey',
        info: 'green',
        verbose: 'cyan',
        silly: 'magenta',
        custom: 'yellow'
    }
};

winston.addColors(loggerConfig.colors);
let twoDigit = '2-digit';
let options = {
    day: twoDigit,
    month: twoDigit,
    year: twoDigit,
    hour: twoDigit,
    minute: twoDigit,
    second: twoDigit
};

const logger = winston.createLogger({
    levels: loggerConfig.levels,
    format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
    transports: [
        new winston.transports.Console({level: 'error',json: false}),
        new(require('winston-daily-rotate-file'))({filename: `logger_combined.json`,prepend: true,json: false,level: 'info',dirname:logPath+"/logger/",maxSize: '500mb'})
    ],exitOnError:false,level:"info"});

const assets = winston.createLogger({
    format: winston.format.combine(winston.format.timestamp(), winston.format.simple()),
    transports: [
        new winston.transports.Console({level: 'error',json: false}),
        new(require('winston-daily-rotate-file'))({filename: `assets_combined.json`,prepend: true,json: false,level: 'info',dirname:logPath+"/asset/",
        })
    ],exitOnError:false,level:"info"});

const inspection= winston.createLogger({
    format: winston.format.combine(winston.format.timestamp(), winston.format.simple()),
    transports: [
        new winston.transports.Console({level: 'error',json: false}),
        new(require('winston-daily-rotate-file'))({filename: `inspection_combined.json`,prepend: true,json: false,level: 'info',dirname:logPath+"/inspectionBay/",
        })
    ],exitOnError:false,level:"info"});

const nearBy= winston.createLogger({
    format: winston.format.combine(winston.format.timestamp(), winston.format.simple()),
    transports: [
        new winston.transports.Console({level: 'error',json: false}),
        new(require('winston-daily-rotate-file'))({filename: `nearBy_combined.json`,prepend: true,json: false,level: 'info',dirname:logPath+"/nearBy/",maxSize: '1500mb'})
    ],exitOnError:false,level:"info"});

const avdds= winston.createLogger({
    format: winston.format.combine(winston.format.timestamp(), winston.format.simple()),
    transports: [
        new winston.transports.Console({level: 'error',json: false}),
        new(require('winston-daily-rotate-file'))({filename: `avdds_combined.json`,
            prepend: true,json: false,level: 'info',dirname:logPath+"/avdds/",maxSize: '35mb'})
    ],exitOnError:false,level:"info"});

const uvis= winston.createLogger({
    format: winston.format.combine(winston.format.timestamp(), winston.format.simple()),
    transports: [
        new winston.transports.Console({level: 'error',json: false}),
        new(require('winston-daily-rotate-file'))({filename: `uvis_combined.json`,
            prepend: true,json: false,level: 'info',dirname:logPath+"/uvis/",maxSize: '35mb'})
    ],exitOnError:false,level:"info"});

const obd= winston.createLogger({
    format: winston.format.combine(winston.format.timestamp(), winston.format.simple()),
    transports: [
        new winston.transports.Console({level: 'error',json: false}),
        new(require('winston-daily-rotate-file'))({filename: `obd_combined.json`,
            prepend: true,json: false,level: 'info',dirname:logPath+"/obd/"})
    ],exitOnError:false,level:"info"});

module.exports = {
    logger: logger,
    assets:assets,
    inspection:inspection,
    nearBy:nearBy,
    avdds:avdds,
    uvis:uvis,
    obd:obd
};