/**
 * Created by suhas on 18/8/18.
 */
let express = require('express');
const url = require('url');
let baseSecurityURL="/api",
    bodyParser = require('body-parser'),
    fs = require('fs'),
    path = require("path"),
    jwt= require('jsonwebtoken'),
    config=require("../config/config"),
    cert = fs.readFileSync(path.join(__dirname,"./server.crt")),
    cookieParser = require('cookie-parser'),
    createError = require('http-errors'),
    cors = require('cors'),
    indexRouter = require('../app/routes/index'),
    assetsRouter = require('../app/routes/asset'),
    authenticationRoute = require('../app/routes/authentication'),
    carRouter = require('../app/routes/car'),
    driverRouter = require('../app/routes/driver'),
    obdDeviceRouter = require('../app/routes/obdDevice'),
    beaconRouter = require('../app/routes/beacon'),
    rfIdRouter = require('../app/routes/rfId'),
    uploadRouter = require('../app/routes/upload'),
    assetGroupRouter = require('../app/routes/assetGroup'),
    assetConfigRouter = require('../app/routes/assetConfig'),
    assetLinkConfigRouter = require('../app/routes/assetsLink'),
    assetDeLinkConfigRouter = require('../app/routes/assetsDeLink'),
    nearByRouters = require('../app/routes/nearBy'),
    assetSearchRouter = require('../app/routes/assetSearch'),
    inspectionBayRouter = require('../app/routes/inspectionBay'),
    beaconExceptionRouter = require('../app/routes/beaconExceptionList'),
    uvisRouter = require('../app/routes/uvis'),
    avddsRouter = require('../app/routes/avdds'),
    licenceHandlerRouter = require('../app/routes/licenceHandler'),
    microservicesRouter = require('../app/routes/microServices.js'),
    serviceAssetRouter = require('../app/routes/serviceAsset.js'),
    serviceAssetConfigRouter = require('../app/routes/serviceAssetConfig.js'),
    assetBulkUploadRouter = require('../app/routes/assetBulkUpload.js'),
    gpsDataRouter = require('../app/routes/gpsData'),
    geoFenceRouter = require('../app/routes/geoFence'),
     rules = require('../app/auth/rules'),
    proxy = require('express-http-proxy');
let userDetailsRouter = require('../app/routes/userDetails.js');
const Sentry = require('@sentry/node');
module.exports = function(app) {

    const corsOpt = {
        origin: process.env.CORS_ALLOW_ORIGIN || '*', // this work well to configure origin url in the server
        methods: ['GET', 'PUT', 'POST', 'DELETE','PATCH'], // to works well with web app, OPTIONS is required
        allowedHeaders: ['Content-Type', 'Authorization'] // allow json and token in the headers
    };


    app.use(function(req, res, next) {
//set headers to allow cross origin request.
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.use(cors(corsOpt)); // cors for all the routes of the application
    app.options('*', cors(corsOpt));


    app.use(function(req, res, next) {
        if (req.url.includes("/authentication"))
        {
            next();
        }else if(req.url.includes("/inspectionBay")){
            next();
        }
        else if(req.url.includes("test")){
            next();
        }
        else if(req.url.includes("/uvis")){
            next();
        }
        else if(req.url.includes("/avdds")){
            next();
        }
        else if(req.url.includes("/licence")){
            next();
        }
        else if(req.url.includes("/microServices")){
            next();
        }
        else if(req.url.includes("/serviceAssets")){
            next();
        }
        else if(req.url.includes("/serviceAssetsConfig")){
            next();
        }
        else if(req.url.includes("/assetBulkUpload/sample")){
            next();
        }
        else if(req.url.includes("/generateError")){
            next();
        }
        else{
            jwt.verify(req.headers['authorization'], cert, { algorithms: ['RS256'] },function(err, decoded) {
                if(decoded && decoded.userDetails){
                   // console.log(decoded)
                    rules(req, res,decoded.userDetails, next)
                }
                if(err){
                    res.send({response:401});
                }
            });
        }
    });


    // view engine setup
    app.set('views', path.join(path.dirname(process.execPath), 'views'));
    app.set('view engine', 'jade');

    app.use(cors());
    //app.use(logger('dev'));
    app.use(express.json({ limit: '150mb', extended: false }));
    app.use(express.urlencoded({ limit: '150mb', extended: false }));
    app.use(cookieParser());

    app.use(bodyParser.raw({type:'application/xml',limit: "50mb", extended: true, parameterLimit:50000}));

    app.use(bodyParser.json({limit: "50mb"}));
    app.use('/test',indexRouter);
   /* let webPath = __dirname+"/../../web/dist";
    console.log(webPath);
    app.use('/web',express.static(webPath));*/
    app.use('/authentication',authenticationRoute);
    app.use(baseSecurityURL+'/users',authenticationRoute);
    app.use(baseSecurityURL+'/',indexRouter);
    //app.use('/asset',assetsRouter);
    app.use(baseSecurityURL+'/asset',assetsRouter);
    app.use(baseSecurityURL+'/car',carRouter);
    app.use(baseSecurityURL+'/rfId',rfIdRouter);
    app.use(baseSecurityURL+'/driver',driverRouter);
    app.use(baseSecurityURL+'/beacon',beaconRouter);
    app.use(baseSecurityURL+'/obd',obdDeviceRouter);
    app.use(baseSecurityURL+'/uploads',uploadRouter);
    app.use(baseSecurityURL+'/assetGroup',assetGroupRouter);
    app.use(baseSecurityURL+'/assetConfig',assetConfigRouter);
    app.use(baseSecurityURL+'/assets/link',assetLinkConfigRouter);
    app.use(baseSecurityURL+'/assets/deLink',assetDeLinkConfigRouter);
    app.use(baseSecurityURL+'/nearBy',nearByRouters);
    //app.use('/assets/search',assetSearchRouter);
    app.use(baseSecurityURL+'/assets/search',assetSearchRouter);
    //app.use('/assets/search',assetSearchRouter);
    app.use(baseSecurityURL+'/inspectionBay',inspectionBayRouter);
    const apiProxyUvis = proxy('http://localhost:4550/inspectionBay/uvis', {
        forwardPath: req => url.parse(req.baseUrl).path
    });
    const apiProxyAvdds = proxy('http://localhost:4550/inspectionBay/avdds', {
        forwardPath: req => url.parse(req.baseUrl).path
    });
    app.use('/inspectionBay/uvis', apiProxyUvis);
    app.use('/inspectionBay/avdds', apiProxyAvdds);
    //app.use('/inspectionBay/avdds', proxy('http://localhost:4550/inspectionBay/avdds'));


    app.use(baseSecurityURL+'/beaconException',beaconExceptionRouter);
    app.use(baseSecurityURL+'/uvis',uvisRouter);
    app.use('/uvis',uvisRouter);
    app.use(baseSecurityURL+'/avdds',avddsRouter);
    app.use('/avdds',avddsRouter);
    app.use('/licence',licenceHandlerRouter);


    app.use('/inspectionBay',inspectionBayRouter);
    app.use('/microServices',microservicesRouter);
   // app.use('/serviceAssets',serviceAssetRouter);
    app.use(baseSecurityURL+'/serviceAssets',serviceAssetRouter);
    app.use('/serviceAssets',serviceAssetRouter);
    app.use('/serviceAssetsConfig',serviceAssetConfigRouter);
    app.use(baseSecurityURL+'/serviceAssetsConfig',serviceAssetConfigRouter);
    app.use(baseSecurityURL+'/assetBulkUpload',assetBulkUploadRouter);
    app.use(baseSecurityURL+'/user',userDetailsRouter);
    app.use(baseSecurityURL+'/gpsData',gpsDataRouter);
    app.use(baseSecurityURL+'/geoFence',geoFenceRouter);
    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
        next(createError(404));
    });
    if(config.errorTracker && config.errorTracker.sentry && config.errorTracker.sentry.enabled){
        Sentry.init({ dsn: config.errorTracker.sentry.dsn});
        app.use(Sentry.Handlers.requestHandler());
        app.use(Sentry.Handlers.errorHandler());
    }
    // error handler
    app.use(function(err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        //res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        res.status(err.status || 500);
        res.send(err.message);
    });
    // Optional fallthrough error handler
  /*  app.use(function onError(err, req, res, next) {
        // The error id is attached to `res.sentry` to be returned
        // and optionally displayed to the user for support.
        res.locals.message = err.message;

        res.status(err.status || 500);
        res.end(res.sentry + '\n');
    });*/
};
