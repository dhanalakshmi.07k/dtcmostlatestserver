/**
 * Created by suhas on 18/6/18.
 */
let ASSETS_TYPES ={
    "BEACON":"beacon",
    "DRIVER":"driver",
    "CAR":"car",
    "OBD_DEVICE":"obdDevice",
    "RF_ID":"rfId",
    "BEACON_GATEWAY":"beaconGateway",
    "RF_ID_GATEWAY":"rfidGateway",
    "SERVER":"server"
};
let PUBLISHER={
    MQTT:{
        TOPIC:{
            SERVICE_ADDED:"servicesAltered"
        }
    },
    SOCKET:{
        HEALTH_MANAGEMENT_RESPONSE:"healthManagementResponse",
        INSPECTION_BAY_UPDATE:"activeInspectionData",
        INSPECTION_BAY_REPORT_UPDATE:"reportUpdate",
        OBD_DEVICE_GPS:'obdDeviceGpsPush'
    }
};
let INSPECTION_BAY ={
    STATUS:{
        PASS:"pass",
        FAIL:"fail",
        CHECK:"check"
    },
    NUMBER_OF_EQUIPMENTS_REQUIRED:5
};
let SUBSCRIBER={
    MQTT:{
        TOPIC:{
            BEACON:'beaconPresence',
            RF_ID_TAG:'rfIdPresence',
            OBD_DEVICE:'obdDeviceGateWay',//{"rpm": 178, "vin": "1G1JC5444R7252367", "vin_ba": "1G1JC5444R7252367"}
            VSR_PRINTER_DEVICE:'vsrPrinterReport',
            INSPECTION_START:"inspectionStarted",
            HEALTH_CHECK:"healthCheck",
            HEALTH_CHECK_MANAGE:"healthCheckManage",
            DRIVER_FEEDBACK:'driverFeedbackListener',
            AVDDS:'avddsNotifier',
            UVIS:'uvisNotifier',
            OBD_DEVICE_GPS:'obdDeviceGpsPush',
            FREEMATRICS_OBD_DEVICE:"obd_device_freematics"
        }
    }
};
let ENGINE_DIAGNOSTICS = {
    STATUS:{
        FAIL:"fail",
        PASS:'pass',
        NOT_FOUND:"notFound"
    }
};
let AUTHENTICATION={
    PARAMETERS:{
        SESSION_EXPIRE_TIME:'24h' //in seconds
    },ROLES:{
        SUPER_ADMIN:"superadmin",
        ADMIN:"admin",
        OPERATOR:"operator",
        INSPECTION_BAY:"inspectionbay",
    }
};
let TIME_SERIES = {
    INSPECTION_REPORT:["hour","day","month","year","minute"],
    SERVER_STATS_TYPE:["cpu","memory","temperature","disk","all"]
};
let BULK_UPLOAD = {
    driver:{
        SAMPLE_DATA:{
            HEADER:['Driver ID', 'Driver Name','Shift Start','Day Off','Radio Jobs','Shift Type','Terminated'],
            PAYLOAD:['0000000','TESTDRIVER','05:00','','Yes','VIP Tesla','No']
        },
        FIELDS:{
            A: 'driverId', B: 'driverName', C: 'shiftStart',
            D:'dayOff',E: 'radioJobs', F: 'shiftType',
            G: 'terminated'
        }
    },
    car:{
        SAMPLE_DATA:{
            HEADER:['SL', 'Side no','Register No','Type Of Plate'
                ,'Chassis No','Date Of Issue', 'Date Of Expire',
                'Type Of Vehicle','MODEL'],
            PAYLOAD:['0000', 'XXXXX','XXXXX','Taxi','XX1XXXFK0FX58XXX6','03/05/2018', '03/05/2019','CAMRY','2018']
        },
        FIELDS:{
            A: 'SL', B: 'fleetNo', C: 'registrationNumber',
            D:'dutyType',E: 'vinNumber', F: 'dateOfIssue',
            G: 'dateOfExpire', H: 'make',
            I: 'modelNo'
        }
    }
};
let RULES={
    USERS:{
        FIELDS:{
            USER_ROLES:{
                READ:"read_user_role",
                WRITE:"create_user_role"
            }
        }
    }
}

module.exports ={
    ASSETS_TYPES:ASSETS_TYPES,
    SUBSCRIBER:SUBSCRIBER,
    INSPECTION_BAY:INSPECTION_BAY,
    AUTHENTICATION:AUTHENTICATION,
    ENGINE_DIAGNOSTICS:ENGINE_DIAGNOSTICS,
    PUBLISHER:PUBLISHER,
    TIME_SERIES:TIME_SERIES,
    BULK_UPLOAD:BULK_UPLOAD,
    RULES:RULES
};