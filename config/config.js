  let path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'dtc_binary',
  constant=require('./constants');
function getConfig(){
    let pathToEnv = path.join(__dirname,'..', 'environment.js');
    if(env.search("binary")>=0){
        pathToEnv = path.join(path.dirname(process.execPath), 'environment.js');
    }

    module.exports = require(pathToEnv).getEnvironment(env);
};
getConfig();


