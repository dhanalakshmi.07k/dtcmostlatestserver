/**
 * Created by suhas on 28/8/18.
 */

const licenseFile = require('nodejs-license-file');
const config = require('../../config/config');
const logger = require('../../config/logger').logger;
const path = require('path');
let validatorData = null;
const template=[
    '====BEGIN LICENSE====',
    '{{&licenseVersion}}',
    '{{&applicationVersion}}',
    '{{&firstName}}',
    '{{&lastName}}',
    '{{&email}}',
    '{{&expirationDate}}',
    '{{&rfId}}',
    '{{&beacon}}',
    '{{&softWarningEnablerInDays}}',
    '{{&serial}}',
    '{{&messageOverride}}',
    '=====END LICENSE====='
].join('\n');
const defaultsoftWarningEnableByDays = 30;
const licenceDetailsModel = require('../models/licenceDetails');
function setLicenceData(data){
    validatorData = data;
};
 let getLicenceData = function(){
        return getFileData(template);
};
let getFileData = function () {
    return licenseFile.parse({
        publicKeyPath: path.join(__dirname, './serverPublicKey.pem'),
        licenseFilePath: config.licence.licenceFile,
        template
    });
};
let validation = function(){
    return new Promise(function(resolve,reject){
        try {
            let data = getFileData();
            setLicenceData(data);
            isLicenceValid(data)
            .then(function(licenceValid){
                resolve(licenceValid)
            });
        } catch (err) {
            reject(err);
        }
    });

};
function isLicenceValid(licenceData){
    return new Promise(function(resolve,reject){
        let data = {isLicenceValid:false};
        if(licenceData && licenceData.valid){
            if(licenceData.data && licenceData.data.expirationDate){
                let expiryDate = new Date(licenceData.data.expirationDate),
                    currentDate = new Date(),
                    timeDifference = expiryDate.getTime() - currentDate.getTime();
                let differentDays = Math.ceil(timeDifference / (1000 * 3600 * 24));
                if(timeDifference > 0){
                    data.isLicenceValid=true;
                    data.days=differentDays;
                    data.msg="LICENCE FILE VALID FOR "+Math.abs(differentDays)+" DAYS";
                    resolve(data);
                }else{
                    data.isLicenceValid=false;
                    data.days=differentDays;
                    data.msg="LICENCE FILE HAS BEEN EXPIRED SINCE "+Math.abs(differentDays)+" DAYS";
                    resolve(data);
                }
            }else{
                data.isLicenceValid=false;
                data.msg="NO LICENCE FILE FOUND OR LICENCE FILE HAS BEEN TAMPERED";
                resolve(data);
            }
        }else{
            data.isLicenceValid=false;
            data.msg="NO LICENCE FILE FOUND OR LICENCE FILE HAS BEEN TAMPERED";
            resolve(data);
        }

    });
}
let startCheckUp = function(){
    setInterval(function(){
        validation().then(function(data){

            if(data.isLicenceValid){
                logger.log({ level: 'info', message:data.msg});
                //console.log(data.msg);
            }else{
                console.error(data.msg);
                process.exit(1)
            }
        }).catch(function(err){
            console.error(err.message);
            process.exit(1)
        });
        /*1000*60*60*6*/
    },1000*60*60*6)
};
let validity = function(){
    return new Promise(function(resolve,reject){
        let licenceData = getFileData();
        let expiryDate = new Date(licenceData.data.expirationDate),
            currentDate = new Date(),
            timeDifference = expiryDate.getTime() - currentDate.getTime();
        let differentDays = Math.ceil(timeDifference / (1000 * 3600 * 24));
        let response = {
            message:null,
            days:differentDays,
            enableSoftWarning:true
        };
        let isMessageOverride = licenceData.data["messageOverride"];
        let softWarningEnableByDays = defaultsoftWarningEnableByDays;
        if(licenceData.data["softWarningEnablerInDays"]){
            softWarningEnableByDays = parseInt(licenceData.data["softWarningEnablerInDays"]);
        }
        if(differentDays>softWarningEnableByDays){
            response.enableSoftWarning=false;
        }
        if(isMessageOverride === "true"){
            getWarningMessage().then(function(data){
                response.message= data._doc.licenseMessage;
                resolve(response);
            })
        }else{
            response.message="License will expire in "+Math.abs(differentDays)+" days. Please contact your vendor to renew your license.";
            resolve(response);
        }
    })


};
let getWarningMessage = function(){
    return new Promise(function(resolve,reject){
        licenceDetailsModel.findOne({},function(err,data){
            resolve(data)
        })
    })
}
module.exports = {
    validation:validation,
    startCheckUp:startCheckUp,
    getFileData:getLicenceData,
    validity:validity
};