/**
 * Created by suhas on 30/6/18.
 */

let nearByLogModel = require('../models/nearbyLogs');
let _= require("lodash");
let config= require("../../config/config");
let constant= require("../../config/constants");
let rfIdDAO = require('../dao/rfId');
let assetDAO = require('../dao/assets');
let that = {};

that.save = function(nearByObj){
    return new Promise(function(resolve,reject){
        nearByObj.assetType = constant.ASSETS_TYPES.RF_ID;
        let nearByLogModelInst = new nearByLogModel(nearByObj);
        nearByLogModelInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};

that.get = function(duration,registered){
    return new Promise(function(resolve,reject) {
        let startDate = new Date();
        let currMin = startDate.getMinutes();
        let durationInMinutes = config.RF_ID.LOG.RETRIEVE_SCANNED_DURATION;
        if (duration) {
            duration = parseFloat(duration);
            durationInMinutes = duration;
        }
        startDate.setMinutes(currMin - durationInMinutes);
        getAllRecentlySeenRfIdTag(startDate)
            .then(function(nearByRfIdTag){
                let groupNearByNearByRfIdTagByRFIDId = _.groupBy(nearByRfIdTag,"RFIDId");
                let nearByRFIDIds = _.keys(groupNearByNearByRfIdTagByRFIDId);
                rfIdDAO.getAllByRSSId(nearByRFIDIds)
                    .then(function(registeredRfIdTag){
                        let registeredRfIdTagsGroupedById = _.groupBy(registeredRfIdTag,"_doc.RFIDId");
                        let beaconDiscoveredObj = _.map(nearByRFIDIds,function(rfIdTagdNearBy){
                            let nearByObj = groupNearByNearByRfIdTagByRFIDId[rfIdTagdNearBy][0];
                            let savedAsset = registeredRfIdTagsGroupedById[rfIdTagdNearBy];
                            nearByObj.isRegistered=false;
                            if(savedAsset && savedAsset[0]){
                                savedAsset = savedAsset[0];
                                nearByObj.isRegistered=true;
                                nearByObj.groups = savedAsset._doc.groups;
                                nearByObj.assetsLinked = savedAsset._doc.assetsLinked;
                                nearByObj._id = savedAsset._doc._id;
                            }
                            nearByObj.createdInMilliSeconds = new Date().getTime();
                            nearByObj.createdInMilliSeconds = new Date(nearByObj.lastSeen).getTime();
                            nearByObj.updated = new Date(nearByObj.lastSeen).getTime();
                            return nearByObj
                        });
                        if(registered){
                            if(registered==="true"){
                                beaconDiscoveredObj = _.remove(beaconDiscoveredObj,function(obj){
                                    return obj.isRegistered
                                });
                            }else if(registered==="false"){
                                beaconDiscoveredObj = _.remove(beaconDiscoveredObj,function(obj){
                                    return !obj.isRegistered
                                });
                            }
                        }
                        resolve(beaconDiscoveredObj);
                    })
                    .catch(function (err2) {reject(err2);})
            })
            .catch(function(err){
                reject(err);
            })
    })

};

let getAllRecentlySeenRfIdTag = function(startDate){
    return new Promise(function(resolve,reject){
        let matchQuery = {
            created: { // 18 minutes ago (from now)
                $gt: new Date(startDate)
            },
            assetType:{
                $eq:constant.ASSETS_TYPES.RF_ID
            }
        };
        let aggregationPipeLine = [
            {$match: matchQuery},
            {$group: {_id:"$RFIDId", "lastSeen":{$last:"$updated"}, "tagName":{$first:"$tagName"}, "assetType":{$first:"$assetType"}}},
            {$project: {_id:0, "RFIDId":"$_id", "updated":"$lastSeen", "tagName":1, "assetType":1, "lastSeen":1}}
        ];
        nearByLogModel.aggregate(aggregationPipeLine,function(err,res){
            resolve(res)
        });
    })
};
that.getLastSeenRfIdTag = function (type,duration) {
    return new Promise(function(resolve, reject){

        let startDate = new Date();
        let currMin = startDate.getMinutes();
        let durationInMinutes = config.RF_ID.INSPECTION_BAY.RETRIEVE_SCANNED_DURATION;
        if(duration){
            duration = parseInt(duration);
            durationInMinutes=duration;
        }
        startDate.setMinutes( currMin - durationInMinutes);
        let matchQuery = {created: {$gt: new Date(startDate)}, assetType:{$eq:constant.ASSETS_TYPES.RF_ID}};
        let pipeLine = [
            {$match: matchQuery},
            {$sort: {"created":1}},
            {
                $group:
                {
                    _id:"$assetType",
                    lastSeenDate: { $last: "$created" },
                    lastSeenRfId: { $last: "$RFIDId" },
                    lastSeenBeacon: { $last: "$beaconId" },
                    "count": {$sum: 1}
                }
                },
            {$project :{_id:0, "type":"$_id",
                lastSeenDate:1,
                lastSeenRfId:1,
                lastSeenBeacon:1,
                count:1}}
        ];
        nearByLogModel.aggregate(pipeLine,function (err,result) {
                if(err){
                    reject(err)
                }else{
                    let lastLoggedRfId = result;
                    if(lastLoggedRfId && lastLoggedRfId[0] && lastLoggedRfId[0].lastSeenRfId){
                        let count = lastLoggedRfId[0].count;
                        rfIdDAO.getAllByRSSId([lastLoggedRfId[0].lastSeenRfId])
                            .then(function(rfIdObject){
                                if(rfIdObject[0] && rfIdObject[0]._doc){
                                    let rfIdObjectData = rfIdObject[0]._doc;
                                    rfIdObjectData.noOfTimesSeen = count;
                                    let assetsLinked = rfIdObjectData.assetsLinked;
                                    if(assetsLinked && assetsLinked.length>0){
                                        this.getCarInfoByRfId(assetsLinked,rfIdObjectData)
                                        .then(function(data){
                                            resolve(data)
                                        });
                                    }else{
                                        resolve({[constant.ASSETS_TYPES.RF_ID]:rfIdObjectData,msg:"RF ID Detected Is Registered But Not Linked To Car"});
                                    }

                                }else{
                                    resolve({[constant.ASSETS_TYPES.RF_ID]:null,msg:"RF ID Detected Is Not Registered"});
                                }
                            })
                    }else{
                        resolve({[constant.ASSETS_TYPES.RF_ID]:null,msg:"No RF ID Detected In Last "+durationInMinutes+" Minutes"})
                    }
                }
            }
        );

    })
};



that.getCarInfoByRfId = function (assetsLinked,rfIdObjectData) {
    return new Promise(function(resolve,reject){
        let assetsLinkedTypes = _.groupBy(assetsLinked, "_doc.assetType");
        if (assetsLinkedTypes[constant.ASSETS_TYPES.CAR] && assetsLinkedTypes[constant.ASSETS_TYPES.CAR].length > 0) {
            let linkedCar = assetsLinkedTypes[constant.ASSETS_TYPES.CAR][0];
            assetDAO.getById(linkedCar._doc._id)
            .then(function (carData) {
                resolve({[constant.ASSETS_TYPES.RF_ID]: rfIdObjectData, [constant.ASSETS_TYPES.CAR]: carData});
            })
        } else {
            resolve({[constant.ASSETS_TYPES.RF_ID]: rfIdObjectData, msg: "RF ID Detected Is Registered But Not Linked To Car"});
        }
    })
};
module.exports=that;