/**
 * Created by suhas on 9/7/18.
 */
let constants = require("../../config/constants");
let config = require("../../config/config");
let beaconLogDataAccess = require("./beaconLog");
let rfIdLogDataAccess = require("./rfIdLog");
let obdLogDataAccess = require("./obdDevice");
let Q = require("q");
let _ = require("lodash");
let that = {};

that.getAllNearBy = function(type,duration,registered){
    return new Promise(function(resolve,reject){
        if(type){
            getNearByType(type,duration,registered)
                .then(function(result){
                    resolve(result);
                }).catch(function(err){
                reject(500).send(err)
            });
        }else{
            let promises = [];
            let BeaconLogPromise = getBeaconLog(duration, registered, resolve, reject).then(function(data){
                return Q(data);
            });
            let RfIdLogPromise=getRfIdLog(duration, registered, resolve, reject).then(function(data){
                return Q(data);
            });
            let ObdLogPromise=getObdLog(duration, registered, resolve, reject).then(function(data){
                return Q(data);
            });
            promises.push(BeaconLogPromise);
            promises.push(RfIdLogPromise);
            promises.push(ObdLogPromise);
            Q.all(promises).then(function(data){
                let result = [];
                _.each(data,function(arr){
                    result = _.union(result,arr);
                });
                resolve(result);
            });

        }
    })

};
that.getAllAssetsLinkedToNearByRfId = function(duration){
    return new Promise(function(resolve,reject){
        let type = constants.ASSETS_TYPES.RF_ID;
        let promises = [];
        let nearBeaconPromise = getBeaconLog(duration,"true")
        .then(function(beaconData){
            return Q({beacons:beaconData});
        });
        let lastRfIdPromise = rfIdLogDataAccess.getLastSeenRfIdTag(type,duration)
        .then(function(rfRelated){
            if(!rfRelated.msg){
                return Q({car:rfRelated[constants.ASSETS_TYPES.CAR],rfId:rfRelated[constants.ASSETS_TYPES.RF_ID]});
            }else{
                return Q(rfRelated);
            }
        });
        promises.push(nearBeaconPromise);
        promises.push(lastRfIdPromise);
        Q.all(promises).then(function(data){
            let nearByAssets = {};
            nearByAssets.beacons = data[0].beacons;
            if(data[1] && data[1][constants.ASSETS_TYPES.RF_ID]){
                nearByAssets[constants.ASSETS_TYPES.RF_ID] = data[1][constants.ASSETS_TYPES.RF_ID];
                if(data[1] && data[1].car){
                    nearByAssets[constants.ASSETS_TYPES.CAR] = data[1][constants.ASSETS_TYPES.CAR];
                }
            }else{
                nearByAssets["error_msg"]=data[1].msg;
            }
            resolve(nearByAssets);
        });
    })

};


let getNearByType = function(type,duration,registered){
    return new Promise(function(resolve,reject){
        let types = _.split(type,",");
        let promises = [];
        let isRfId =_.includes(types,constants.ASSETS_TYPES.RF_ID);
        let isBeacon =_.includes(types,constants.ASSETS_TYPES.BEACON);
        let isObd =_.includes(types,constants.ASSETS_TYPES.OBD_DEVICE);
        if(isBeacon){
            let promise = getBeaconLog(duration, registered, resolve, reject).then(function(data){
                return Q(data);
            });
            promises.push(promise);
        }
        if(isRfId){
            let promise=getRfIdLog(duration, registered, resolve, reject).then(function(data){
                return Q(data);
            });
            promises.push(promise);
        }
        if(isObd){
            let promise=getObdLog(duration, registered, resolve, reject).then(function(data){
                return Q(data);
            });
            promises.push(promise);
        }

        Q.all(promises).then(function(data){
            let result = [];
            _.each(data,function(arr){
                result = _.union(result,arr);
            });
            resolve(result);
        });
    })

};
let getBeaconLog = function (duration, registered) {
    return new Promise(function(resolve, reject){
        beaconLogDataAccess.get(duration, registered)
            .then(function (result) {
                resolve(result);
            }).catch(function (err) {
            reject(500).send(err)
        });
    })
};
let getRfIdLog = function (duration, registered) {
    return new Promise(function(resolve, reject){
        rfIdLogDataAccess.get(duration, registered)
            .then(function (result) {
                resolve(result);
            })
            .catch(function (err) {
                reject(500).send(err)
            });
    })
};
let getObdLog = function (duration, registered) {
    return new Promise(function(resolve, reject){
        obdLogDataAccess.getAll(duration, registered)
            .then(function (result) {
                resolve(result);
            })
            .catch(function (err) {
                reject(500).send(err)
            });
    })
};

module.exports=that;