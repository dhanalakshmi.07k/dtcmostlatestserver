/**
 * Created by suhas on 27/6/18.
 */
let assetConfigModel = require('../models/assetConfiguration');
let _ = require("lodash");
let that ={};
that.getAll = function(){
    return new Promise(function(resolve,reject){
        assetConfigModel.find({},function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
    })
};
that.getAllTypes = function(){
    return new Promise(function(resolve,reject){
        assetConfigModel.aggregate(
            // Pipeline
            [
                // Stage 1
                {
                    $group: {_id:"$assetType.typeId",
                        label:{ $last: "$assetType.label" },
                        theme:{ $last: "$assetType.theme" },
                        order:{ $last: "$assetType.order" },
                        config:{ $last: "$assetType.config" },
                        configuration:{ $last: "$configuration"}
                    }

                },

                // Stage 2
                {
                    $project: {
                        "type":"$_id",
                        "label":1,
                        "theme":1,
                        "order":1,
                        "config":1,
                        "configuration":1,
                        "_id":0
                    }
                },

            ],function(err,response){
                if(err){
                    reject(err)
                }else{
                    resolve(response)
                }
            }
        );

    })
};
that.getByType = function(type){
    return new Promise(function(resolve,reject){
        assetConfigModel.findOne({"assetType.typeId":type},function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
    })
};

module.exports=that;