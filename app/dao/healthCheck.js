/**
 * Created by suhas on 10/10/18.
 */

let healthCheckLogModel = require('../models/healthCheck'),
moment=require("moment"),_=require("lodash");
let constants = require("../../config/constants");
let that = {};

that.save = function(healthCheckObj){
    return new Promise(function(resolve,reject){
        let healthCheckLogModelInst = new healthCheckLogModel(healthCheckObj);
        healthCheckLogModelInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};

that.getServerStats = function(startDateInMilliSeconds,endDateInMilliSeconds,assetId){
    return new Promise(function(resolve,reject){
        let startDate = new Date(moment(parseFloat(startDateInMilliSeconds)).utc(false));
        let endDate = new Date(moment(parseFloat(endDateInMilliSeconds)).utc(false));
        healthCheckLogModel.aggregate(
            [
                {
                    $match: {"updated" :{"$gt": startDate, "$lt": endDate}, "gateWayDetail._id":assetId}
                },
                {
                    $group: {
                        _id:null,
                        lastUpdated:{$last:"$updated"},
                        mem:{$last:"$mem"},
                        cpu:{$last:"$cpu"},
                        osInfo:{$last:"$osInfo"},
                        fsSize:{$last:"$fsSize"},
                        currentLoad:{$last:"$currentLoad"},
                        fullLoad:{$last:"$fullLoad"},
                        temperature:{$last:"$temperature"},
                        name:{$last:"$gateWayDetail.name"},
                        ip:{$last:"$gateWayDetail.gatewayIpAddress"}
                    }
                },

            ],function(err,result){
                if(err){
                    reject(err.stack)
                }else if(result){
                    resolve(result)
                }
            })
    });

};
that.getAllServerStats = function(startDateInMilliSeconds,endDateInMilliSeconds){
    return new Promise(function(resolve,reject){
        let startDate = new Date(moment(parseFloat(startDateInMilliSeconds)).utc(false));
        let endDate = new Date(moment(parseFloat(endDateInMilliSeconds)).utc(false));
        healthCheckLogModel.aggregate(
            [
                {
                    $match: {"updated" :{"$gt": startDate, "$lt": endDate}}
                },
                {
                    $group: {
                        _id:"$gateWayDetail._id",
                        lastUpdated:{$last:"$updated"},
                        mem:{$last:"$mem"},
                        cpu:{$last:"$cpu"},
                        osInfo:{$last:"$osInfo"},
                        fsSize:{$last:"$fsSize"},
                        currentLoad:{$last:"$currentLoad"},
                        fullLoad:{$last:"$fullLoad"},
                        temperature:{$last:"$temperature"},
                        name:{$last:"$gateWayDetail.name"},
                        ip:{$last:"$gateWayDetail.gatewayIpAddress"}
                    }
                },

            ],function(err,result){
                if(err){
                    reject(err.stack)
                }else if(result){
                    resolve(result)
                }
            })
    });
};
that.getServiceHealthByServiceId = function(startDateInMilliSeconds,endDateInMilliSeconds,serviceId){
    return new Promise(function(resolve,reject){
        let startDate = new Date(moment(parseFloat(startDateInMilliSeconds)).utc(false));
        let endDate = new Date(moment(parseFloat(endDateInMilliSeconds)).utc(false));
        healthCheckLogModel.aggregate(
            [
                // Stage 1
                {
                    $unwind: {
                        path : "$serviceDetails"
                    }
                },

                // Stage 2
                {
                    $project: {
                        "_id":"$serviceDetails.serviceDetails._id",
                        "serviceDetails":1,
                        "updated":1
                    }
                },

                // Stage 3
                {
                    $match: {
                        "updated" :{"$gt": startDate, "$lt": endDate},
                        "_id":serviceId
                    }
                },

                // Stage 4
                {
                    $group: {
                        "_id":"$_id",
                        serviceId:{$last:"$serviceDetails.serviceDetails._id"},
                        status:{$last:"$serviceDetails.status"},
                        name:{$last:"$serviceDetails.serviceDetails.name"},
                        monitor:{$last:"$serviceDetails.monitor"},
                        upTime:{$last:"$serviceDetails.upTime"},
                        running:{$last:"$serviceDetails.running"}
                    }
                },

            ],function(err,result){
                if(err){
                    reject(err.stack)
                }else if(result){
                    resolve(result)
                }
            })
    });

};
that.getAllServiceHealth = function(startDateInMilliSeconds,endDateInMilliSeconds){
    return new Promise(function(resolve,reject){
        let startDate = new Date(moment(parseFloat(startDateInMilliSeconds)).utc(false));
        let endDate = new Date(moment(parseFloat(endDateInMilliSeconds)).utc(false));
        healthCheckLogModel.aggregate(
            [
                {
                    $match: {"updated" :{"$gt": startDate, "$lt": endDate}}
                },
                {
                    $unwind: {
                        path : "$serviceDetails"
                    }
                },

                // Stage 3
                {
                    $project: {
                        "serviceDetails":1
                    }
                },

                // Stage 4
                {
                    $group: {
                        "_id":"$serviceDetails.serviceDetails._id",
                        serviceId:{$last:"$serviceDetails.serviceDetails._id"},
                        status:{$last:"$serviceDetails.status"},
                        name:{$last:"$serviceDetails.serviceDetails.name"},
                        monitor:{$last:"$serviceDetails.monitor"} ,
                        upTime:{$last:"$serviceDetails.upTime"},
                        running:{$last:"$serviceDetails.running"}
                    }
                },

            ]
            ,function(err,result){
                if(err){
                    reject(err.stack)
                }else if(result){
                    resolve(result)
                }
            })
    });

};


that.getServiceHistoricalStats = function(startDateInMilliSeconds,endDateInMilliSeconds,serviceId){
    return new Promise(function(resolve,reject){
        let startDate = new Date(moment(parseFloat(startDateInMilliSeconds)).utc(false));
        let endDate = new Date(moment(parseFloat(endDateInMilliSeconds)).utc(false));

        healthCheckLogModel.aggregate(
            [{
                $unwind: {
                    path : "$serviceDetails"
                }
            }, {
                $match: {
                    "created" :{"$gt": startDate, "$lt": endDate},
                    "serviceDetails.serviceDetails._id":serviceId
                }
            }, {
                $group: {
                    _id:{hour: { $hour: "$created" },
                        day: { $dayOfMonth: "$created" },
                        month: { $month: "$created" },
                        year: { $year: "$created" }},
                    memory: { $avg: "$serviceDetails.monitor.memory" },
                    cpu: { $avg: "$serviceDetails.monitor.cpu" } ,
                    created: { $last: "$created" }
                }
            },{
                    $project: {
                        "_id":1,
                        "memory":1,
                        "cpu":1,
                        "created":1

                    }
                }]
            ,function(err,result){
                if(err){
                    reject(err.stack)
                }else if(result){
                    if(result && result.length>0){
                        result = _.map(result,function(data){
                            let dateString ='MMM Do YYYY, h a';
                            let dateInString = moment(data.created).format(dateString);
                            let milliSeconds =
                            data.dateInString=dateInString;
                            return data
                        })
                    }
                    resolve(result)
                }
            })
    });
};
that.getServerHistoricalStats = function(startDateInMilliSeconds,endDateInMilliSeconds,assetId,series,field){
    return new Promise(function(resolve,reject){
        let startDate = new Date(moment(parseFloat(startDateInMilliSeconds)).utc(false));
        let endDate = new Date(moment(parseFloat(endDateInMilliSeconds)).utc(false));
        let dateToConsidered = "created";
        let groupData = {
            "minute":{
                minutes: { $minute: "$"+dateToConsidered },
                hour: { $hour: "$"+dateToConsidered },
                day: { $dayOfMonth: "$"+dateToConsidered },
                month: { $month: "$"+dateToConsidered },
                year: { $year: "$"+dateToConsidered }
            },
            "hour":{
                hour: { $hour: "$"+dateToConsidered },
                day: { $dayOfMonth: "$"+dateToConsidered },
                month: { $month: "$"+dateToConsidered },
                year: { $year: "$"+dateToConsidered }
            },
            "day":{
                day: { $dayOfMonth: "$"+dateToConsidered },
                month: { $month: "$"+dateToConsidered },
                year: { $year: "$"+dateToConsidered }
            },
            "month":{
                month: { $month: "$"+dateToConsidered },
                year: { $year: "$"+dateToConsidered }
            },
            "year":{
                year: { $year: "$"+dateToConsidered }
            },
            /*status:"$overview.status"*/
        };
        /*if(!series){
            series="minute";
        }*/
        let fieldsSelected = {
            all:{
                groups:{ _id:groupData[series],
                    currentLoad: { $avg: "$currentLoad.currentload" },
                    fullLoad: { $avg: "$fullLoad" },
                    temperature_main: { $avg: "$temperature.main" } ,
                    temperature_max: { $avg: "$temperature.max" } ,
                    mem_total: { $avg: "$mem.total" } ,
                    mem_free: { $avg: "$mem.free" } ,
                    mem_used: { $avg: "$mem.used" } ,
                    created: { $last: "$created" } ,
                    diskSpace: { $last: "$fsSize" }
                },
                project:{
                    "_id":1,
                    "cpu":{"currentload":"$currentLoad","fullLoad":"$fullLoad"},
                    "temperature":{"main":"$temperature_main","max":"$temperature_max"},
                    "memory":{"total":"$mem_total","free":"$mem_free","used":"$mem_used"},
                    "created":"$created",
                    "fsSize":"$diskSpace",
                }
            },
            cpu:{
                groups:{ _id:groupData[series],
                    currentLoad: { $avg: "$currentLoad.currentload" },
                    fullLoad: { $avg: "$fullLoad" },
                    created: { $last: "$created" } ,
                },project:{
                    "_id":1,"created":"$created",
                    "cpu":{"currentload":"$currentLoad","fullLoad":"$fullLoad"},
                }
                },
            memory:{
                groups:{ _id:groupData[series],
                    mem_total: { $avg: "$mem.total" } ,
                    mem_free: { $avg: "$mem.free" } ,
                    mem_used: { $avg: "$mem.used" } ,
                    created: { $last: "$created" } ,
                },
                project:{
                    "_id":1,"created":"$created",
                    "memory":{"total":"$mem_total","free":"$mem_free","used":"$mem_used"}
                }
                },
            temperature:{
                groups:{ _id:groupData[series],
                    temperature_main: { $avg: "$temperature.main" } ,
                    created: { $last: "$created" } ,
                    temperature_max: { $avg: "$temperature.max" }
                    },
                project:{
                    "_id":1,"created":"$created",
                    "temperature":{"main":"$temperature_main","max":"$temperature_max"}
                }
                },
            disk:{
                groups:{ _id:groupData[series],
                    created: { $last: "$created" } ,
                    diskSpace: { $last: "$fsSize" }
                },project:{
                    "_id":1,
                    "fsSize":"$diskSpace","created":"$created",
                }
            }
        };
        if(!field){
            field="all";
        }
        if(series){
            getServerGroupedStats(startDate, endDate, assetId, fieldsSelected, field, series)
            .then(function(result){
                resolve(result)
            }).catch(function(error){
                reject({msg:error});
            });
        }else{
            getServerStatsSample(startDate, endDate, assetId, field)
            .then(function(result){
                resolve(result)
            }).catch(function(error){
                reject({msg:error});
            });
        }
    });
};

function getServerGroupedStats(startDate, endDate, assetId, fieldsSelected, field, series) {
    return new Promise(function(resolve,reject){
        healthCheckLogModel.aggregate(
            [
                // Stage 1
                {
                    $match: {
                        "updated": {"$gt": startDate, "$lt": endDate},
                        "gateWayDetail._id": assetId
                    }
                },

                // Stage 2
                {
                    $group: fieldsSelected[field].groups
                },

                // Stage 3
                {
                    $project: fieldsSelected[field].project
                },

            ]
            , function (err, result) {
                if (err) {
                    reject(err.stack)
                } else if (result) {
                    if (result && result.length > 0) {
                        console.log()
                        result = _.map(result, function (data) {
                            let dateSeries = null;
                            let dateString = "";
                            if (series === constants.TIME_SERIES.INSPECTION_REPORT[0]) {
                                dateString = 'MMM Do YYYY, h a';
                                dateSeries = moment().year(data._id.year).month(data._id.month - 1).date(data._id.day).hour(data._id.hour);
                            }
                            if (series === constants.TIME_SERIES.INSPECTION_REPORT[1]) {
                                dateString = 'MMM Do YYYY';
                                dateSeries = moment().year(data._id.year).month(data._id.month - 1).date(data._id.day);

                            }
                            if (series === constants.TIME_SERIES.INSPECTION_REPORT[2]) {
                                dateString = 'MMM YYYY';
                                dateSeries = moment().year(data._id.year).month(data._id.month - 1);
                            }
                            if (series === constants.TIME_SERIES.INSPECTION_REPORT[3]) {
                                dateString = 'YYYY';
                                dateSeries = moment().year(data._id.year);
                            }
                            if (series === constants.TIME_SERIES.INSPECTION_REPORT[4]) {
                                dateString = 'h:mm a';
                                dateSeries = moment().year(data._id.year).month(data._id.month - 1).date(data._id.day).hour(data._id.hour).minutes(data._id.minutes);
                            }
                            data.milliseconds = parseFloat(moment(dateSeries).format("x"));
                            data.dateInString = moment(dateSeries).format(dateString);
                            return data
                        })
                    }
                    resolve(result)
                }
            })
    })
};
function getServerStatsSample(starTime,endTime,assetId,field){
    return new Promise(function(resolve,reject){
        let fieldsSelected = {
            all:{
                project:{
                    "_id":1,
                    "cpu":{"currentload":"$currentLoad.avgload","fullLoad":"$fullLoad"},
                    "temperature":1,
                    "memory":"$mem",
                    "created":1,
                    "fsSize":1,
                }
            },
            cpu:{
                project:{
                    "_id":1,"created":"$created",
                    "cpu":{"currentload":"$currentLoad.avgload","fullLoad":"$fullLoad"},
                }
            },
            memory:{
                project:{
                    "_id":1,"created":"$created",
                    "memory":"$mem",
                }
            },
            temperature:{
                project:{
                    "_id":1,"created":"$created",
                    "temperature":1,
                }
            },
            disk:{
                project:{
                    "_id":1,"created":"$created",
                    "fsSize":1,
                }
            }
        };
        let dateFormat = "L H:m";
        healthCheckLogModel.aggregate(
            [
                {
                    $match: {
                        "updated":{
                            $gte: starTime,
                            $lte: endTime
                        },
                        "gateWayDetail._id" : assetId,
                    }
                },
                {
                    $project: fieldsSelected[field].project
                },
            ])
            .then(function(result){
            if(result){
                if(result && result.length>0){
                    result = _.map(result,function(data){
                        let dateString ='h:mm a';
                        let dateSeries = moment(data.created);
                        data.milliseconds = parseFloat(moment(dateSeries).format("x"));
                        data.dateInString = moment(dateSeries).format(dateFormat);
                        data.uniqueDate = moment(dateSeries).format(dateFormat);
                        return data
                    })

                }

                getTimeSeries(starTime,endTime,1,dateFormat).
                then(function(timeSeries){
                    let groupAnalyticsDataOnTimeString = _.groupBy(result,"uniqueDate");
                    let responseData = _.map(timeSeries,function(timeSeriesObj){
                        if(groupAnalyticsDataOnTimeString && groupAnalyticsDataOnTimeString[timeSeriesObj.dateString] && groupAnalyticsDataOnTimeString[timeSeriesObj.dateString][0]){
                            return groupAnalyticsDataOnTimeString[timeSeriesObj.dateString][0]
                        }else{
                            return {
                                dateInString:timeSeriesObj.dateString,
                                milliseconds:timeSeriesObj.milliseconds,
                                "cpu":{"currentload":null,"fullLoad":null},
                                "temperature":{main:0,max:0},
                                "memory":{
                                    "total" : 0,
                                    "free" : 0,
                                    "used" : 0,
                                    "active" : 0,
                                    "available" : 0,
                                    "buffcache" : 0,
                                    "swaptotal" : 0,
                                    "swapused" : 0,
                                    "swapfree" : 0
                                },
                                "fsSize":[],
                            };
                        }
                    });
                    resolve(responseData)
                });

            }
        })
            .catch(function(error){
            reject(error);
        });
    })

}

function getTimeSeries(startTime,endTime,diffInMinutes,format){
    return new Promise(function(resolve,reject){
        let timeSeries=[]
        let result = "";
        let i = 0;
        let dateString =format;
        let startOfDateInMilliSeconds = parseFloat(moment(startTime).format("x"));
        let endOfDateInMilliSeconds = parseFloat(moment(endTime).format("x"));
        let startTimeInMoment = moment(startTime);
        let totalMinutes = moment(endOfDateInMilliSeconds).diff(moment(startOfDateInMilliSeconds),'minutes');
        for(let i=0;i<totalMinutes;i++){
            let dateSeries = startTimeInMoment.add(diffInMinutes,'minutes');
            let dateInString = moment(dateSeries).format(dateString);
            let obj = {
                dateString:dateInString,
                milliseconds:moment(dateSeries).format('x')
            }
            timeSeries.push(obj);
            if(i===totalMinutes-1){
                resolve(timeSeries);
            }
        }

    })
}
module.exports=that;