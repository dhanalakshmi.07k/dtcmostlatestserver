/**
 * Created by suhas on 18/3/19.
 */

const gpsLogModel = require('../models/gpsLog');
const socketConn = require("../ws/socketIo");
const constants = require('../../config/constants');
const moment = require('moment');
const _ = require('lodash');

let that ={};
that.save=function(gpsDataLogs){
    const gpsLogModelInstance = new gpsLogModel(gpsDataLogs);
    pushOverSocket(gpsDataLogs);
    gpsLogModelInstance.save(function(err){
        if(err){
            console.log(err)
        }
    })
};
that.getHistoricalData = function(startDate1,endDate1,vinNo){
    return new Promise(function (resolve, reject) {
        let startDate = new Date(moment(parseFloat(startDate1)).utc(false));
        let endDate = new Date(moment(parseFloat(endDate1)).utc(false));
        const pipeline = [
            {
                "$match": {
                    "assetDetails.data.vinNumber": vinNo,
                    "updated": {
                        "$gt": startDate,
                        "$lt": endDate
                    }
                }
            },
            {
                "$group": {
                    "_id": {
                        "seconds": {
                            "$second": "$updated"
                        },
                        "minute": {
                            "$minute": "$updated"
                        },
                        "hour": {
                            "$hour": "$updated"
                        },
                        "day": {
                            "$dayOfMonth": "$updated"
                        },
                        "month": {
                            "$month": "$updated"
                        },
                        "year": {
                            "$year": "$updated"
                        }
                    },
                    "updated": {
                        "$first": "$updated"
                    },
                    "location": {
                        "$first": "$location"
                    },/*
                    "assetDetails": {
                        "$first": "$assetDetails"
                    },*/
                    "speed": {
                        "$first": "$speed"
                    }
                }
            },
            {
                "$project": {
                    "_id": 0.0,
                    /*"car": {
                        "_id": "$assetDetails._id",
                        "carNo": "$assetDetails.data.registrationNumber",
                        "vinNumber": "$assetDetails.data.vinNumber"
                    },*/
                    "coordinates": "$location.coordinates",
                    "speed": "$speed",
                    "updated":"$updated"
                }
            },
            {
                "$sort": {
                    "updated": 1.0
                }
            }
        ]

        //const cursor = gpsLogModel.aggregate(pipeline, options);
        let aggregationData = gpsLogModel.aggregate(pipeline
            ,function(err,data){
                if(err){
                    reject(err)
                }else{
                    data=_.map(data,function (geoFence) {
                        /*if(geoFence.coordinates){
                            geoFence.coordinates = _.map(geoFence.coordinates,function (coordinate) {
                                return [coordinate[1],coordinate[0]]
                            })
                        }*/
                        geoFence.coordinates = [geoFence.coordinates[1],geoFence.coordinates[0]];
                        return geoFence
                    })
                    resolve(data)
                }
            })
        aggregationData.allowDiskUse(true);
        aggregationData.cursor({batchSize:50});
    })
}
let pushOverSocket = function (data) {
    const coordinates= [data.location.coordinates[1],data.location.coordinates[0]]
    data.location.coordinates = coordinates;
    socketConn.emitToAllSocketConnection(constants.PUBLISHER.SOCKET.OBD_DEVICE_GPS, data)
};
module.exports=that