/**
 * Created by suhas on 28/6/18.
 */
let assetDAO = require("./assets"),
    mongoose = require("mongoose"),
    logger=require("../../config/logger").assets,
    _ = require("lodash");
let that = {};

that.link =  function(linkObj){
    return new Promise(async function(resolve,reject){
        let primaryAsset = linkObj.primaryAsset;
        let assetsToBeLinked = linkObj.assetsToBeLinked;
        if(!primaryAsset){
            reject("Primary Assets Not Found")
        }
        if(!assetsToBeLinked){
            reject("Assets to Be Not Found")
        }
        linkAllTOPrimary(assetsToBeLinked,primaryAsset)
        .then(function (primaryAssetModified) {
            linkPrimaryToAll(assetsToBeLinked,primaryAsset)
            .then(function (allOtherAssetsModified) {
                allOtherAssetsModified.push(primaryAssetModified)
                resolve(allOtherAssetsModified)
            })
        });
    })

};

async function linkAllTOPrimary(idList, primaryAssetId){
    return new Promise(function(resolve,reject) {
        "use strict";
        let array =[primaryAssetId]
        assetDAO.getAllByIds(array).then(async function (prAssetObj) {
            let isLinked = await linkAndUpdate(prAssetObj[0],idList);
            resolve(isLinked);
        });
    })
}
async function linkPrimaryToAll(idList, primaryAssetId){
    return new Promise(function(resolve,reject){
        let assetsUpdated = [];
        let allPromises = [];
        assetDAO.getAllByIds(idList)
        .then(function (linkableAssetsArray) {
            if(linkableAssetsArray && linkableAssetsArray.length>0){
                _.each(linkableAssetsArray,async function (linkableAssetsObj) {
                    let assetsToBeLinked = [];
                    assetsToBeLinked.push(primaryAssetId);
                    let updateObj = linkAndUpdate(linkableAssetsObj,assetsToBeLinked);
                    assetsUpdated.push(updateObj);
                    allPromises.push(updateObj);
                });
                Promise.all(allPromises).then( function (updatedAssetList) {
                    resolve(updatedAssetList);
                })
            }
            });
    })
}
function linkAndUpdate(assetsObj,assetsToBeLinked){
    "use strict";
    return new Promise(function(resolve,reject) {
        let assetsLinked = assetsObj["_doc"]["assetsLinked"];
        if(assetsLinked && assetsLinked.length>0){
            _.each(assetsToBeLinked,function (id) {
                let assetsLinkedArray = _.map(assetsLinked,function (obj) {
                    return obj.toString()
                })
                let isPresent = _.includes(assetsLinkedArray,id.toString());
                if(!isPresent){
                    let linkObj = new mongoose.Types.ObjectId(id);
                    assetsLinked.push(linkObj)
                }
            })
            assetsObj["_doc"]["assetsLinked"] = assetsLinked;
            assetDAO.updateAssetsLinked(assetsObj._doc._id.toString(),assetsObj._doc)
            .then(function(result){
                resolve(result)
            })
        }else{
            assetsLinked=[];
            _.each(assetsToBeLinked,function (id) {
                let linkObj = new mongoose.Types.ObjectId(id);
                assetsLinked.push(linkObj)
            })
            assetsObj["_doc"]["assetsLinked"] = assetsLinked;
            assetDAO.updateAssetsLinked(assetsObj._doc._id.toString(),assetsObj._doc)
                .then(function(result){
                    resolve(result)
                })
        }
    })

}
module.exports = that;