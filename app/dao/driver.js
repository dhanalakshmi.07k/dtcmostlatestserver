/**
 * Created by suhas on 14/6/18.
 */
var assetModel = require('../models/assets'),
    constants=require('../../config/constants');

var that = {};
that.save = function(driverObj){
    return new Promise(function(resolve,reject){
        let driverModelInst = new assetModel(driverObj);
        driverModelInst.assetType = constants.ASSETS_TYPES.DRIVER;
        driverModelInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};
that.getByDriverId=function(driverId){
    return new Promise(function(resolve,reject){
        let id = driverId;
        let assetType = constants.ASSETS_TYPES.DRIVER;
        assetModel.findOne({"driverId":id,"assetType":assetType},function(err, docs){
            if(err){
                reject(err);
            }else{
                resolve(docs);
            }
        }).populate({path: 'groups',model: 'assetGroup'}).populate({path: 'assetsLinked',model: 'asset'});

    })
};
module.exports=that;