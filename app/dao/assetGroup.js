/**
 * Created by suhas on 27/6/18.
 */
let assetGroupModel = require('../models/assetGroup');
let assetDAO = require('./assets');
let _ = require('lodash'),
/*sync = require('sync'),*/
mongoose=require("mongoose");
let that ={};
that.save = function(assetGroupModelObj){
    return new Promise(function(resolve,reject){
        var assetGroupModelInst = new assetGroupModel(assetGroupModelObj);
        assetGroupModelInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};
that.update = function(id,assetGroupObj){
    return new Promise(function(resolve,reject){
        assetGroupModel.findById(id, function (err, assetModelObjDb) {
            if (err){
                reject(err)
            }
            else{
                assetModelObjDb.set(assetGroupObj);
                assetModelObjDb.save(function (err, updatedAssetGroup) {
                    if (err) {
                        reject(err)
                    }
                    else{
                        resolve(updatedAssetGroup);
                    }
                });
            }


        });
    })
};
that.saveMultiple = function(assetGroups){
    return new Promise(function(resolve,reject){
        //var assetModelInst = new assetModel(assetObj);
        assetGroupModel.insertMany(assetGroups,function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};
that.getAll = function(){
    return new Promise(function(resolve,reject){
        assetGroupModel.find({},function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        }).select({ "label": 1,"color": 1,"type": 1, "_id": 1})
    })
};
that.getAllById=function(assetGroupIds){
    return new Promise(function(resolve,reject){
        let arr = _.map(assetGroupIds,function(ele){
            return new mongoose.Types.ObjectId(ele)
        });
        assetGroupModel.find()
            .where('_id')
            .in(assetGroupIds)
            .exec(function(err,res){
                if(err){
                    reject(err)
                }else{
                    resolve(res);
                }
            });
    })
};
that.getById = function(id){
    return new Promise(function(resolve,reject){
        //var carModelInst = new carModel();
        assetGroupModel.findById(id,function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        });
    })
};
that.linkToAsset = function(assetGroupObj){
    return new Promise(function(resolve,reject){
        try{
            let assetIdList = assetGroupObj.assets;
            let assetGroupIdList = assetGroupObj.groups;
            getAllAssetsAndGroupToBeLinked(assetGroupIdList,assetIdList)
            .then(function(allAssetAndGroupList){
                linkToGroups(allAssetAndGroupList)
                .then(function(updatedAssetList){
                    if(updatedAssetList && updatedAssetList.length>0){
                    _.each(updatedAssetList,function(assetObj,index){
                        let data = assetDAO.updateGroups(assetObj._doc._id.toString(),assetObj._doc);
                        if(index===updatedAssetList.length-1){
                            resolve(updatedAssetList);
                        }
                    });
                    }else{
                        resolve("No Asset Found")
                    }
                })
            });
        }
        catch(e){
            reject(e)
        }
    })
};
that.deLinkToAsset = function(assetGroupObj){
    return new Promise(function(resolve,reject){
        let assetIdList = assetGroupObj.assets;
        let assetGroupIdList = assetGroupObj.groups;
        getAllAssetsAndGroupToBeLinked(assetGroupIdList,assetIdList)
        .then(function(allAssetAndGroupList){
            deLinkToGroups(allAssetAndGroupList)
            .then(function(updatedAssetList){
                if(updatedAssetList && updatedAssetList.length>0){
                    _.each(updatedAssetList,function(assetObj,index){
                        let data = assetDAO.updateGroups(assetObj._doc._id.toString(),assetObj._doc);
                        if(index===updatedAssetList.length-1){
                            resolve(updatedAssetList);
                        }
                    });
                }else{
                    resolve("No Asset Found")
                }
            })
        });
    })
};
function getAllAssetsAndGroupToBeLinked(groupIdList,assetIdList){
    return new Promise(function(resolve,reject){
        that.getAllById(groupIdList).then(function(allGroupsSelected){
            let allGroups = allGroupsSelected;
            assetDAO.getAllByIds(assetIdList)
            .then(function (assetsToBeLinked) {
                resolve({groupList:allGroups,assetList:assetsToBeLinked})
            })
        });
    })
}
function linkToGroups(data){
    return new Promise(function(resolve,reject){
        /*sync(function(){*/
        let groupList = data.groupList;
        let assetList = data.assetList;
        if(assetList && assetList.length>0){
            _.each(assetList,function(assetObj,index){
                if (assetObj._doc.groups) {
                    let groupDataArranged = _.map(groupList,function(obj){
                        return obj._doc._id.toString()
                    });
                    let groupDataStored = _.map(assetObj._doc.groups,function(obj){
                        return obj.toString()
                    });
                    let diff = _.difference(groupDataArranged,groupDataStored);
                    let finalArray = _.uniq(_.union(diff,groupDataStored));
                    _.map(finalArray,function(obj){
                        return new mongoose.Types.ObjectId(obj)
                    });
                    assetObj._doc.groups = finalArray;
                } else {
                    assetObj._doc.groups = _.map(groupList,function(groupObj){
                        return groupObj._doc._id
                    });
                }
                if(index===assetList.length-1){
                    resolve(assetList);
                }
            });
        }else{
            resolve(null)
        }
        });
    /*})*/
}
function deLinkToGroups(data){
    return new Promise(function(resolve,reject){
        /*sync(function() {*/
        let groupList = data.groupList;
        let assetList = data.assetList;
        if (assetList && assetList.length > 0) {
            _.each(assetList, function (assetObj, index) {
                if (assetObj._doc.groups) {
                    let groupDataToBeRemoved = _.groupBy(_.map(groupList, function (obj) {
                        return obj._doc._id.toString()
                    }));
                    let assetGroupData = _.map(assetObj._doc.groups, function (obj) {
                        return obj.toString()
                    });
                    _.remove(assetGroupData, function(assetGroupId) {
                        return groupDataToBeRemoved[assetGroupId];
                    });
                    let finalArray = _.uniq(assetGroupData);
                    _.map(finalArray, function (obj) {
                        return new mongoose.Types.ObjectId(obj)
                    });
                    assetObj._doc.groups = finalArray;
                }
                if (index === assetList.length - 1) {
                    resolve(assetList);
                }
            });
        } else {
            resolve(null)
        }
        /*})*/
    });
}


module.exports=that;