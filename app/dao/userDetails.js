/**
 * Created by suhas on 8/2/19.
 */

let user = require('../models/userDetails');
let fs = require("fs");
let jwt= require('jsonwebtoken');
let bCrypt = require('bcrypt-nodejs');
let path = require('path');
let constant=require('../../config/constants');
let config=require('../../config/config');
let _=require('lodash');
let userDetailsModel = require('../models/userDetails');
let roleInfoDao = require("../dao/roles");
let ROLES_CONSTANT = require("../auth/ability").RULES;
let createUser = function (req, res) {
    let pwd=req.body.password;
    // find a user in Mongo with provided username
    /*user.findOne({'username':req.body.username},function(err, user) {
        // In case of any error return
        if (err){
            res.status(401).send('User does not exists register to the application');
        }
        // already exists
        if (user) {
            res.status(401).send('User already exists');

        } else {
            // if there is no user with that email
            // create the user
            req.ability.throwUnlessCan('create_role',req.body.roles);
            userDataAccess.saveUserDetails(req.body)
                .then(function(result){
                    res.send(result);
                })
        }
    });*/
    res.status(401).send('Authorization Required');
};
const { packRules } = require('@casl/ability/extra')

let verifyUser = function (username, pwd) {

    return new Promise(function (resolve, reject) {

        user.findOne({ 'username' :username },
            function(err, user) {
                // In case of any error, return using the done method
                if (err)
                    return done(err);
                // Username does not exist, log error & redirect back
                if (!user){
                    reject('Wrong user or password');
                    return;
                }
                // User exists but wrong password, log the error
                let status=isValidPassword(user, pwd );
                if (!isValidPassword(user, pwd )){
                    reject('Wrong  password');
                    return;
                }
                let roles = user._doc.roles;
                let token=generateToken(user)
                let roleAssigned = roles[0];
                roleInfoDao.getByRole(roleAssigned).then(function(roleInfo){
                    if(roleInfo.ability){
                        roleInfo.ability = packRules(roleInfo.ability)
                    }
                    resolve({ token: token,roles:roles,roleInfo:roleInfo});
                });

            }
        );
    })


};


let getAllUser = function (ability) {
    return new Promise(function (resolve, reject) {
        let groupedAbilityRoles = _.groupBy(ability.rules,'subject');
        let getAbilityBySubject = groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE];

        if(getAbilityBySubject && getAbilityBySubject[0] && groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE][0] &&
            groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE] &&
            groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE][0].actions  &&
            groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE][0].actions.length>0  ) {
            let rolesCanBeAccessed = groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE][0].actions;
            if(rolesCanBeAccessed){
                user.find( { roles: { $in: rolesCanBeAccessed } },
                    function(err, users) {
                        // In case of any error, return using the done method
                        if (err)
                            return done(err);
                        resolve(users);
                    }
                );
            }
        }

    })

};

let addUser = function(body, ability){
    let pwd=body.password;
    let roles=body.roles;
    let username=body.username;
    ability.throwUnlessCan(ROLES_CONSTANT.ACTION.WRITE,ROLES_CONSTANT.SUBJECT.USER);
return new Promise(function (resolve, reject) {
    if(!pwd){
        reject(400).send("password is required");
    }
    if(!username){
        reject(400).send("username is required");
    }
    if(roles && roles.length===0){
        reject(400).send("role is required");
    }
    ability.throwUnlessCan(roles[0],ROLES_CONSTANT.SUBJECT.WRITE_ROLE);
    user.findOne({'username':username},function(err, user) {
        // In case of any error return
        if (err){
            reject(400).send(err.message);
        }
        // already exists
        if (user) {
            reject(400).send('User already exists');

        } else {
            // if there is no user with that email
            // create the user
            ability.throwUnlessCan('write',"User");
            saveUserDetails(body)
                .then(function(result){
                    resolve({msg:"User Created Successfully",obj:result});
                })
        }
    });

})

};



let deleteUser = function(userId,ability){
    return new Promise(function (resolve, reject) {
        if(!userId){
            reject(400).send("user Id is required");
        }
        user.findById(userId,function(err, userObj) {
            if (err){
                reject(400).send(err.message);
            }
            if (userObj) {
                let userToBeDeletedRole = userObj._doc.roles;
                ability.throwUnlessCan(userToBeDeletedRole[0],ROLES_CONSTANT.SUBJECT.DELETE_ROLE);
                user.deleteOne({ _id:userObj._doc._id }, function (err,result) {
                    if (err) {
                        reject(500).send(err)
                    }else{
                        resolve({msg:'User Deleted Successfully'});
                    }
                });


            } else {
                reject(400).send({msg:'User Not Found'});
            }
        });

    })


};

function getAllRoles(roles){
    return new Promise(function (resolve, reject) {

        let groupedAbilityRoles = _.groupBy(roles,'subject');
        let getAbilityBySubject = groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE];

        if(getAbilityBySubject && getAbilityBySubject[0] && groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE][0] &&
            groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE] &&
            groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE][0].actions  &&
            groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE][0].actions.length>0  ) {
            let rolesCanBeAccessed = groupedAbilityRoles[ROLES_CONSTANT.SUBJECT.READ_ROLE][0].actions;
            resolve(rolesCanBeAccessed);
        }else{
            reject([]);
        }
    })

};
function generateToken(user){
    // User and password both match, return user from
    // done method which will be treated like success
    // We are sending the profile inside the token
    //let filePath=path.join(__dirname, '/../../public.key');
    let cert = fs.readFileSync(path.join(__dirname,"../../config/auth_public_key.key"));
    let obj={}
    obj.userDetails=restructureObj(user);
    let genToken = jwt.sign(obj, cert, { algorithm: 'RS256'}, {
        expiresIn: constant.AUTHENTICATION.PARAMETERS.SESSION_EXPIRE_TIME// expires in 60seconds
    });

    return genToken;
}
let isValidPassword = function(user, pwd){
    return bCrypt.compareSync(pwd, user.password);
}
function restructureObj(obj){
    let modifiedObj={};
    modifiedObj.username=obj.username;
    modifiedObj.firstName=obj.firstName;
    modifiedObj.lastName=obj.lastName;
    modifiedObj.email=obj.email;
    modifiedObj.roles=obj._doc.roles;
    modifiedObj._id=obj._doc._id.toString();
    return modifiedObj

}

function saveUserDetails(userObject){
    return new Promise(function(resolve,reject){
        var obj=restructureNewUserObj(userObject)
        let userDetailsModelInst = new userDetailsModel(obj);
        userDetailsModelInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};
function restructureNewUserObj(obj){
    let modifiedObj={}
    modifiedObj.username=obj.username
    modifiedObj.password=createHash(obj.password)
    modifiedObj.firstName=obj.firstName
    modifiedObj.lastName=obj.lastName
    modifiedObj.email=obj.email
    modifiedObj.roles=obj.roles
    return modifiedObj

}



let createHash = function(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

let updatePassword= function(newPassword,userDetails){
    return new Promise(function(resolve,reject){
        if(!newPassword){
            reject({msg:"password is empty"})
        }else if(newPassword.length<4){
            reject({msg:"password length should be greater than 4"})
        }else{
            let newPasswordHash =createHash(newPassword);
            user.update({"username":userDetails.username}, { $set: { password:newPasswordHash}}, { multi: true },
                function(err, userObject){
                    resolve(userObject)
                })
        }
    })
};


let isUserNameAlreadyUsed = function(userName){
    return new Promise(function (resolve, reject) {
        user.findOne({"username":userName}, function (err, person) {
            if(err){
                reject(400).send(err);
            }
            if(person){
                resolve(true);
            }
            else{
                resolve(false);
            }
        })
    })
}


let getUserById = function(id){
    return new Promise(function(resolve,reject){
        user.findOne({"_id":id}, function (err, user){
            if(err){
                reject(err);
                return err;
            }
            if(user){
                resolve(user);
            }
            else{
                reject("User Not Found")
            }
        })
    })
}

let updateUserById = function (id,userObject) {
    return new Promise(function (resolve, reject) {
        user.findByIdAndUpdate(id,
            {"firstName": userObject.firstName, "lastName": userObject.lastName, "email": userObject.email, "roles":userObject.roles},
            {new: true}, function (err, updatedUser) {
                if (err) {
                    reject(err);
                }
                if (updatedUser) {
                    resolve(updatedUser);
                }
            })
    })
}

let updateUserByName = function(id,userObject){

    return new Promise(function (resolve, reject) {

        user.findByIdAndUpdate(id,
            {"username": userObject.username,"firstName": userObject.firstName, "lastName": userObject.lastName, "email": userObject.email,"roles":userObject.roles},
            {new: true},
            function (err, updatedUser) {
                if (err) {
                    reject(err);
                }
                if (updatedUser) {
                    resolve(updatedUser);
                }
            })

    })
}

let updateUserProfile = function(id,userObj){
    return new Promise(function(resolve,reject){
        getUserById(id)
            .then(function(userProfile){
                if(userProfile.username === userObj.username){
                    updateUserById(id,userObj)
                        .then(function(updatedUserObject){
                            resolve(updatedUserObject)
                        }).catch(function(err){
                        reject(err)
                    });
                }
                else{
                    isUserNameAlreadyUsed(userObj.username)
                        .then(function(isUserNameAlreadyUsed){
                            if(!isUserNameAlreadyUsed){
                                updateUserByName(id,userObj)
                                    .then(function(updatedUserObject){
                                        resolve(updatedUserObject)
                                    }).catch(function(err){
                                    reject(err)
                                });
                            }else{
                                reject("User Already Exist With UserName  "+userObj.username+".\n  Username Should be Unique")
                            }
                        }).catch(function(err){
                        reject(err)
                    });
                }
            })
            .catch(function(err){
                reject(err)
            })
    })
}

module.exports={
    createUser:createUser,
    verifyUser:verifyUser,
    getAllUser:getAllUser,
    addUser:addUser,
    allRolesDefinition:getAllRoles,
    updatePassword:updatePassword,
    deleteUser:deleteUser,
    isUserNameAlreadyUsed:isUserNameAlreadyUsed,
    getUserById:getUserById,
    updateUserProfile:updateUserProfile
}
