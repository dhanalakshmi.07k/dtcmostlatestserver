/**
 * Created by suhas on 20/7/18.
 */
/**
 * Created by suhas on 23/6/18.
 */
let rfIdModel = require('../models/assets');
let constants= require("../../config/constants");
let that = {};



that.getAllByRSSId = function(rffIds){
    return new Promise(function(resolve,reject){
        rfIdModel.find({'RFIDId':{$in:rffIds},"assetType":constants.ASSETS_TYPES.RF_ID})
            .populate({path: 'groups',model: 'assetGroup'})
            .populate({path: 'assetsLinked',model: 'asset'})
            .exec(function(err,res){
                if(err){
                    reject(err)
                }else{
                    resolve(res);
                }
            });
    })
};
that.updateLastSeen = function(rffIds){
    return new Promise(function(resolve,reject){
        rfIdModel.findOneAndUpdate({RFIDId: rffIds,assetType:constants.ASSETS_TYPES.RF_ID}, {$set:
            {lastSeen:new Date().getTime(),updated:new Date().getTime()}}, {new: true}, function(err, doc){
            if(err){
                console.log("Something wrong when updating RFID Last Seen!");
            }
            if(doc && doc._doc){
                resolve(doc._doc)
                console.log("updated RFID With Last Seen data :"+rffIds);
            }
        }).populate({path: 'assetsLinked',model: 'asset'});;
    })
};
module.exports=that;