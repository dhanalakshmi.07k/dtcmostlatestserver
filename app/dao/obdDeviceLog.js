/**
 * Created by suhas on 20/7/18.
 */
let obdDeviceLogModel = require('../models/obdLog');
let constants = require('../../config/constants');
let that = {};

that.save = function(nearByObj){
    return new Promise(function(resolve,reject){
        nearByObj.assetType =constants.ASSETS_TYPES.OBD_DEVICE;
        let obdDeviceLogModelInst = new obdDeviceLogModel(nearByObj);
        obdDeviceLogModelInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};
that.getAllWithInGivenMinutesAndVin = function(durationInMinutes,vin){
    let startDate = new Date();
    let currMin = startDate.getMinutes();
    if (durationInMinutes) {
        durationInMinutes = parseFloat(durationInMinutes);
    }
    startDate.setMinutes(currMin - durationInMinutes);
    return new Promise(function(resolve,reject){
        let matchQuery = {
            created: {
                $gt: new Date(startDate)
            }, vin:vin
        };
        let aggregationPipeLine = [
            {$match: matchQuery}
        ];
        obdDeviceLogModel.aggregate(aggregationPipeLine,function(err,res){
            resolve(res)
        });
    })
};

module.exports=that;