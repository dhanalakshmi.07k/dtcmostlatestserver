/**
 * Created by suhas on 13/2/19.
 */

let roleInfoModel = require('../models/roles');
let _ = require("lodash");
let that = {};
that.getAll = function(){
    return new Promise(function(resolve,reject){
        roleInfoModel.find({},function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
    })
};
that.getByRole = function(role){
    return new Promise(function(resolve,reject){
        roleInfoModel.findOne({"role":role},function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res._doc);
            }
        })
    })
};

module.exports=that;
