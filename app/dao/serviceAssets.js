/**
 * Created by suhas on 11/10/18.
 */

let serviceAssetModel = require('../models/serviceAssets');
let serviceAssetConfigModel = require('../models/serviceAssetsConfig');
let assetDao = require('../dao/assets'),
    healthCheckDao = require('../dao/healthCheck');
let serviceAssetConfigDao = require('../dao/serviceAssetConfig');
let _ = require('lodash');
let mongoose = require('mongoose');
let that = {};
let mqtt=require('../publisher/mqtt'),
    constants=require('../../config/constants'),
    Q=require("q");
let request = require("request");
that.save = function(serviceAssetObj){
    return new Promise(function(resolve,reject){
        let serviceAssetModelInst = new serviceAssetModel(serviceAssetObj);
        serviceAssetModelInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};
that.getById = function(id){
    return new Promise(function(resolve,reject){
        serviceAssetModel.findById(id,function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        }).populate({path: 'gatewayAsset',model: 'asset'});
    })
};
that.getAll = function(skip,limit,order,type){
    return new Promise(function(resolve,reject){
        try{

            serviceAssetConfigDao.getAllTypes()
                .then(function(types){
                    let assetTypeByConfig = _.keys(_.groupBy(types,'type'));
                    if (type) {
                        assetTypeByConfig = _.intersection(assetTypeByConfig,type);
                    }
                    serviceAssetModel.find({})
                        .where('serviceType')
                        .in(assetTypeByConfig)
                        .sort({'created':-1})
                        .limit(limit)
                        .skip(skip)
                        .populate({path: 'gatewayAsset',model: 'asset'})
                        .exec(function(err,res){
                            if(err){
                                reject(err)
                            }else{
                                resolve(res);
                            }
                        });
                })
        }
        catch(e){
            console.log(e);
            reject(e)
        }
    })
};
that.getAllByGatewayId = function(gatewayId){
    return new Promise(function(resolve,reject){

    })
};
that.linkGateway = function(linkData){
    return new Promise(function(resolve,reject){
        linkServiceAssets(linkData)
        .then(function(response){
            let serviceAssetTopic = constants.PUBLISHER.MQTT.TOPIC.SERVICE_ADDED;
            publishServiceAsset(linkData,serviceAssetTopic);
            resolve(response);
        })
    })
};
that.deLinkGateway = function(deLinkData){
    return new Promise(function(resolve,reject){
        deLinkServiceAssets(deLinkData)
        .then(function (response) {
            let serviceAssetTopic = constants.PUBLISHER.MQTT.TOPIC.SERVICE_ADDED;
            publishServiceAsset(deLinkData,serviceAssetTopic);
            resolve(response);
        })
    })
};
that.update = function(assetServiceObj){
    return new Promise(function(resolve,reject){
        update(assetServiceObj)
        .then(function(res){
            resolve(res);
        })
    })
};
that.delete = function(id){
    return new Promise(function(resolve,reject){
        deleteById(id).then(function(res){
            resolve(res);
        })
    })
};
that.getAllByIds = function(serviceAssetIdList){
    return new Promise(function(resolve,reject){
        let arr = _.map(serviceAssetIdList,function(ele){
            return new mongoose.Types.ObjectId(ele)
        });
        let queryModel = serviceAssetModel.find()
            .where('_id')
            .in(arr)
            .populate({path: 'gatewayAsset',model: 'asset'})
        queryModel.exec(function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        });
    })
};
that.getConfig = function(){
    return new Promise(function(resolve,reject){
        serviceAssetConfigModel.find({},function(err,model){
            if(err){reject(err)}
            else{resolve(model)}
        })
    })
}
that.getCount = function(type){
    return new Promise(function(resolve,reject){
        serviceAssetConfigDao.getAllTypes()
            .then(function(types){
                let assetTypeByConfig = _.keys(_.groupBy(types,'type'));
                if (type) {
                    assetTypeByConfig = _.intersection(assetTypeByConfig,type);
                }

                serviceAssetModel.aggregate(
                    [
                        {
                            $match: {"serviceType":{$in:assetTypeByConfig}}

                        },
                        {
                            $group: {_id : null,count:{$sum : 1}}
                        },
                        {
                            $project: {"count":1,"_id":0}
                        },

                    ],
                    function(err,count){
                        if(err){
                            reject(err)
                        }else{
                            if(count[0] && count[0]["count"]){
                                resolve(count[0]["count"])
                            }else{
                                resolve(0)
                            }

                        }
                    });
            })
    })

};
that.updateById = function(id,serviceObj){
    return new Promise(function(resolve,reject){
        serviceAssetModel.findById(id, function (err, serviceAssetModelObj) {
            if (err){
                reject(err)
            }
            else{
                serviceObj.gatewayAsset = serviceAssetModelObj._doc.gatewayAsset;
                serviceAssetModelObj.set(serviceObj);
                serviceAssetModelObj.save(function (err, updatedServiceAsset) {
                    if (err) {
                        reject(err)
                    }
                    else{
                        let id=updatedServiceAsset._doc._id;
                        that.getById(id)
                        .then(function(result){
                                resolve(result)
                        })
                    }
                });
            }


        });
    })
};
that.serviceActions = function(id,action){
    return new Promise(function(resolve,reject){
        that.getById(id).then(function(obj){
            let serviceObject = obj._doc;
            if(serviceObject.gatewayAsset){
                let data = { serviceName:serviceObject.identifier, actions: action,serviceDetails:serviceObject};
                if(serviceObject.networkType==="remote"){
                    let serviceAssetTopic = constants.PUBLISHER.MQTT.TOPIC.SERVICE_ADDED;
                    let mqttTopicHealthManagement = serviceObject.gatewayAsset._doc.gatewayIpAddress+"Actions"+serviceAssetTopic;
                    publishServiceAsset(data,mqttTopicHealthManagement)
                    resolve({error:false,message:" Command Has Been Issued , Please Wait For Response"})
                }else if(serviceObject.networkType==="local"){
                    let options = { method: 'POST',
                        url: 'http://'+serviceObject.gatewayAsset._doc.gatewayIpAddress+':4501/serviceManager/manage',
                        headers:{'Content-Type': 'application/json' },
                        body:data,
                        json: true };

                    request(options, function (error, response, body) {
                        if (error){
                            reject(error)
                        }else{
                            resolve(body)
                        }
                    });
                }else{
                    reject({error:true,message:"Network Type Not Defined , Please Define Network Type"})
                }
            }else{
                reject({error:true,message:"Not Linked To Server/Gateway , Please Link To Manage"})
            }
        })
    })
};
that.getStatus = function(startDateInMilliSeconds,endDateInMilliSeconds,serviceId){
    return new Promise(function(resolve,reject){
        if(serviceId){
            healthCheckDao.getServiceHealthByServiceId(startDateInMilliSeconds,endDateInMilliSeconds,serviceId).then(function(data){
                resolve(data)
            }).catch(function(err){
                reject(err)
            })
        }else{
            healthCheckDao.getAllServiceHealth(startDateInMilliSeconds,endDateInMilliSeconds).then(function(data){
                resolve(data)
            }).catch(function(err){
                reject(err)
            })
        }
    })
}
that.getHistoricalServiceStats = function(startDateInMilliSeconds,endDateInMilliSeconds,serviceId){
    return new Promise(function(resolve,reject){
        if(serviceId){
            healthCheckDao.getServiceHistoricalStats(startDateInMilliSeconds,endDateInMilliSeconds,serviceId)
            .then(function(data){
                resolve(data)
            }).catch(function(err){
                reject(err)
            })
        }
    })
}

function linkServiceAssets(linkObj){
    return new Promise(function(resolve,reject){
        let gatewayId= linkObj.gatewayId;
        let serviceAssetIds = linkObj["serviceAssetIds"];
        if(serviceAssetIds){
            serviceAssetIds = _.uniq(serviceAssetIds);
        }
        linkGatewayToService(gatewayId,serviceAssetIds)
        .then(function (result1) {
            linkServiceToGateway(gatewayId,serviceAssetIds)
            .then(function (result2) {
                result2.push(result1);
                resolve(result1)
            })
        })
    })
}
function linkGatewayToService(gatewayId, serviceIdToBeLinked){
    return new Promise(function(resolve,reject){
        assetDao.getById(gatewayId)
        .then(function(gatewayObj){
            if(gatewayObj){
                let serviceLinked = gatewayObj._doc.serviceAssets;
                let serviceToBeLinked = [];
                if(serviceLinked && serviceLinked.length>0){
                    _.each(serviceIdToBeLinked,async function (serviceObjToBeLinkedObj) {
                        let allServiceId = _.map(serviceLinked,function (alreadyLinkedServiceId) {
                            return alreadyLinkedServiceId.toString();
                        });
                        let alreadyExist = _.includes(allServiceId,serviceObjToBeLinkedObj.toString());
                        if(!alreadyExist){
                            let linkObj = new mongoose.Types.ObjectId(serviceObjToBeLinkedObj.toString());
                            serviceToBeLinked.push(linkObj)
                        }
                    });
                    gatewayObj._doc.serviceAssets = serviceToBeLinked;

                }else{
                    gatewayObj._doc.serviceAssets = _.map(serviceIdToBeLinked,function (serviceId) {
                       return  new mongoose.Types.ObjectId(serviceId.toString());
                    });
                }
                assetDao.updateServiceAssetsLinked(gatewayObj._doc._id.toString(),gatewayObj)
                .then(function(result){
                    resolve(result)
                })
            }
        });
    })
}
async function linkServiceToGateway(gatewayId, serviceIdList){
    return new Promise(function(resolve,reject) {
        that.getAllByIds(serviceIdList)
        .then(function(allServiceObjList){
            let promises = [];
            _.each(allServiceObjList,async function (serviceObj) {
                serviceObj._doc.gatewayAsset=new mongoose.Types.ObjectId(gatewayId.toString());
                let promise = that.update(serviceObj._doc)
                promises.push(promise)
            })
            Promise.all(promises).then(function (data) {
                resolve(data)
            })
        })
    })

}


function deLinkServiceAssets(linkObj){
    return new Promise(function(resolve,reject){
        let gatewayId= linkObj.gatewayId;
        let serviceAssetIds = linkObj["serviceAssetIds"];
        if(serviceAssetIds){
            serviceAssetIds = _.uniq(serviceAssetIds);
        }
        deLinkGatewayToService(gatewayId,serviceAssetIds)
        .then(function (result1) {
            deLinkServiceToGateway(serviceAssetIds,[gatewayId])
            .then(function (result2) {
                result2.push(result1);
                resolve(result2)
            })
        })
    })
}
function deLinkGatewayToService(gatewayId, serviceIdToBeDeLinked){
    return new Promise(function(resolve,reject){
        assetDao.getById(new mongoose.Types.ObjectId(gatewayId))
        .then(function(gatewayObj){
            if(gatewayObj){
                let serviceLinked = gatewayObj._doc.serviceAssets;
                let allServiceId = _.map(serviceLinked,function (alreadyLinkedServiceId) {
                    return alreadyLinkedServiceId._doc._id.toString();
                });
                if(serviceLinked && serviceLinked.length>0){
                    _.remove(allServiceId,function (serviceId) {
                        return _.includes(serviceIdToBeDeLinked,serviceId)
                    })
                }
                gatewayObj._doc.serviceAssets = allServiceId;
                assetDao.updateServiceAssetsLinked(gatewayObj._doc._id.toString(),gatewayObj)
                    .then(function(result){
                        resolve(result)
                    })
            }
        });
    })
}
function deLinkServiceToGateway(gatewayId, serviceIdList){
    return new Promise(function(resolve,reject) {
        that.getAllByIds(serviceIdList)
            .then(function(allServiceObjList){
                let promises = [];
                _.each(allServiceObjList,function (serviceObj) {
                    let gatewayLinked = serviceObj._doc.gatewayAsset;
                    if(gatewayLinked && gatewayLinked.toString()===gatewayId.toString()){
                        serviceObj._doc.gatewayAsset=null;
                        let promise = that.update(serviceObj._doc);
                        promises.push(promise)
                    }
                });
                Promise.all(promises).then(function (data) {
                    resolve(data)
                })
            })
    })

}




function update(assetServiceObj){
    return new Promise(function(resolve,reject){
        let id = assetServiceObj._id;
        serviceAssetModel.findByIdAndUpdate(id,assetServiceObj,{new: true},
        function(err, model){
            if(err){
                reject(err)
            }else{
                resolve(model)
            }
        })
    })
}
function deleteById(id){
    return new Promise(function(resolve,reject){
        serviceAssetModel.remove({'_id':id}, function(err, docs) {
            if(err){
                reject(err)
            }else{
                resolve(docs)
            }
        })
    })
}
function isServiceAssetAlreadyLinked(assetService,serviceAssetsLinkedToGateway){
    let isAlreadyLinked = false;
    if(serviceAssetsLinkedToGateway && serviceAssetsLinkedToGateway.length>0){
        let allServiceAssetId = _.map(serviceAssetsLinkedToGateway,function(serviceAssetLinkObj){
            return serviceAssetLinkObj._doc.toString()
        });
        let assetServiceToBeLinked =assetService._id.toString();
        isAlreadyLinked = _.includes(allServiceAssetId,assetServiceToBeLinked);
    }else{
        isAlreadyLinked = false;
    }
    return isAlreadyLinked;
}
function updateGateway(gatewayAsset){
    return new Promise(function(resolve,reject) {
        let id = gatewayAsset._id;
        assetDao.updateServiceAssetsLinked(id, gatewayAsset).then(function (assetUpdate) {
            resolve(assetUpdate);
        })
    });
}
function removeGatewayDetails(id) {
    return new Promise(function(resolve,reject) {
        serviceAssetModel.findOneAndUpdate({ _id: id }, { gatewayAsset: null }, function(err, res) {
            if(err){
                reject(err)
            }else{
                resolve(res)
            }
        })
    })
}
function publishServiceAsset(data,serviceAssetTopic){
    //let serviceAssetTopic = constants.PUBLISHER.MQTT.TOPIC.SERVICE_ADDED;
    mqtt.publish(serviceAssetTopic,data)
}

module.exports=that;