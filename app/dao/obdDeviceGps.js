/**
 * Created by suhas on 14/3/19.
 */
/**
 * Created by suhas on 20/7/18.
 */
let gpsDao = require('../dao/gpsLogs');
let constants = require('../../config/constants');
const assetDao = require('./assets');
let _ = require("lodash");

let that = {};


that.save = function(obdDeviceGpsData){
    return new Promise(function(resolve,reject){
        let location= {
            "type": "Point",
            "coordinates": [ obdDeviceGpsData.longitude,obdDeviceGpsData.latitude,]
        };
        let assetDetails = {
            "uniqueIdentifier": {
                "value": obdDeviceGpsData.assetslInked.car.vinNumber,
                "key":"vinNumber",
            },
            _id:obdDeviceGpsData.assetslInked.car._id,
            data:obdDeviceGpsData.assetslInked.car
        };
        let data = {
            location:location,
            assetDetails:assetDetails,
            speed:obdDeviceGpsData.speed,
            deviceId:assetDetails.uniqueIdentifier.value
        }
        gpsDao.save(data);
    })
};
that.handler = function(obdDeviceGpsData){
    return new Promise(function(resolve,reject){
        obdDeviceGpsData.assetType =constants.ASSETS_TYPES.OBD_DEVICE;
        getAllLinkedAssets(obdDeviceGpsData.deviceId)
            .then(function (obdData) {
            if(obdData &&obdData._doc && obdData._doc.assetsLinked){
                let assetsLinked = obdData._doc.assetsLinked;
                if(assetsLinked && assetsLinked.length>0){
                    assetsLinked = _.map(assetsLinked,function (assetLinkedObj) {
                        return assetLinkedObj._doc
                    });
                    let assetsLinkedGroupByType = _.groupBy(assetsLinked,"assetType")
                    let assetsLinkedKey = _.keys(assetsLinkedGroupByType);
                    let assetsLinkedObj = {};
                    _.each(assetsLinkedKey,function (obj) {
                        //return assetsLinkedGroupByType[obj][0]
                        assetsLinkedObj[obj] =  assetsLinkedGroupByType[obj][0]

                    });
                    obdDeviceGpsData.assetslInked = assetsLinkedObj;
                    that.save(obdDeviceGpsData);
                    /*pushOverSocket(obdDeviceGpsData);*/
                }
            }
        }).catch(function (err) {

        })

    })
};

function getAllLinkedAssets(deviceId){
    return new Promise(function(resolve,reject){
        assetDao.getObdDeviceByUniqueIdentifier(deviceId).
        then(function (data) {
            resolve(data)
        }).catch(function (err) {
            reject(data)
        })
    })
}

module.exports=that;