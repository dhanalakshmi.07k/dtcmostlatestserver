/**
 * Created by suhas on 17/10/18.
 */
let serviceAssetConfigModel = require('../models/serviceAssetsConfig.js');
let _ = require("lodash");
let that ={};
that.getAll = function(){
    return new Promise(function(resolve,reject){
        serviceAssetConfigModel.find({},function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
    })
};
that.getAllTypes = function(){
    return new Promise(function(resolve,reject){
        serviceAssetConfigModel.aggregate(
            // Pipeline
            [
                // Stage 1
                {
                    $group: {_id:"$serviceType.typeId",
                        label:{ $last: "$serviceType.label" },
                        theme:{ $last: "$serviceType.theme" },
                        order:{ $last: "$serviceType.order" },
                        config:{ $last: "$serviceType.config" },
                        configuration:{ $last: "$configuration"}}

                },

                // Stage 2
                {
                    $project: {
                        "type":"$_id",
                        "label":1,
                        "theme":1,
                        "order":1,
                        "config":1,
                        "configuration":1,
                        "_id":0
                    }
                },

            ],function(err,response){
                if(err){
                    reject(err)
                }else{
                    resolve(response)
                }
            }
        );

    })
};
that.getByType = function(type){
    return new Promise(function(resolve,reject){
        serviceAssetConfigModel.findOne({"serviceType.typeId":type},function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
    })
};

module.exports=that;