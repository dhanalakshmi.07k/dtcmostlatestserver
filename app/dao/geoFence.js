/**
 * Created by suhas on 27/3/19.
 */
const geoFenceLogModel = require('../models/geoFence');
const _ = require('lodash');

let that ={};
that.save=function(geoFenceDataLogs){
    return new Promise(function (resolve, reject) {
        if(geoFenceDataLogs.location.coordinates){
            geoFenceDataLogs.location.coordinates = _.map(geoFenceDataLogs.location.coordinates,function (coordinate) {
                return [coordinate[1],coordinate[0]]
            })
        }
        if(geoFenceDataLogs.location.coordinates && geoFenceDataLogs.location.coordinates.length>0){
            geoFenceDataLogs.location.coordinates[geoFenceDataLogs.location.coordinates.length-1] = geoFenceDataLogs.location.coordinates[0];
        }
        const coOrdinates = geoFenceDataLogs.location.coordinates;
        geoFenceDataLogs.location.coordinates = [];
        geoFenceDataLogs.location.coordinates[0] = coOrdinates;
        const gpsLogModelInstance = new geoFenceLogModel(geoFenceDataLogs);
        gpsLogModelInstance.save(function(err){
            if(err){
                console.log(err)
            }else{
                resolve(gpsLogModelInstance)
            }
        })
    })
};
that.getAll = function(){
    return new Promise(function (resolve, reject) {
        geoFenceLogModel.find({},function (err, data) {
            if(err){
                reject(err)
            }else{
                data=_.map(data,function (geoFence) {
                    if(geoFence._doc.location.coordinates){
                        geoFence._doc.location.coordinates = _.map(geoFence._doc.location.coordinates[0],function (coordinate) {
                            return [coordinate[1],coordinate[0]]
                        })
                    }
                    return geoFence
                })
                resolve(data)
            }
        })
    })
}
that.getById = function(id){
    return new Promise(function (resolve, reject) {
        geoFenceLogModel.findById(id,function (err, data) {
            if(err){
                reject(err)
            }else{
                if( data && data._doc.location.coordinates){
                    data._doc.location.coordinates = _.map(data._doc.location.coordinates[0],function (coordinate) {
                        return [coordinate[1],coordinate[0]]
                    })
                }
                resolve(data)
            }
        })
    })
}
that.removeById = function(id){
    return new Promise(function (resolve, reject) {
        geoFenceLogModel.findByIdAndDelete(id,function (err, data) {
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}
that.updateById = function(id,data){
    return new Promise(function (resolve, reject) {
        geoFenceLogModel.findByIdAndUpdate(id,data,function (err, data) {
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}
module.exports=that;