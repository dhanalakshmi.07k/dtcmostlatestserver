/**
 * Created by suhas on 18/6/18.
 */

let assetModel = require('../models/assets'),
    healthCheckDao = require('../dao/healthCheck')
    assetConfigDao = require('../dao/assetConfig'),
    constants=require('../../config/constants'),
    mongoose=require('mongoose'),logger=require("../../config/logger").assets;
    _=require("lodash"),moment=require('moment');
let limitChecker = require("../stringHandler/validator");
let Q = require("q");
let fs = require('fs');

let config = require('../../config/config');
let excelToJson = require('convert-excel-to-json');
let Excel = require('exceljs');

function getSearchCriteria(searchText) {
    let allTypes = constants.ASSETS_TYPES;
    let searchCriteria = {
        [allTypes.CAR]: [
            {"fleetNo": {$regex: searchText, $options: 'i'}},
            {"registrationNumber": {$regex: searchText, $options: 'i'}},
            {"vinNumber": {$regex: searchText, $options: 'i'}},
            {"make": {$regex: searchText, $options: 'i'}},
            {"modelNo": {$regex: searchText, $options: 'i'}}
        ],
        [allTypes.DRIVER]: [
            {"mobileNumber": {$regex: searchText, $options: 'i'}},
            {"licenceNo": {$regex: searchText, $options: 'i'}},
            {"driverName": {$regex: searchText, $options: 'i'}},
            {"department": {$regex: searchText, $options: 'i'}},
            {"badgeNo": {$regex: searchText, $options: 'i'}},
            {"driverId": {$regex: searchText, $options: 'i'}},
            {"shiftStart": {$regex: searchText, $options: 'i'}}
        ],
        [allTypes.OBD_DEVICE]: [
            {"make": {$regex: searchText, $options: 'i'}},
            {"uniqueIdentifier": {$regex: searchText, $options: 'i'}},
            {"type": {$regex: searchText, $options: 'i'}}
        ],
        [allTypes.RF_ID]: [
            {"RFIDId": {$regex: searchText, $options: 'i'}},
            {"tagName": {$regex: searchText, $options: 'i'}}
        ],
        [allTypes.RF_ID_GATEWAY]: [
            {"gatewayName": {$regex: searchText, $options: 'i'}},
            {"gatewayIpAddress": {$regex: searchText, $options: 'i'}}
        ],
        [allTypes.BEACON_GATEWAY]: [
            {"gatewayName": {$regex: searchText, $options: 'i'}},
            {"gatewayIpAddress": {$regex: searchText, $options: 'i'}}
        ],
        [allTypes.BEACON]: [
            {"beaconId": {$regex: searchText, $options: 'i'}},
            {"beaconType": {$regex: searchText, $options: 'i'}}
        ],
        [allTypes.SERVER]: [
            {"name": {$regex: searchText, $options: 'i'}},
            {"gatewayIpAddress": {$regex: searchText, $options: 'i'}}
        ]
    };
    return searchCriteria;
}
function getSearchResult(condition, type, aggregationArray,limit,skip) {
    return new Promise(function(resolve,reject){
        if (condition && condition.length > 0) {
            if (skip) {
                skip=parseInt(skip);
            }else{skip=0}
            if (limit) {
                limit= parseInt(limit);
            }
            let assetQuery = assetModel.find({$or:condition,"assetType": {$in: type}})
                .skip(parseInt(skip))
                .limit(limit)
                .populate({path: 'groups',model: 'assetGroup'})
                .populate({path: 'assetsLinked',model: 'asset'})
                .populate({path: 'serviceAssets',model: 'serviceAssets'});
            assetQuery.exec(function(err,res){
                if (err) {
                    reject(err);
                } else {
                    resolve(res);
                }
            });
        } else {
            reject(" ASSET TYPE NOT FOUND ");
        }
    });
}
function getAssetSearchCount(condition, type, aggregationArray, reject, resolve) {
    return new Promise(function(resolve,reject){

        if (condition && condition.length > 0) {
            let matchQuery = {
                $match: {
                    $or: condition,
                    "assetType": {$in: type}
                }
            };
            aggregationArray.push(matchQuery);
            let group = [
                {
                    $group: {_id: "", count: {$sum: 1}}
                },
                {
                    $project: {"_id": 0, "count": 1}
                }
            ];
            aggregationArray = _.union(aggregationArray, group);
            assetModel.aggregate(aggregationArray, function (err, docs) {
                if (err) {
                    reject(err);
                } else {
                    if (!docs[0]) {
                        docs[0] = {
                            count: 0
                        };
                    }
                    resolve(docs[0]);
                }
            });
        }
        else {
            reject(" ASSET TYPE NOT FOUND ");
        }
    })
}


let that ={};
that.save = function(assetObj){
    return new Promise(function(resolve,reject){
        let assetModelInst = new assetModel(assetObj);
        assetModelInst.save(function(err,res){
            if(err){
                /*console.log(err);*/
                reject(err);
            }else{
                //resolve(res);
                let id=res._doc._id;
                that.getById(id)
                .then(function(result){
                    resolve(result)
                })
            }
        })
    })
};
that.getById = function(id){
    return new Promise(function(resolve,reject){
        //let carModelInst = new carModel();
        //let id = mongoose.Types.ObjectId(id)
        assetModel.findById(id,function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
        .populate({path: 'groups',model: 'assetGroup'})
        .populate({path: 'assetsLinked',model: 'asset'})
        .populate({path: 'serviceAssets',model: 'serviceAssets'});
    })
};
that.getAllByIds = function(assetIdList,allDetails){
    return new Promise(function(resolve,reject){
        let arr = _.map(assetIdList,function(ele){
            return new mongoose.Types.ObjectId(ele)
        });
        let queryModel = assetModel.find()
        .where('_id')
        .in(arr)
        if(allDetails){
            queryModel.populate({path: 'groups',model: 'assetGroup'})
                .populate({path: 'assetsLinked',model: 'asset'})
                .populate({path: 'serviceAssets',model: 'serviceAssets'})
        }

        queryModel.exec(function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        });
    })
};
that.getAllByTypes = function(type){
    return new Promise(function(resolve,reject){
        assetModel.find({"assetType":type})
        .sort({created:-1})
        .populate({path: 'groups',model: 'assetGroup'})
        .populate({path: 'assetsLinked',model: 'asset'})
        .populate({path: 'serviceAssets',model: 'serviceAssets'})
        .exec(function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        });
    })
};
that.getAll = function(start,limit,order,type){
    return new Promise(function(resolve,reject){
        assetConfigDao.getAllTypes()
        .then(function(types){
            let assetTypeByConfig = _.keys(_.groupBy(types,'type'));
            if (type) {
                assetTypeByConfig = _.intersection(assetTypeByConfig,type);
            }
            assetModel.find({})
            .where('assetType')
            .in(assetTypeByConfig)
            .sort({'created':-1})
            .limit(limit)
            .skip(start)
            .populate({path: 'groups',model: 'assetGroup'})
            .populate({path: 'assetsLinked',model: 'asset'})
            .populate({path: 'serviceAssets',model: 'serviceAssets'})
            .exec(function(err,res){
                if(err){
                    reject(err)
                }else{
                    resolve(res);
                }
            });
        });
    })

};
that.getCount = function(type){
    return new Promise(function(resolve,reject){
        assetConfigDao.getAllTypes()
            .then(function (types) {
                let assetTypeByConfig = _.keys(_.groupBy(types,'type'));
                if (type) {
                    assetTypeByConfig = _.intersection(assetTypeByConfig,type);
                }
                if(assetTypeByConfig.length>0){
                    assetModel.aggregate(
                        [
                            {
                                $match: {"assetType":{$in:assetTypeByConfig}}

                            },
                            {
                                $group: {_id : null,count:{$sum : 1}}
                            },
                            {
                                $project: {"count":1,"_id":0}
                            },

                        ],
                        function(err,count){
                            if(err){
                                reject(err)
                            }else{
                                if(count[0] && count[0]["count"]){
                                    resolve(count[0]["count"])
                                }else{
                                    resolve(0)
                                }

                            }
                        });
                }else{
                    reject("Asset Type Invalid")
                }
            })
    })

};
that.getAllCountByTypes = function(){
    return new Promise(function(resolve,reject){
        assetModel.aggregate(
            [
                {
                    $group: {_id:"$assetType", count:{$sum:1}}

                },
                {
                    $project: {
                        "type":"$_id",
                        "count":1,
                        "_id":0
                    }
                },

            ],
            function(err,allTypeCounts){
                if(err){
                    reject(err)
                }else{
                    assetConfigDao.getAllTypes()
                    .then(function(allTypes){
                        let groupCountByType = _.groupBy(allTypeCounts,"type");
                        _.map(allTypes,function(typeObj){
                            if(groupCountByType[typeObj.type] && groupCountByType[typeObj.type][0]){
                                typeObj.count=groupCountByType[typeObj.type][0].count;
                            }else{
                                typeObj.count=0;
                            }
                        });
                        resolve(allTypes)
                    });
                }
            });
    });

};
that.update = function(id,assetObj){
    return new Promise(function(resolve,reject){
        assetModel.findById(id, function (err, assetModelObj) {
            if (err){
                reject(err)
            }
            else{
                let assetsLinked = assetModelObj._doc.assetsLinked;
                let groups = assetModelObj._doc.groups;
                let serviceAssets = assetModelObj._doc.serviceAssets;
                assetObj.assetsLinked = assetsLinked;
                assetObj.groups = groups;
                assetObj.serviceAssets = serviceAssets;
                assetModelObj.set(assetObj);
                assetModelObj.save(function (err, updatedAsset) {
                    if (err) {
                        reject(err)
                    }
                    else{
                        //resolve(updatedAsset);
                        let id=updatedAsset._doc._id;
                        that.getById(id)
                        .then(function(result){
                            logger.log({level:"info",message:"Updated Following Asset ====> "+JSON.stringify(result._doc)})
                            resolve(result)
                        })
                    }
                });
            }


        });
    })
};
that.updateAssetsLinked = function(id,assetObj){
    return new Promise(function(resolve,reject){
        let assetsLinked = assetObj.assetsLinked;
        assetModel.findOneAndUpdate({ _id: id }, { assetsLinked: assetsLinked },{new: true}, function(err, res) {
            resolve(res);
        });
    })
};
that.updateGroups = function(id,assetObj){
    return new Promise(function(resolve,reject){
        let groups = assetObj.groups;
        assetModel.findOneAndUpdate({ _id: id }, { groups: groups }, function(err, res) {
            resolve(res);
        });
    })
};
that.updateServiceAssetsLinked = function(id,assetObj){
    return new Promise(function(resolve,reject){
        let serviceAssetsLinked = assetObj._doc.serviceAssets;
        assetModel.findOneAndUpdate({ _id: id }, { serviceAssets: serviceAssetsLinked },{new: true}, function(err, res) {
            resolve(res);
        }).populate({path: 'serviceAssets',model: 'serviceAssets'});
    })
};
that.delete = function(id){
    return new Promise(function(resolve,reject){
        assetModel.findById(id, function (err, assetModelObj) {
            assetModel.findByIdAndDelete(id,function(err,res){
                if(err){
                    console.log(err);
                    reject(err);
                }else{
                    resolve(res);
                    logger.log({level:"warn",message:"Removed Following Asset ====> "+JSON.stringify(assetModelObj._doc)})
                }
            })
        })
    })
};

that.search=function(type,searchText,skip,limit){
    return new Promise(function(resolve,reject){
        let aggregationArray=[];
        let condition = [];

        let searchCriteria = getSearchCriteria(searchText);
        if(type){
            type = _.split(type,",");
            let allConditions = [];
            _.each(type,function(types){
                allConditions = _.union(allConditions,searchCriteria[types])
            });
            condition = allConditions;
            getSearchResult(condition, type, aggregationArray, limit, skip)
                .then(function(searchResult){
                    resolve(searchResult)
                })
                .catch(function(error){
                    reject(error)
                });
        }
        else if(!type){
            //Get All Types From System
            assetConfigDao.getAllTypes().then(function(allAssetTypes){
                type = _.keys(_.groupBy(allAssetTypes,"type"));
                let allConditions = [];
                _.each(type,function(types){
                    allConditions = _.union(allConditions,searchCriteria[types])
                });
                condition = allConditions;
                getSearchResult(condition, type, aggregationArray, limit, skip)
                .then(function(searchResult){
                    resolve(searchResult)
                })
                .catch(function(error){
                    reject(error)
                });
            })
        }
    })
};
that.searchCount=function(type,searchText){
    return new Promise(function(resolve,reject){
        let aggregationArray=[];
        let condition = [];
        let searchCriteria = getSearchCriteria(searchText);
        if(type){
            type = _.split(type,",");
            let allConditions = [];
            _.each(type,function(types){
                allConditions = _.union(allConditions,searchCriteria[types])
            });
            condition = allConditions;
            getAssetSearchCount(condition, type, aggregationArray)
            .then(function(searchResult){
                resolve(searchResult)
            })
            .catch(function(error){
                reject(error)
            });
        }
        else if(!type){
            //Get All Types From System
            assetConfigDao.getAllTypes().then(function(allAssetTypes){
                type = _.keys(_.groupBy(allAssetTypes,"type"));
                let allConditions = [];
                _.each(type,function(types){
                    allConditions = _.union(allConditions,searchCriteria[types])
                });
                condition = allConditions;
                getAssetSearchCount(condition, type, aggregationArray)
                .then(function(searchResult){
                    resolve(searchResult)
                })
                .catch(function(error){
                    reject(error)
                });
            })
        }


    })
};

that.getRfIAssetByRSSID = function(RFIDId){
    return new Promise(function(resolve,reject){
        //let carModelInst = new carModel();
        assetModel.findOne({"RFIDId":RFIDId,assetType:constants.ASSETS_TYPES.RF_ID},function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
            .populate({path: 'groups',model: 'assetGroup'})
            .populate({path: 'assetsLinked',model: 'asset'})
            .populate({path: 'serviceAssets',model: 'serviceAssets'});
    })
};


let getAllById = function(allAssetsLinked, resolve) {
    return new Promise(function(resolve,reject){
        let allIds = _.keys(_.groupBy(allAssetsLinked, "_doc._id"));
        that.getAllByIds(allIds,true)
        .then(function (res) {
            resolve(res)
        })
    })
};
that.getAllAssetsLinkedDetails = function(id){
    return new Promise(function(resolve,reject){
        that.getById(id).then(function(assetObj){
            if(assetObj && assetObj._doc && assetObj._doc.assetsLinked){
                let allAssetsLinked = assetObj._doc.assetsLinked;
                if(allAssetsLinked && allAssetsLinked.length>0){
                    getAllById(allAssetsLinked).then(function(allAssets){
                        resolve(allAssets);
                    });
                }else{
                    resolve(" No Assets Linked ")
                }
            }else{
                resolve(" No Assets Found ")
            }

        })
    })
};

that.saveMultiple = function(assets){
    return new Promise(function(resolve,reject){
        let limitData  = limitChecker.getFileData().data;
        let rfIdLimit = parseInt(limitData[constants.ASSETS_TYPES.RF_ID]);
        let beaconLimit = parseInt(limitData[constants.ASSETS_TYPES.BEACON]);
        let groupByType = _.groupBy(assets,"assetType");
        that.getAllCountByTypes()
            .then(function(allCounts){
                let groupCountByAssetType = _.groupBy(allCounts,"type");
                let rfIdTotalCount = groupCountByAssetType[constants.ASSETS_TYPES.RF_ID][0].count;
                let beaconTotalCount = groupCountByAssetType[constants.ASSETS_TYPES.BEACON][0].count;

                if(groupByType[constants.ASSETS_TYPES.RF_ID]){
                    rfIdTotalCount = rfIdTotalCount+groupByType[constants.ASSETS_TYPES.RF_ID].length;
                }
                if(groupByType[constants.ASSETS_TYPES.BEACON]){
                    beaconTotalCount = beaconTotalCount+groupByType[constants.ASSETS_TYPES.BEACON].length;
                }
                let promises = [];
                let message=[];
                let canRfIdBeAdded = rfIdTotalCount<=rfIdLimit && groupByType[constants.ASSETS_TYPES.RF_ID];
                let canBeaconBeAdded = beaconTotalCount<=beaconLimit && groupByType[constants.ASSETS_TYPES.BEACON];
                if(canRfIdBeAdded){
                    let rfIdPromise = multipleInsert(groupByType[constants.ASSETS_TYPES.RF_ID]).then(function (data) {
                        return Q(data);
                    });
                    //limit[constants.ASSETS_TYPES.RF_ID]=false;
                    promises.push(rfIdPromise);
                }else if(groupByType[constants.ASSETS_TYPES.RF_ID]){
                    //limit[constants.ASSETS_TYPES.RF_ID]=true;
                    message.push("Limit Exceeded For RF ID Tag , Current Limit For RF ID "+rfIdLimit)
                }
                if(canBeaconBeAdded){
                    let beaconPromise = multipleInsert(groupByType[constants.ASSETS_TYPES.BEACON]).then(function (data) {
                        return Q(data);
                    });
                    //limit[constants.ASSETS_TYPES.BEACON]=false;
                    promises.push(beaconPromise);
                }else if(groupByType[constants.ASSETS_TYPES.BEACON]){
                    //limit[constants.ASSETS_TYPES.BEACON]=true;
                    message.push("Limit Exceeded For Beacon , Current Limit For Beacon "+beaconLimit)
                }
                if(promises.length>0){
                    Q.all(promises).then(function (res) {
                        let result = [];
                        if(res && res.length===2){
                            result = _.union(res[0],res[1])
                        }else if(res && res.length===1){
                            result = res[0]
                        }
                        let data = {
                            saved:result,
                            error:message
                        };
                        /*console.log(data);*/
                        resolve(data)
                    }).catch(function (err) {
                        reject(err);
                    });
                }else{

                    let data = {
                        saved:[],
                        error:message
                    };
                    resolve(data)
                }
            })
    })
};
function multipleInsert(assets){
    return new Promise(function(resolve,reject){
        assetModel.insertMany(assets,function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res)
            }
        })
    });
}


that.getAllMicroService = function(type,ipAddress){
    return new Promise(function(resolve,reject){
        assetConfigDao.getAllTypes()
            .then(function(types){
                /*let assetTypeByConfig = _.keys(_.groupBy(types,'type'));
                if (type) {
                    assetTypeByConfig = _.intersection(assetTypeByConfig,type);
                }*/
                assetModel.findOne({"gatewayIpAddress":ipAddress})/*
                    .where('assetType')
                    .in(assetTypeByConfig)*/
                    .populate({path: 'groups',model: 'assetGroup'})
                    .populate({path: 'assetsLinked',model: 'asset'})
                    .populate({path: 'serviceAssets',model: 'serviceAssets'})
                    .exec(function(err,res){
                        if(err){
                            reject(err)
                        }else{
                            resolve(res);
                        }
                    });
            });
    });
};


that.bulkImport = function(req,data,fields,type){
    return new Promise(function(resolve,reject){
        const result = excelToJson({
            sourceFile: req.file.path,
            columnToKey: fields
        });
        let getAllKeys = _.keys(result);
        if(result && getAllKeys && getAllKeys.length>0 && result[getAllKeys[0]]){
            let count = 0;
            let assetList = _.map(result[getAllKeys[0]],function(assetData){

                count = count+1;
                if(count !== 1){
                    if(assetData){
                        assetData.assetType = type;
                    }
                }
                return assetData
            });

            let header = _.map(_.keys(assetList[0]),function(data){return assetList[0][data]});
            let sampleHeaders = constants.BULK_UPLOAD[type].SAMPLE_DATA.HEADER
            let isHeadersValid1 = _.difference(sampleHeaders,header);
            let isHeadersValid2 = _.difference(header,sampleHeaders);
            if((isHeadersValid1 && isHeadersValid1.length>0)||(isHeadersValid2 && isHeadersValid2.length>0)){
                reject({err:"Please Check The Headers , take sample for reference ,Check For Following Header "+isHeadersValid1.toString()+" , "+isHeadersValid2.toString()})
            }else{
                if(assetList && assetList.length>0){
                    assetList.slice(1, assetList.length);
                    multipleInsert(assetList).then(function (data) {
                        let length = data.length-1;
                        resolve({status:"success",msg:"SAVED "+length+" "+type.toUpperCase()+" ASSETS"});
                    });
                }else{
                    reject({err:"Empty File"})
                }
            }
        }else{
            reject({err:"Sheet1 Not Found"})
        }
    })
};

that.getSampleBulkUploadData = function(req,res,type){
    res.set('Content-Type', 'application/octet-stream');
    res.set('Content-Disposition', 'disp-ext-type; filename='+type+'_sample.xls;creation-date-parm:'+new Date());
    let workbook = new Excel.stream.xlsx.WorkbookWriter({ stream: res })
    let worksheet = workbook.addWorksheet('some-worksheet');
    worksheet.addRow(constants.BULK_UPLOAD[type].SAMPLE_DATA.HEADER).commit();
    worksheet.addRow(constants.BULK_UPLOAD[type].SAMPLE_DATA.PAYLOAD).commit();
    worksheet.commit();
    workbook.commit();
};

that.getStats = function(startDateInMilliSeconds,endDateInMilliSeconds,assetId,seriesType){
    return new Promise(function(resolve,reject){
        healthCheckDao.getServerStats(startDateInMilliSeconds,endDateInMilliSeconds,assetId,seriesType)
        .then(function(data){
            resolve(data)
        }).catch(function(err){
            reject(err)
        })
    });

}
that.getAllStats = function(startDateInMilliSeconds,endDateInMilliSeconds,assetId,seriesType){
    return new Promise(function(resolve,reject){
        healthCheckDao.getAllServerStats(startDateInMilliSeconds,endDateInMilliSeconds,assetId,seriesType)
            .then(function(data){
                resolve(data)
            }).catch(function(err){
            reject(err)
        })
    });

}



that.getHistoricalStatsById = function(startDateInMilliSeconds,endDateInMilliSeconds,assetId,seriesType,fields){
    return new Promise(function(resolve,reject){
        healthCheckDao.getServerHistoricalStats(startDateInMilliSeconds,endDateInMilliSeconds,assetId,seriesType,fields)
        .then(function(data){
                resolve(data)
            }).catch(function(err){
            reject(err)
        })
    });
}


that.getObdDeviceByUniqueIdentifier = function(identifier){
    return new Promise(function(resolve,reject) {
        assetModel.findOne({"assetType":constants.ASSETS_TYPES.OBD_DEVICE,"uniqueIdentifier":identifier})
            .populate({path: 'groups',model: 'assetGroup'})
            .populate({path: 'assetsLinked',model: 'asset'})
            .populate({path: 'serviceAssets',model: 'serviceAssets'})
            .exec(function(err,res){
                if(err){
                    reject(err)
                }else{
                    resolve(res);
                }
            });
    })
}

module.exports=that;