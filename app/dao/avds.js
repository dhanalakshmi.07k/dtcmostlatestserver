/**
 * Created by suhas on 9/8/18.
 */
let avdsModel = require('../models/avds');
let that = {};
let logger = require('../../config/logger').avdds;
let config = require('../../config/config');
that.save = function(avdsData){
    return new Promise(function(resolve,reject){
        let avdsModelInst = new avdsModel(avdsData);
        avdsModelInst.save(function(err,res){
            if(err){
                reject(err)
            }else{
                logger.log({level:"info",message:"Saved New AVDDS DATA ====> "+JSON.stringify(res)});
                resolve(res);
            }
        })
    })
};

that.getAvddsImage = function(filename,res,host,isSource){
    return new Promise(function(){
        const fs = require('fs');
        let actualFilename = filename;
        const filePath =  config.avdds.imageBasePath+filename;// or any file format
        // Check if file specified by the filePath exists
            fs.exists(filePath, function(exists){
                if (exists) {
                    res.writeHead(200, {"Content-Type": "image/jpeg"});

                    fs.createReadStream(filePath).pipe(res);
                } else {
                    res.writeHead(400, {"Content-Type": "text/plain"});
                    res.end("ERROR File does not exist");
                }
            });

    })
}
module.exports=that;