/**
 * Created by suhas on 14/6/18.
 */

let carModel = require('../models/assets'),
    assetModel = require('../models/assets'),
    constants=require('../../config/constants');
var that = {};
that.save = function(carObj){
    return new Promise(function(resolve,reject){
        var carModelInst = new carModel(carObj);
        carModelInst.assetType = constants.ASSETS_TYPES.CAR;
        carModelInst.save(function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
    })
};

that.search=function(searchText,skip,limit){
    let assetType = constants.ASSETS_TYPES.CAR;
    return new Promise(function(resolve,reject){
        let aggregationArray = [];
        let matchQuery = {
            $match: {$or:[
                {"fleetNo": {$regex:searchText, $options:'i'}},
                {"registrationNumber": {$regex:searchText, $options:'i'}},
                {"vinNumber": {$regex:searchText, $options:'i'}}
            ],"assetType":assetType}
        };
        aggregationArray.push(matchQuery);
        if(limit){
            let limitPipeLine = { $limit: parseInt(limit)};
            aggregationArray.push(limitPipeLine);
        }
        if(skip){
            let skipPipeLine = { $limit: parseInt(skip)};
            aggregationArray.push(skipPipeLine);
        }
        assetModel.aggregate(aggregationArray,function(err, docs){
                if(err){
                    reject(err);
                }else{
                    resolve(docs);
                }
            });

    })

};

that.getByVinNo = function(vinNo){
    return new Promise(function(resolve,reject){
        carModel.findOne({"vinNumber":vinNo},function(err,carObj){
            if(err){
                resolve(null)
            }
            else{
                resolve(carObj)
            }
        })
    })
};
module.exports=that;