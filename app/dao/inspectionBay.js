/**
 * Created by suhas on 25/7/18.
 */
let inspectionBayLogModel = require('../models/inspectionBayLog');
let logger = require('../../config/logger').logger;
let _ = require("lodash");
let constants = require("../../config/constants");
let config = require("../../config/config");
let fs = require("fs");
let path = require("path");
let request = require("request");
let q= require('q');
let moment = require("moment");

let Q = require("q");

let Excel = require('exceljs');
let that = {};

function getSearchCriteria(searchText) {
    return [
        {"car.fleetNo": {$regex: searchText, $options: 'i'}},
        {"car.registrationNumber": {$regex: searchText, $options: 'i'}},
        {"car.vinNumber": {$regex: searchText, $options: 'i'}},
        {"car.make": {$regex: searchText, $options: 'i'}},
        {"driverDetails.modelNo": {$regex: searchText, $options: 'i'}},
        {"driverDetails.mobileNumber": {$regex: searchText, $options: 'i'}},
        {"driverDetails.licenceNo": {$regex: searchText, $options: 'i'}},
        {"driverDetails.driverName": {$regex: searchText, $options: 'i'}},
        {"driverDetails.department": {$regex: searchText, $options: 'i'}},
        {"driverDetails.badgeNo": {$regex: searchText, $options: 'i'}},
        {"driverDetails.driverId": {$regex: searchText, $options: 'i'}},
        {"driverDetails.shiftStart": {$regex: searchText, $options: 'i'}},
        {"rfId": {$regex: searchText, $options: 'i'}}];
}

that.save = function(inspectionBayObject){
    return new Promise(function(resolve,reject){
        let inspectionBayLogModelInst = new inspectionBayLogModel(inspectionBayObject);
        inspectionBayLogModelInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};

that.update = function(inspectionBayObject){
    return new Promise(function(resolve,reject){
        let inspectionBayLogModelInst = new inspectionBayLogModel(inspectionBayObject);
        inspectionBayLogModelInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};

that.getAll = function(skip,limit,search,min){
    return new Promise(function(resolve,reject){
       let inspectionQueryObj =  inspectionBayLogModel.find({});
       if(search && search.length>0){
           inspectionQueryObj.find({$or:getSearchCriteria(search)})
       }else{
           inspectionQueryObj.find({})
       }
        inspectionQueryObj.sort({'updated':-1}).limit(limit).skip(skip);
        inspectionQueryObj.exec(function(err,res){
                if(err){
                    reject(err)
                }else{
                    if(min && min==="true"){
                        getMinifiedData(res).then(function(minifiedData){
                            resolve(minifiedData);
                        })
                    }else{
                        resolve(res);
                    }
                }
            });
    })
};

that.getCount = function(searchText){
    return new Promise(function(resolve,reject){
        let aggregationPipeLine = [];

        if(searchText && searchText.length>0){
            let matchQuery = {
                $match: {
                    $or: getSearchCriteria(searchText)
                }
            };
            aggregationPipeLine.push(matchQuery);
        }
        let groupQuery = {$group: {_id : null,count:{$sum : 1}}};
        let projectQuery = {$project: {"count":1,"_id":0}};
        aggregationPipeLine.push(groupQuery);
        aggregationPipeLine.push(projectQuery);
        inspectionBayLogModel.aggregate(aggregationPipeLine,
            function(err,count){
                if(err){
                    reject(err)
                }else{
                    if(count[0] && count[0]["count"]){
                        resolve(count[0])
                    }else{
                        resolve(0)
                    }

                }
            });
    })
};

that.updateAvddsDetailsByEventNo = function(info){
    inspectionBayLogModel.findOne({"avdds.event.eventnumber":info.eventNo},
    function(err,inspectionObj){
        if(inspectionObj){
            let avddsObj =inspectionObj._doc.avdds;
            avddsObj.image = info.allImageInfo;
            inspectionObj._doc.avdds = avddsObj;
            updateAdds(inspectionObj._doc._id.toString(),avddsObj);
        }
    })


};

that.getAvddsByInspectionId = function(inspectionId){
    return new Promise(function(resolve,reject){
        inspectionBayLogModel.findById({_id:inspectionId})
            .sort({'updated':-1})
            .limit(limit)
            .skip(skip)
            .exec(function(err,res){
                if(err){
                    reject(err)
                }else{
                    resolve(res);
                }
            });
    })

};

function updateAdds(id,avdds){
    inspectionBayLogModel.findOneAndUpdate({ _id: id }, { avdds: avdds }, function(err, res) {
        console.log("updated")
    });
}

function getMinifiedData(data){
    return new Promise(function(resolve,reject){
        let minifiedData = _.map(data,function(inspectionObjVal){
            let inspectionObj = inspectionObjVal._doc;
            let obj = {};
            if(inspectionObj["avdds"]){
                obj["avdds"]=true;
            }else{
                obj["avdds"]=false;
            }
            if(inspectionObj["uvis"]){
                obj["uvis"]=true;
            }else{
                obj["uvis"]=false;
            }
            obj["driverFeedBack"] = inspectionObj["driverFeedBack"];
            obj["startTime"] = inspectionObj["startTime"];
            obj["endTime"] = inspectionObj["endTime"];
            obj["inspectionTime"] = inspectionObj["inspectionTime"];
            obj["overview"] = inspectionObj["overview"];
            obj["groups"] = inspectionObj["groups"];
            if(inspectionObj["engineInspection"]){
                let obd = inspectionObj["engineInspection"].carInfo;
                if(obd){
                    if((obd.dtc_p && obd.dtc_p.length>0) || (obd.dtc_s && obd.dtc_s.length>0)){
                        obj["engineInspection"] = constants.ENGINE_DIAGNOSTICS.STATUS.FAIL
                    }else{
                        obj["engineInspection"] = constants.ENGINE_DIAGNOSTICS.STATUS.PASS
                    }
                }else{
                    bj["engineInspection"] = constants.ENGINE_DIAGNOSTICS.STATUS.NOT_FOUND
                }
            }else{
                obj["engineInspection"] = constants.ENGINE_DIAGNOSTICS.STATUS.NOT_FOUND
            }

            obj["car"]= {};
            if(inspectionObj["car"] && inspectionObj["car"].registrationNumber){
                obj["car"].registrationNumber=inspectionObj["car"].registrationNumber;
            }
            if(inspectionObj["car"] && inspectionObj["car"]._id){
                obj["car"]._id=inspectionObj["car"]._id.toString();
            }
            obj["beacons"]=inspectionObj["beacons"];
            obj["_id"] = inspectionObj["_id"];
            return obj
        });
        resolve(minifiedData)
    })
}



that.getFieldsByInspectionId = function(id,field){
    return new Promise(function(resolve,reject){
        let query = inspectionBayLogModel.findById(id);
        query.select(field);
        query.exec(function (err, details) {
            if (err) return handleError(err);
            resolve(details)
        });
    })
}

that.getByInspectionId = function(id){
    return new Promise(function(resolve,reject){
        let query = inspectionBayLogModel.findById(id);
        query.exec(function (err, details) {
            if (err) return handleError(err);
            resolve(details)
        });
    })
};

that.generateInspectionReport = function (inspectionData,res) {
    try{
        let api = config.reportGenMiroService.END_POINT_PDF_GENERATOR;
        let host = config.reportGenMiroService.host;
        let port = config.reportGenMiroService.port;
        let url = "http://" + host + ":" + port + "/generatePdf";
        if (inspectionData && inspectionData._doc) {

            let data = {
                inspectionData: inspectionData._doc,
                needResponse: true
            };
            const options = {
                url: url,
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            if (inspectionData) {
                request(options, function (err, httpResponse, body) {
                    if(inspectionData && inspectionData._doc && inspectionData._doc._id){
                        let actualFilename = inspectionData._doc._id.toString()+"_reports.pdf";
                        const filePath =  path.join(config.inspectionBay.reportsPath,actualFilename);
                        let inspectionId = inspectionData._doc._id.toString();
                        logger.log({level:"info",message:'generated report for inspection of id '+inspectionId});
                        if(res){
                            res.setHeader('Content-disposition', 'attachment; filename=reports_'+inspectionId+'.pdf');
                            res.writeHead(200, {"Content-Type": "application/pdf"});
                            fs.createReadStream(filePath).pipe(res);
                        }
                    }
                })
            } else {
                if(res){
                    //res.send({message: "Not Valid Inspection Id"})
                }
            }
        }
        else {

            if(res){
                //res.send({message: "Not Valid Inspection Id"})
            }
        }
    }
    catch(e){
        console.log(e.message)
    }
};
that.getReports = function(id,res){
    return new Promise(function(resolve,reject){
        try{

            let actualFilename = id+"_reports.pdf";
            const filePath =  path.join(config.inspectionBay.reportsPath,actualFilename);

            fs.exists(filePath, function(exists){
                if (exists) {
                    res.setHeader('Content-disposition', 'attachment; filename=reports_'+id+'.pdf');
                    res.writeHead(200, {"Content-Type": "application/pdf"});
                    fs.createReadStream(filePath).pipe(res);
                } else {
                    that.getByInspectionId(id).then(function(inspectionData){
                        that.generateInspectionReport(inspectionData, res);
                    })
                }
            });
        }
        catch(e){
            res.send({message:e.message})
        }
    })
};

that.updateEngineDiagnostics  = function(id,engineInspection){
    try{
        inspectionBayLogModel.findOneAndUpdate({ _id: id }, { engineInspection: engineInspection }, function(err, res) {
            console.log("updated obd to previous inspection")
        });
    }
    catch(e){
        console.log(e.message)
    }
};


that.getCountByDays = function(startDate,endDate,series,status,carRegistrationNo){
    return new Promise(function(resolve,reject){
        getCountByDays(startDate,endDate,series,status,carRegistrationNo).then(function(data){
            resolve(data)
        }).catch(function(err){
            reject(err);
        })
    })
};

that.getNewCountByDays = function(startDate,endDate,series,status,carRegistrationNo){
    return new Promise(function(resolve,reject){
        /*getCountBetweenDays(startDate,endDate,series,status,carRegistrationNo)
        .then(function(data){
            resolve(data)
        }).catch(function(err){
            reject(err);
        })*/
        let groupData = {
            "hour":{
                diff:"seconds",value:1*60
            },
            "day":{
                diff:"seconds",value:1*60*24
            },
            "month":{
                diff:"seconds",value:1*60*24*31
            },
            "year":{
                diff:"seconds",value:1*60*24*31*12
            }
        };
        startDate = new Date(startDate);
        endDate = new Date(endDate);
        let startDateForLoop = moment(startDate);
        let endDateForLoop = moment(endDate);
        if(groupData[series]){
            let promises = [];
            do {
                startDateForLoop = moment(startDate);
                endDateForLoop = moment(startDate).subtract(groupData[series].value, groupData[series].diff);
                let promise = getCountBetweenDays(startDateForLoop,endDateForLoop,null)
                    .then(function(){
                        return Q(data);
                    });
                promises.push(promise);
            }
            while (s < 5);
            Q.all(promises).then(function (countData) {

            })
        }
    })
};

function getCountByDays(startDateInMilliSeconds,endDateInMilliSeconds,series,status,carRegNo){
    return new Promise(function(resolve,reject){
        let startDate = new Date(moment(parseFloat(startDateInMilliSeconds)).utc(false));
        let endDate = new Date(moment(parseFloat(endDateInMilliSeconds)).utc(false));
        let dateToConsidered = "updated";
        let groupData = {
            "hour":{
                hour: { $hour: "$"+dateToConsidered },
                day: { $dayOfMonth: "$"+dateToConsidered },
                month: { $month: "$"+dateToConsidered },
                year: { $year: "$"+dateToConsidered }
                },
            "day":{
                day: { $dayOfMonth: "$"+dateToConsidered },
                month: { $month: "$"+dateToConsidered },
                year: { $year: "$"+dateToConsidered }
                },
            "month":{
                month: { $month: "$"+dateToConsidered },
                year: { $year: "$"+dateToConsidered }
                },
            "year":{
                year: { $year: "$"+dateToConsidered }
            },
            /*status:"$overview.status"*/
        };
        let match = {
            "updated" :
                {
                    "$gt" : startDate, "$lt" : endDate
                }
        };
        if(carRegNo){
            match["car.registrationNumber"] = carRegNo;
        }
        let groupedObj = groupData[series];

        if(status){
            groupedObj["status"]="$overview.status";
        };
        inspectionBayLogModel.aggregate(
            [
                {$match:match},
                {$group: {
                        _id : groupedObj,
                        "count":{$sum: 1},
                        "created":{$first: "$"+dateToConsidered}
                    }},
                {$sort: {"created":-1}},
            ],
            function(err,result){
                if(err){
                    reject(err.stack)
                }else if(result && result.length>0){

                    let countResponse = _.map(result,function(data){
                        let dateString = "";
                        let dateSeries = null;
                        let dateGroupObj = data._id;
                        if(series === constants.TIME_SERIES.INSPECTION_REPORT[0]){
                            dateString ='MMM Do YYYY, h a';
                            dateSeries = moment().year(data._id.year).month(data._id.month-1).date(data._id.day).hour(data._id.hour);
                        }
                        if(series === constants.TIME_SERIES.INSPECTION_REPORT[1]){
                            dateString ='MMM Do YYYY';
                            dateSeries = moment().year(data._id.year).month(data._id.month-1).date(data._id.day);

                        }
                        if(series === constants.TIME_SERIES.INSPECTION_REPORT[2]){
                            dateString ='MMM YYYY';
                            dateSeries = moment().year(data._id.year).month(data._id.month-1);
                        }
                        if(series === constants.TIME_SERIES.INSPECTION_REPORT[3]){
                            dateString ='YYYY';
                            dateSeries = moment().year(data._id.year);
                        }

                        let milliSeconds = parseFloat(moment(dateSeries).format("x"));
                        let dateObj = {dateString:moment(dateSeries).format(dateString),count:data.count,milliSeconds:moment.utc(milliSeconds).valueOf()};
                        if(status){
                            dateObj.status = data._id.status;
                        }
                        return dateObj;
                    });
                    let orderedData = _.orderBy(countResponse, ['milliSeconds'], ['asc']);
                    let responseData = _.map(orderedData,function(data){return {dateString:data.dateString,count:data.count,milliSeconds:data.milliSeconds,status:data.status}});
                    if(status){
                        let groupByDateString = _.groupBy(responseData,"dateString");
                        responseData = groupByDateString;
                        /*responseData = */
                    }
                    resolve(responseData);
                    //resolve(result);
                }else if(result && result.length==0){
                    resolve(result);
                    /*console.log(result)*/
                }
            });
    })
}

function getCountBetweenDays(startDate,endDate,carRegNo){
    return new Promise(function(resolve,reject){
        let dateToConsidered = "updated";
        let match = {
            "updated" :
                {
                    "$gt" : startDate, "$lt" : endDate
                }
        };
        if(carRegNo){
            match["car.registrationNumber"] = carRegNo;
        }
        inspectionBayLogModel.aggregate(
            [
                {$match:match},
                {$group: {
                    _id : "$overview.status",
                    "count":{$sum: 1},
                    "created":{$first: "$"+dateToConsidered}
                }},
                {$sort: {"created":-1}},
            ],
            function(err,result){
                if(err){
                    reject(err.stack)
                }else if(result && result.length>0){
                    resolve(result);
                }else if(result && result.length===0){
                    resolve(result);
                }
            });
    })
}
that.getSample = function(noOfSample){
    return new Promise(function(resolve,reject){
        getSample(noOfSample).then(function(result){
            resolve(result);
        })
    })
};
function getSample(noOfSample){
    return new Promise(function(resolve,reject){
        if(noOfSample){
            noOfSample=parseInt(noOfSample)
        }else{
            noOfSample=1
        }
        inspectionBayLogModel.aggregate(
            [ { $sample: { size: noOfSample } } ]
            ,function(err,result){
                if(err){
                    reject(err.stack)
                }else if(result && result.length>0){
                    resolve(result);
                }else if(result && result.length===0){
                    resolve(result);
                }
            }
        )
    })
}


/*that.getSampleBulkUploadData = function(req,res,type){
    res.set('Content-Type', 'application/octet-stream');
    res.set('Content-Disposition', 'disp-ext-type; filename='+type+'_sample.xls;creation-date-parm:'+new Date());
    let workbook = new Excel.stream.xlsx.WorkbookWriter({ stream: res })
    let worksheet = workbook.addWorksheet('some-worksheet');
    worksheet.addRow(constants.BULK_UPLOAD[type].SAMPLE_DATA.HEADER).commit();
    worksheet.addRow(constants.BULK_UPLOAD[type].SAMPLE_DATA.PAYLOAD).commit();
    worksheet.commit();
    workbook.commit();
};*/

that.downloadInspectionData = function(startDate,endDate,series,status,carRegNo,res){
    getCountByDays(startDate,endDate,series,status,carRegNo).then(function(data){
        //res.set('Content-Type', 'application/xls');
        res.set('Content-Disposition', 'disp-ext-type; filename=analytics.xls;creation-date-parm:'+new Date());
        if(data){
            let workbook = new Excel.stream.xlsx.WorkbookWriter({ stream: res })
            let worksheet = workbook.addWorksheet('Inspection');
            worksheet.addRow(["Date","Pass","Check","Fail","Total"]);
            let allDataArray = _.keys(data);
            _.each(allDataArray,function(dateString){
                let analyticsData = data[dateString];
                let groupOverStatus = _.groupBy(analyticsData,"status");
                let allAvailableStatus = _.keys(groupOverStatus);
                let statusData = {
                    check:0,fail:0,pass:0,total:0
                };
                _.each(allAvailableStatus,function(status){
                    statusData[status] = groupOverStatus[status][0].count;
                });
                statusData.total = statusData.check+statusData.pass+statusData.fail;
                worksheet.addRow([dateString,statusData.pass,statusData.check,statusData.fail,statusData.total]);
            });
            worksheet.commit();
            workbook.commit();
        }
    });
};
module.exports=that;
