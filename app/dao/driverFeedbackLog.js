/**
 * Created by suhas on 19/11/18.
 */
let driverFeedbackModel = require('../models/driverFeedback');
let that = {};
let config = require('../../config/config');
that.save = function(driverFeedBack){
    return new Promise(function(resolve,reject){
        let driverFeedBackInst = new driverFeedbackModel(driverFeedBack);
        driverFeedBackInst.save(function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
    })
};