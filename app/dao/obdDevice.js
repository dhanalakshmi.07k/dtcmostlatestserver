/**
 * Created by suhas on 14/6/18.
 */

let nearByLogModel = require('../models/nearbyLogs');
let assetDAO = require('../dao/assets');
let _= require("lodash");
let config= require("../../config/config");
let constant= require("../../config/constants");
let that = {};
that.getAll = function(duration,registered){
    return new Promise(function(resolve,reject){
        let startDate = new Date();
        let currMin = startDate.getMinutes();
        let durationInMinutes = config.RF_ID.LOG.RETRIEVE_SCANNED_DURATION;
        if(duration){
            duration = parseInt(duration);
            durationInMinutes=duration;
        }
        startDate.setMinutes( currMin - durationInMinutes);
        let matchQuery = {
            created: { // 18 minutes ago (from now)
                $gt: new Date(startDate)
            },
            assetType:{
                $eq:constant.ASSETS_TYPES.OBD_DEVICE
            }
        };
        let assetTypeBeacon = [constant.ASSETS_TYPES.CAR];
        assetDAO.getAll(null,null,null,assetTypeBeacon).then(function(registeredCars){
            /*{
             "RFIDId" : "5d0101cc5c1abd1ef63b4d0485105234e1340c1e0073",
             "tagName": "tag1"
             }*/
            let groupRegisteredCarByVin =  _.groupBy(registeredCars,"_doc.vinNumber");
            let registeredCarByVin =  _.keys(groupRegisteredCarByVin);
            nearByLogModel.aggregate([
                    {$match: matchQuery},
                    {$sort: {created: 1}}
                ]
                ,function (err,response){
                    if(err){
                        reject(err)
                    }else{
                        let groupByVin = _.groupBy(response,"vin");
                        let vinList = _.keys(groupByVin);
                        let carDiscoveredObj = _.map(vinList,function(vin){
                            if(groupByVin[vin][0]){
                                let array = groupByVin[vin];
                                let length = array.length;
                                if(groupByVin[vin][length-1]){
                                    groupByVin[vin][length-1].createdInMilliSeconds = new Date(groupByVin[vin][length-1].created).getTime();
                                    groupByVin[vin][length-1].nearByType = assetTypeBeacon;
                                    if(groupByVin[vin][length-1].RFIDId){
                                        let vinId = groupByVin[vin][length-1].vin;
                                        groupByVin[vin][length-1].isRegistered = _.includes(registeredCarByVin,vinId);
                                        if(groupByVin[vin][length-1].isRegistered){
                                            groupByVin[vin][length-1].assetDetails = groupRegisteredCarByVin(vin)[0];
                                        }
                                    }
                                }
                                return groupByVin[vin][length-1]
                            }
                        });

                        if(registered && registered==="true"){
                            carDiscoveredObj = _.remove(carDiscoveredObj,function(obj){
                                return obj.isRegistered
                            });
                        }
                        resolve(carDiscoveredObj);
                    }
                })
        })
    })

};
module.exports=that;