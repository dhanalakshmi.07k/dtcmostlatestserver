/**
 * Created by suhas on 31/7/18.
 */
let beaconException = require("../models/beaconExceptionModel");
let Q = require("q");
let _ = require("lodash");
let that = {};

that.save = function(id){
    return new Promise(function(resolve,reject){
        if(id.length>0){
            let beaconExceptionObjectList = _.map(id,function(obj){
                return {beaconId:obj}
            })
            beaconException.insertMany(beaconExceptionObjectList,function(err,res){
                if(err){
                    console.log(err);
                    reject(err);
                }else{
                    resolve(res)
                }
            })
        }
    })

};
that.delete = function(id){
    return new Promise(function(resolve,reject){
        if(id){
            id = _.split(id,",");
            beaconException.remove({"beaconId":{$in:id}},function(err,res){
                if(err){
                    console.log(err);
                    reject(err);
                }else{
                    resolve(res)
                }
            })
        }
    })

};
that.get = function(){
    return new Promise(function(resolve,reject){
        beaconException.find({},function(err,result){
            resolve(result);
        })
    })

};

module.exports=that;