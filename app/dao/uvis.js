/**
 * Created by suhas on 9/8/18.
 */
let uvisModel = require('../models/uvis');
let config = require('../../config/config');
const fs = require('fs');
let that = {};
/*let moment = require("moment");*/
that.save = function(uvisData){
    return new Promise(function(resolve,reject){
        let uvisModelInst = new uvisModel(uvisData);
        uvisModelInst.save(function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
    })
};

that.getUvisImage = function(filename,res){
    return new Promise(function(){
        let actualFilename = filename;
            const filePath =  config.uvis.imageBasePath+filename;

            fs.exists(filePath, function(exists){
                if (exists) {
                    // Content-type is very interesting part that guarantee that
                    // Web browser will handle response in an appropriate manner.

                    res.writeHead(200, {"Content-Type": "image/jpeg"});
                    fs.createReadStream(filePath).pipe(res);
                } else {
                    res.writeHead(400, {"Content-Type": "text/plain"});
                    res.end("ERROR File does not exist");
                }
            });
    })
}

module.exports=that;