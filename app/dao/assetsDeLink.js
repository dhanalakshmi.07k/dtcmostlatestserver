/**
 * Created by suhas on 28/6/18.
 */

let assetDAO = require("./assets"),
    logger=require("../../config/logger").assets,
    _ = require("lodash")/*,
    sync = require('sync')*/;
that = {};

that.deLink = function(linkObj){
    return new Promise(function(resolve,reject){
        let primaryAsset = linkObj.primaryAsset;
        let assetsToBeDeLinked = linkObj.assetsToBeDeLinked;
        if(!primaryAsset){
            reject("Primary Assets Not Found")
        }
        if(!assetsToBeDeLinked){
            reject("Assets to Be Not Found")
        }
        deLinkAllTOPrimary(assetsToBeDeLinked,primaryAsset)
            .then(function (primaryAssetModified) {
                deLinkPrimaryToAll(assetsToBeDeLinked,primaryAsset)
                    .then(function (allOtherAssetsModified) {
                        allOtherAssetsModified.push(primaryAssetModified);
                        resolve(allOtherAssetsModified)
                    })
            });
    })
};

async function deLinkAllTOPrimary(idList, primaryAssetId){
    return new Promise(function(resolve,reject) {
        "use strict";
        let array =[primaryAssetId]
        assetDAO.getAllByIds(array).then(async function (prAssetObj) {
            let updatedObj = await deLinkAndUpdate(prAssetObj[0],idList);
            resolve(updatedObj);
        });
    })
}
async function deLinkPrimaryToAll(idList, primaryAssetId){
    return new Promise(function(resolve,reject){
        let assetsUpdated = [];
        let allPromises = [];
        assetDAO.getAllByIds(idList)
            .then(function (linkableAssetsArray) {
                if(linkableAssetsArray && linkableAssetsArray.length>0){
                    _.each(linkableAssetsArray,async function (linkableAssetsObj) {
                        let assetsToBeLinked = [];
                        assetsToBeLinked.push(primaryAssetId);
                        let updateObj = deLinkAndUpdate(linkableAssetsObj,assetsToBeLinked);
                        assetsUpdated.push(updateObj);
                        allPromises.push(updateObj);
                    });
                    Promise.all(allPromises).then( function (updatedAssetList) {
                        resolve(updatedAssetList);
                    })
                }
            });
    })
}
function deLinkAndUpdate(assetsObj,assetsToBeLinked){
    "use strict";
    return new Promise(function(resolve,reject) {
        let assetsLinked = assetsObj["_doc"]["assetsLinked"];
        if(assetsLinked && assetsLinked.length>0){
            _.each(assetsToBeLinked,function (id) {
                _.remove(assetsLinked, function(obj) {
                    console.log(obj)
                    return obj.toString() === id.toString();
                });
            });
            assetsObj["_doc"]["assetsLinked"] = assetsLinked;
            assetDAO.updateAssetsLinked(assetsObj._doc._id.toString(),assetsObj._doc)
                .then(function(result){
                    resolve(result)
                })
        }else{
            resolve(assetsObj)

        }
    })

}
module.exports = that;