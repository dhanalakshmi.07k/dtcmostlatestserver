/**
 * Created by suhas on 23/6/18.
 */
let beaconModel = require('../models/assets');
let constants= require("../../config/constants");
let that = {};

that.getAllByBeaconId = function(beaconId){
    return new Promise(function(resolve,reject){
        beaconModel.find({})
        .where('assetType')
        .eq(constants.ASSETS_TYPES.BEACON)
        .where('beaconId')
        .in(beaconId)
        .sort({'updated':1})
        .populate({path: 'groups',model: 'assetGroup'})
        .populate({path: 'assetsLinked',model: 'asset'})
        .exec(function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        });
    })
};

that.isBeaconRegistered = function(beaconUUid){
    return new Promise(function(resolve,reject){
        //var carModelInst = new carModel();
        let type = constants.ASSETS_TYPES.BEACON;
        beaconModel.findOne({beaconId:beaconUUid,assetType:type},function(err,res){
            if(err){
                reject(err)
            }else{
                let  registered  = false;
                if(res){
                    registered = true;
                }
                resolve({isRegistered:registered});

            }
        })
    })
};


that.updateLastSeen = function(beaconId){

    return new Promise(function(resolve,reject){
        beaconModel.findOneAndUpdate({beaconId: beaconId,assetType:constants.ASSETS_TYPES.BEACON}, {$set:
            {lastSeen:new Date().getTime(),updated:new Date().getTime()}}, {new: true}, function(err, doc){
            if(err){
                console.log("Something wrong when updating Beacon Last Seen!");
            }
            if(doc){
                resolve(doc._doc)
            }
        }).populate({path: 'groups',model: 'assetGroup'}).populate({path: 'assetsLinked',model: 'asset'});
    })
}
module.exports=that;