/**
 * Created by suhas on 21/6/18.
 */

let nearByLogsModel = require('../models/nearbyLogs');
let beaconDAO = require('../dao/beacon');
let _= require("lodash");
let config= require("../../config/config");
let constant= require("../../config/constants");
let that = {};

that.save = function(nearByObj){
    return new Promise(function(resolve,reject){
        nearByObj.assetType = constant.ASSETS_TYPES.BEACON;
        let beaconsLogInst = new nearByLogsModel(nearByObj);
        beaconsLogInst.save(function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};
that.get = function(duration,registered){
    return new Promise(function(resolve,reject) {
        let startDate = new Date();
        let currMin = startDate.getMinutes();
        let durationInMinutes = config.BEACON.LOG.RETRIEVE_SCANNED_DURATION;
        if (duration) {
            duration = parseFloat(duration);
            durationInMinutes = duration;
        }
        startDate.setMinutes(currMin - durationInMinutes);
        getAllRecentlySeenBeacons(startDate)
            .then(function(nearByBeacons){
                let groupNearByBeaconsById = _.groupBy(nearByBeacons,"beaconId");
                let nearByBeaconIds = _.keys(groupNearByBeaconsById);
                beaconDAO.getAllByBeaconId(nearByBeaconIds)
                    .then(function(registeredBeacons){
                        let registeredBeaconsGroupedById = _.groupBy(registeredBeacons,"_doc.beaconId");
                        let beaconDiscoveredObj = _.map(nearByBeaconIds,function(beaconIdNearBy){
                                let nearByObj = groupNearByBeaconsById[beaconIdNearBy][0];
                                let savedAsset = registeredBeaconsGroupedById[beaconIdNearBy];
                                nearByObj.isRegistered=false;
                                if(savedAsset && savedAsset[0]){
                                    savedAsset = savedAsset[0];
                                    nearByObj.isRegistered=true;
                                    nearByObj.groups = savedAsset._doc.groups;
                                    nearByObj.assetsLinked = savedAsset._doc.assetsLinked;
                                    nearByObj._id = savedAsset._doc._id;
                                }
                                nearByObj.createdInMilliSeconds = new Date().getTime();
                                nearByObj.createdInMilliSeconds = new Date(nearByObj.lastSeen).getTime();
                                nearByObj.updated = new Date(nearByObj.lastSeen).getTime();
                                return nearByObj
                        });
                        if(registered){
                            if(registered==="true"){
                                beaconDiscoveredObj = _.remove(beaconDiscoveredObj,function(obj){
                                    return obj.isRegistered
                                });
                            }else if(registered==="false"){
                                beaconDiscoveredObj = _.remove(beaconDiscoveredObj,function(obj){
                                    return !obj.isRegistered
                                });
                            }
                        }
                        resolve(beaconDiscoveredObj);
                    })
                    .catch(function (err2) {reject(err2);})
            })
            .catch(function(err){
                reject(err);
            })
    })

};
let getAllRecentlySeenBeacons = function(startDate){
    return new Promise(function(resolve,reject){
        let matchQuery = {
            created: { // 18 minutes ago (from now)
                $gt: new Date(startDate)
            },
            assetType:{
                $eq:constant.ASSETS_TYPES.BEACON
            }
        };
        let aggregationPipeLine = [
            {$match:matchQuery},
            {
                $group: {
                    _id:"$beaconId", "lastSeen":{$last:"$updated"}, "rssi":{$first:"$rssi"},
                    "txPowerLevel":{$first:"$txPowerLevel"}, "localName":{$first:"$localName"}, "beaconType":{$first:"$beaconType"},
                    "uuid":{$first:"$uuid"}, "iBeacon":{$first:"$iBeacon"}, "txPower":{$first:"$txPower"}, "assetType":{$first:"$assetType"}
                }
            },
            {
                $project: {
                    _id:0,"beaconId":"$_id", "updated":"$lastSeen", "ant":"$ant", "lastSeen":1, "rssi":1, "txPowerLevel":1,
                    "localName":1, "beaconType":1, "uuid":1, "iBeacon":1, "txPower":1, "assetType":1
                }
            },

        ];
        nearByLogsModel.aggregate(aggregationPipeLine,function(err,res){
            resolve(res)
        });
    })
};

module.exports=that;