let userDetailsService = require('../dao/userDetails');
let verifyUser = function (username, pwd) {
    return new Promise( function (resolve, reject) {

        userDetailsService.verifyUser(username, pwd).then(function (status) {

            if(status){
                resolve(status);
            }
            else{
                reject(status);
            }
        }).catch((err)=>{
            reject(err);
        });
    })

};

module.exports={
    verify_user:verifyUser
}



