



var bCrypt = require('bcrypt-nodejs');
let that = {};
let userDetailsService = require('../dao/userDetails');



that.getAllUsers = function (ability) {
    return new Promise(function (resolve, reject) {

        userDetailsService.getAllUser(ability).then(function (allUsers) {
            if(allUsers){
                resolve(allUsers)
            }
            else{
                reject(400).send(allUsers)
            }

        }).catch((err)=>{
            reject(err);
        });

    })
};

that.createUser = function (body, ability) {
    return new Promise(function (resolve, reject) {
        userDetailsService.addUser(body, ability).then(function (success) {
            if(success){
                resolve(success);            }

        }).catch((err)=>{
            reject(err);
        });

    })

};

that.isUserNameAlreadyUsed = function(userName){
    return new Promise(function(resolve, reject){

        userDetailsService.isUserNameAlreadyUsed(userName).then(function (status) {

            let duplicate={};
            duplicate.present = status;
            if(status){
                resolve(duplicate);
            }
            else{

                resolve(duplicate)
            }

        }).catch((err)=>{

            reject(err)
        });

    })

};

that.getUserById = function(id){
    return new Promise(function (resolve, reject) {
        userDetailsService.getUserById(id)
            .then(function(updatedUserObject){
                resolve(updatedUserObject)
            }).catch(function(err){
            reject(err)
        });
    })
};
that.updateUserProfile = function(id,userObj){
    return new Promise(function (resolve, reject) {
        userDetailsService.updateUserProfile(id,userObj)
            .then(function(updatedUserObject){
                resolve(updatedUserObject)
            }).catch(function(err){
            reject(err)
        });
    })
}


that.getRoles = function (roles) {
    return new Promise( function (resolve, reject) {
        userDetailsService.allRolesDefinition(roles).then(function (data) {
            if(data){
                resolve(data);
            }
        }).catch((err)=>{ reject(err)});
    })
};

that.updatePassword = function (newPassword, userDetails) {
    return new Promise(function(resolve,reject){
        userDetailsService.updatePassword(newPassword, userDetails)
            .then(function(data){
                resolve(data)
            }).catch(function(err){
            reject(err)
        });
    })
};

that.delete = function (userId,ability) {
    return new Promise(function (resolve, reject) {
        userDetailsService.deleteUser(userId,ability).then(function (delResponse) {
            if(delResponse){
                resolve(delResponse)
            }
        }).catch((err)=>{
            reject(err)
        })

    })
};

module.exports=that;
