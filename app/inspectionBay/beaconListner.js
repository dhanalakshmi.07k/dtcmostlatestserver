/**
 * Created by suhas on 8/8/18.
 */

let activeInspection = require("./activeInspection");
let _ = require("lodash");


let extractRelavantBeaconData = function(beaconObj){
    let nearByBeaconObj = beaconObj;
    let activeInspectionData = activeInspection.getActiveData();
    if(activeInspectionData.car){
        isBeaconBelongsToCar(activeInspectionData.car.assetsLinked,nearByBeaconObj)
            .then(function(isBeaconBelongs){
                if(isBeaconBelongs){
                    isBeaconAlreadyAccounted(nearByBeaconObj)
                        .then(function(isBeaconAccounted){
                            if(!isBeaconAccounted){
                                if(nearByBeaconObj.groups && nearByBeaconObj.groups.length>0){
                                    activeInspectionData.beacons.push(beaconObj);
                                    activeInspectionData.groups.push(nearByBeaconObj.groups[0]._doc);
                                    activeInspection.setActiveData(activeInspectionData);
                                    //console.log("FOUND BEACON LINKed TO CAR"+nearByBeaconObj.groups[0].label);
                                    activeInspection.pushData();
                                }
                            }
                        })
                }
            })
    }
};
let isBeaconBelongsToCar = function(assetsLinkedToCar,beaconObj){
    return new Promise(function(resolve,reject){
        let isBeaconFoundLinkedToCar = false;
        if(assetsLinkedToCar && assetsLinkedToCar.length>0){
            let assetsLinked = _.map(assetsLinkedToCar,function(obj){
                return obj._doc._id.toString()
            });
            let detectedBeaconId = beaconObj._id.toString();
            isBeaconFoundLinkedToCar = _.includes(assetsLinked,detectedBeaconId);
        }
        resolve(isBeaconFoundLinkedToCar);
    })
};
let isBeaconAlreadyAccounted = function(beaconObj){
    return new Promise(function(resolve,reject){
        let beaconId = beaconObj.beaconId;
        let activeInspectionData = activeInspection.getActiveData();
        if(activeInspectionData.beacons.length>0){
            let allBeaconIds = _.keys(_.groupBy(activeInspectionData.beacons,"beaconId"));
            let isBeaconAccounted = _.includes(allBeaconIds,beaconObj.beaconId);
            resolve(isBeaconAccounted)
        }else{
            resolve(false)
        }
    })
}

module.exports = {
    listenToBeacon:extractRelavantBeaconData
};