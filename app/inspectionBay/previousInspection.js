/**
 * Created by suhas on 19/9/18.
 */

let config = require("../../config/config");
let previousInspectionData = null;
let inspectionInterval = null;
let inspectionBayDao = require("../dao/inspectionBay");
let obdLogDao = require("../dao/obdDeviceLog");

let socketConnection = require("../ws/socketIo");

function clearPreviousInspection(){
    previousInspectionData = null;
    stopTimer();
};
let set = function(previousInspection){
    clearPreviousInspection();
    previousInspectionData = previousInspection;
    /*stopTimer();*/
    lookForObdData();
    startTimer();
};
let get = function(){
    return previousInspectionData;
};
let checkAndUpdateWithObd = function(obdData){
    try{
        let canWeUpdate = previousInspectionData && obdData
            && previousInspectionData.car && previousInspectionData.car.vinNumber
            && previousInspectionData.car.vinNumber===obdData.vin && !previousInspectionData.engineInspection;
        if(canWeUpdate){
            previousInspectionData.engineInspection = obdData;
            updateInspectionData(obdData);
        }
    }catch(e){
        console.log(e.message)
    }
};

function startTimer(){
    let maxTimeForInspection = parseInt(config.inspectionBay.maxTimeForPreviousInspection);
    stopTimer();
    //console.log("Previous Inspection Timer Started "+new Date().getTime());
    inspectionInterval = setInterval(function(){
        clearPreviousInspection();
        },1000*60*maxTimeForInspection)
}
function stopTimer(){
    if(inspectionInterval){
        //console.log("Previous Inspection Timer Stopped "+new Date().getTime());
        clearInterval(inspectionInterval)
    }
}
function updateInspectionData(obdData){
    let id = previousInspectionData._id.toString();
    inspectionBayDao.updateEngineDiagnostics(id,obdData);
    clearPreviousInspection();
};
let lookForObdData = function(){
    try{
        if(previousInspectionData.car && previousInspectionData.car.vinNumber){
            let durationInMinutes = 10;
            obdLogDao.getAllWithInGivenMinutesAndVin(durationInMinutes,previousInspectionData.car.vinNumber)
                .then(function(allObdData){
                    if(allObdData && allObdData.length>0 && allObdData[allObdData.length-1]._doc){
                        updateInspectionData(allObdData[allObdData.length-1]._doc);
                    }
                })
        }
    }catch(e){
        console.log(e.message)
    }
};

function pushData(){
    socketConnection.emitToAllSocketConnection(config.SOCKET.TOPIC.INSPECTION_BAY_REPORT_UPDATE,getActiveData())
};
module.exports = {
    set:set,
    checkAndUpdateWithObd:checkAndUpdateWithObd,
};