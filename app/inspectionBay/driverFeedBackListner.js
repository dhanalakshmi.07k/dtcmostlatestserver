/**
 * Created by suhas on 8/8/18.
 */

let driverFeedbackLogDao = require("../dao").driverFeedbackLogModule;
let activeInspection = require("./activeInspection");
let logger = require("../../config/logger").inspection;
let details = {
    driverFeedBack:null,
    driverDetails:null
};
let setDriverFeedBack=function(driverFeedBack){
    let activeData = activeInspection.getActiveData();
    logger.log({level:"info",message:" DRIVER FEED BACK ====> "+JSON.stringify(driverFeedBack)});
    if(activeData.car!==null){
        //activeData.driverFeedBack=driverFeedBack;
        activeInspection.setDriverFeedBack(driverFeedBack);
        details.driverFeedBack = driverFeedBack;
    };
    //driverFeedbackLogDao.save(details)
};

let setDriverDetails=function(driverDetails){
    //activeInspection.active.driverDetails=driverDetails;
    let activeData = activeInspection.getActiveData();
    logger.log({level:"info",message:" DRIVER LOGGED IN =====> "+JSON.stringify(driverDetails)});
    details = {
        driverFeedBack:null,
        driverDetails:null
    };
    if(activeData.car!==null){
       // activeData.driverDetails=driverDetails;
        activeInspection.setDriverDetails(driverDetails);
        details.driverDetails = driverDetails;
    }
};

module.exports = {
    setDriverFeedBack:setDriverFeedBack,
    setDriverDetails:setDriverDetails
};