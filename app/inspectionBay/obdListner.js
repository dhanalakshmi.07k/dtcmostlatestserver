/**
 * Created by suhas on 13/8/18.
 */

let carDao = require("../dao/car");
let _ = require("lodash");
let activeInspection = require("./activeInspection");
let previousInspectionHandler = require("./previousInspection");

let listenToObd =  function(obdData){
    let activeData = activeInspection.getActiveData();
    if(activeData.car){
        if(obdData.vin){
            isObdLinkedToCar(obdData.vin)
                .then(function(carLinked){
                    if(carLinked){
                        let carRelatedToObd = carLinked._doc;
                        activeData = activeInspection.getActiveData();
                        if(activeData.car.vinNumber === carRelatedToObd.vinNumber){
                            //activeData.engineInspection = obdData;
                            activeInspection.setObdData(obdData);
                        }
                    }
                });
        }else if(activeData.car && activeData.car["make"]==="ALTIMA" && !obdData.vin){
            activeData = activeInspection.getActiveData();
            activeInspection.setObdData(obdData);
        }
    }else{
        //previousInspectionHandler.checkAndUpdateWithObd(obdData)
    }

};
let isObdLinkedToCar = function(vin){
    return new Promise(function(resolve,reject){
        carDao.getByVinNo(vin).then(function(isCarLinked){
            resolve(isCarLinked)
        })
    })
};

module.exports = {
    listenToObd:listenToObd,
};