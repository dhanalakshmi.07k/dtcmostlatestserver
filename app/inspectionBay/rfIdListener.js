/**
 * Created by suhas on 8/8/18.
 */
let constants = require("../../config/constants");
let config = require("../../config/config");
let assetsDao = require("../dao/assets");
let _ = require("lodash");
let activeInspection = require("./activeInspection");
let logger = require("../../config/logger").inspection;
let util = require("../util");
let moment = require("moment");

let listenToRfId =  function(rfIdData){
    if(rfIdData.RFIDId){
        isRfIdLinkedToCar(rfIdData.assetsLinked)
            .then(function(res){
                if(res.isRfIdAndCarLinked){
                    assetsDao.getById(res.carId)
                        .then(function(carData){
                            let data = carData._doc;
                            data.lifeSpan = getLifeSpan(data);
                            data.rfIdTag=rfIdData.RFIDId;
                            let activeCarDetails = activeInspection.getActiveData();
                            if(!activeCarDetails.car && !activeCarDetails.rfId && !activeCarDetails.currentRfTagId){
                                //activeCarDetails.startTime=new Date().getTime();
                                activeCarDetails.currentRfTagId=data.rfIdTag;
                                activeInspection.startNewInspection(data);
                            }else if(activeInspection.active.car && isRfIdDetectionTimeElapsed(activeCarDetails)){
                                //activeCarDetails.startTime=new Date().getTime();
                                activeCarDetails.currentRfTagId=data.rfIdTag;
                                //activeInspection.setActiveData(activeCarDetails);
                                activeInspection.startNewInspection(data);
                            }else if(activeCarDetails.car && data.vinNumber!==activeCarDetails.car.vinNumber){
                                //activeCarDetails.startTime=new Date().getTime();
                                activeCarDetails.currentRfTagId=data.rfIdTag;
                                //activeInspection.setActiveData(activeCarDetails);
                                activeInspection.startNewInspection(data);
                            }
                        })
                }
            });
    }

};
let isRfIdLinkedToCar = function(assetsLinked){
    return new Promise(function(resolve,reject){
        let isCarLinked = false;
        let carId = null;
        if(assetsLinked && assetsLinked.length>0){
            let assetsGrouped = _.groupBy(assetsLinked,"_doc.assetType");
            let types = _.keys(assetsGrouped);
            isCarLinked = _.includes(types,constants.ASSETS_TYPES.CAR);
            if(isCarLinked){
                carId = assetsGrouped[constants.ASSETS_TYPES.CAR][0]._id;
            }
        }
        resolve({isRfIdAndCarLinked:isCarLinked,carId:carId})
    })
};
let isRfIdDetectionTimeElapsed=function(activeCarDetails){
    let hasTimeElapsed = true;
    if(activeCarDetails.startTime) {
        let timeDiff = (new Date().getTime() - activeCarDetails.startTime) / 1000;
        hasTimeElapsed = timeDiff > (1000 * 60 * config.RF_ID.INSPECTION_BAY.TIME_BETWEEN_SAME_RF_ID_TAG_SCAN);
        return hasTimeElapsed;
    }else{
        return hasTimeElapsed;
    }
};
function getLifeSpan(carData){
    if(carData){
        if(carData.dateOfExpire && util.date.isValidDate(carData.dateOfExpire)){
            let expiryDate = moment(carData.dateOfExpire,'DD/MM/YYYY');
            let today = moment();
            return expiryDate.diff(today, 'days')
        }else{
            return null
        }
    }else{
        return null
    }
}
module.exports = {
    listenToRfId:listenToRfId,
};