/**
 * Created by suhas on 8/8/18.
 */
let constants = require("../../config/constants");
let config = require("../../config/config");
let inspectionBayDao = require("../dao/inspectionBay");
let _ = require("lodash");
let socketConnection = require("../ws/socketIo");
let logger = require("../../config/logger").inspection;
let assetsDao = require("../dao/assets");
let assetsGroupDao = require("../dao/assetGroup");
let active = {
    rfId:null,
    car:null,
    beacons:[],
    groups:[],
    inspectionTime:new Date().getTime(),
    StarTime:null,
    endTime:null,
    engineInspection:false,
    uvis:false,
    bodyScan:false,
    driverFeedBack:null,
    currentRfTagId:null,
    avdds:null,
    driverDetails:null,
    equipments:[],
    overview:{
        status:constants.INSPECTION_BAY.STATUS.FAIL,
        reasons:[]
    }
};
let inspectionIntervalTOGetObdData = null;
let inspectionInterval = null;
let obdLogDao = require("../dao/obdDeviceLog");/*
let previousInspectionHandler = require("./previousInspection");*/

let mqttConnection = require('../publisher/mqtt');
let clearActiveInspection=function(){
    return new Promise(function(resolve,reject){
       let defaultActive = {
            rfId:null,
            car:null,
            beacons:[],
            groups:[],
            inspectionTime:null,
            StarTime:null,
            endTime:null,
            engineInspection:null,
            uvs:null,
            bodyScan:null,
            driverFeedBack:null,
            currentRfTagId:null,
            avdds:null,
           driverDetails:null,
           equipments:[],
           overview:{
                status:constants.INSPECTION_BAY.STATUS.FAIL,
               reason:""
           }
        };
       setActiveData(defaultActive);
        //pushData();
        resolve(active);
    })

}
let getActiveData = function(){
    //return new Promise(function(resolve,reject){
        return active
   /* });*/
};
let setActiveData = function(modifiedActiveData){
    active=modifiedActiveData;
    pushData()
};

let pushData = function(){
    socketConnection.emitToAllSocketConnection(config.SOCKET.TOPIC.INSPECTION_BAY_UPDATE,getActiveData())
};

let setBodyScan=function(){
    return new Promise(function(resolve,reject){
        active.bodyScan=true;
        pushData();
        resolve(active)
    })
};
let setAvds=function(data){
    return new Promise(function(resolve,reject){
        if(!active.avdds){
            active.avdds=data._doc;
            active.avdds.recievedTime=new Date().getTime();

            /*let avddsPlateNo = data.event.frontlicenseplates.licenseplate.formatted;
             let carPlateNo = active.car.registrationNumber;*/
            active.avdds.match = active.car && active.car.registrationNumber &&
                data.event && data.event.frontlicenseplates && data.event.frontlicenseplates.licenseplate &&
                data.event.frontlicenseplates.licenseplate.formatted &&
                data.event.frontlicenseplates.licenseplate.formatted === active.car.registrationNumber;
            active.avdds.receivedTime = new Date().getTime();
            setActiveData(active);
            pushData();
            resolve(active)
        }
    })
};
let setUvs=function(uvisDoc){
    return new Promise(function(resolve,reject){
        if(!active.uvis){
            let data = uvisDoc._doc;
            active.uvis=data;
            active.uvis.recievedTime=new Date().getTime();
            active.uvis.match = data.plateNumber &&
                active.car && active.car.registrationNumber
                && active.car.registrationNumber === data.plateNumber;
            active.uvis.receivedTime = new Date().getTime();
            setActiveData(active);
            pushData();
            resolve(active)
        }
    })
};
let setEngineInspection=function(){
    return new Promise(function(resolve,reject){
        //active.engineInspection=true;
        //pushData();
        resolve(active)
    })
};

let startNewInspection =  function(data){
    let detectedCar = data;
    let detectedRfId = data.rfIdTag;
    console.log("Inspection Started For RfTag " +
        ""+detectedRfId+" And Car Reg No : " +
        ""+detectedCar.registrationNumber);
    stopCurrentActiveCarInspection()
        .then(function(response){
            startInspectionTimer();
            active.rfId=detectedRfId;
            active.car = detectedCar;
            active.inspectionTime=new Date().getTime();
            active.startTime=new Date().getTime();
            active.currentRfTagId=detectedRfId;
            pushData(active)
        }).catch(function(err){
        startInspectionTimer();
        startInspectionTimerTOGetObdData();
        active.rfId=detectedRfId;
        active.car = detectedCar;
        active.inspectionTime=new Date().getTime();
        active.startTime=new Date().getTime();
        active.currentRfTagId=detectedRfId;
        pushData(active)
    });
};

let saveInspectionDetails = function (resolve) {
    if(active.car){

        inspectionBayDao.save(active)
            .then(function (savedData) {
                console.log("SuccessFully Inspected");
                logger.log({level: "info", message: "SuccessFully Inspected ====> " + JSON.stringify(active)});
                inspectionBayDao.generateInspectionReport(savedData);
                clearActiveInspection()
                    .then(function (activeData) {
                        resolve(savedData);
                    });
            });
    }
};
let triggerDriverReport = function (status) {
    let data = {
        overview: status,
        startTime: new Date(active.startTime),
        endTime: new Date(active.endTime),
        car: {
            registrationNumber: active.car.registrationNumber,
            fleetNumber: active.car.fleetNo
        },
        driver: active.driverDetails
    };
    mqttConnection.publish(constants.SUBSCRIBER.MQTT.TOPIC.VSR_PRINTER_DEVICE, data);
};
let onCurrentInspectionComplete = function (resolve, status) {
    saveInspectionDetails(resolve);
    triggerDriverReport(status);
};
let stopCurrentActiveCarInspection = function(){
    return new Promise(function(resolve,reject){
        stopInspectionTimer();
        if(active.car){
            active.endTime=new Date().getTime();
            getAllEquipmentDetails(active).then(function(equipmentsDetails){
                active.equipments = equipmentsDetails;
                getInspectionStatus(active).then(function(status){
                    active.overview = status;
                    onCurrentInspectionComplete(resolve, status);
                    //resolve();
                })
            });
        }else{
            pushData();
            reject({msg:"No Active Car"});
        }
    })
};


/*Timer To Stop Inspection If not Stopped Manually*/
function startInspectionTimer(){
    let maxTimeForInspection = parseFloat(config.inspectionBay.maxTimeForInspection);
    stopInspectionTimer();
    triggerOnInspectionSTart(active);
    console.log("Inspection Timer Started "+new Date());
    logger.log({level:"info",message:"Started Inspection ====> "+JSON.stringify(active)});
    inspectionInterval = setInterval(function(){
        stopCurrentActiveCarInspection();
        stopInspectionTimer();
        logger.log({level:"warn",message:"Inspection Stopped By Timer As It Exceeded "+maxTimeForInspection+"  Minutes \n Stopped Time"+new Date().getTime()});
    },1000*60*maxTimeForInspection)
}
function stopInspectionTimer(){
    if(inspectionInterval){
        clearInterval(inspectionInterval)
    }
}

/*OBD DATA */
let setObdData = function(obdData){
    if(active.car && obdData.vin && active.car.vinNumber===obdData.vin && !active.engineInspection){
        active.engineInspection=obdData;
        active.engineInspection.receivedTime = new Date().getTime();
        setActiveData(active)
    }
    else if(active.car && active.car["make"]==="ALTIMA" && !obdData.vin){
        active.engineInspection=obdData;

        active.engineInspection.receivedTime = new Date().getTime();
        setActiveData(active)
    }
}

let setAvddsImagePath = function(info){
    if(active.car && active.avdds
        && active.avdds.event
        && active.avdds.event.eventnumber
        && active.avdds.event.eventnumber === info.eventNo){
        active.avdds.image = {};
        active.avdds.image=info.allImageInfo;
        pushData();
    }else{
        updateHistoricalInspection(info)
    }
};
function updateHistoricalInspection(info){
    inspectionBayDao.updateAvddsDetailsByEventNo(info)
}

let getAllEquipmentDetails = function(activeInspection){
    return new Promise(function(resolve,reject){
        let groupByIds = _.groupBy(activeInspection.car.assetsLinked,"_doc._id");
        let allIds = _.keys(groupByIds);
        assetsDao.getAllByIds(allIds,true)
        .then(function(allAssetsLinked){
            let groupByTypes = _.groupBy(allAssetsLinked,"_doc.assetType");
            let allTypes = _.keys(groupByTypes);
            let assetLinked = {};
            _.each(allTypes,function(type){
                assetLinked[type] = _.map(groupByTypes[type],function(assetLinked){ return assetLinked._doc});
            });
            let beaconsLinkedToCar = assetLinked[constants.ASSETS_TYPES.BEACON];
            let beaconsIdFoundOnInspection = _.map(activeInspection.beacons,function(beacon){
                return beacon.beaconId;
            });
            if(beaconsLinkedToCar && beaconsLinkedToCar.length>0){
                assetLinked[constants.ASSETS_TYPES.BEACON] = _.map(assetLinked[constants.ASSETS_TYPES.BEACON]
                    ,function(beacon){
                        let group = beacon.groups;
                        beacon.groupBelonged = "";
                        beacon.found = _.includes(beaconsIdFoundOnInspection,beacon.beaconId);
                        if(group && group.length>0){
                            beacon.groupDetail = group[0]._doc
                        }
                        return beacon
                    })
            }
            resolve(assetLinked)
        })
    })

};
let getInspectionStatus = function(inspectionData){
    return new Promise(function(resolve,reject){
        assetsGroupDao.getAll()
        .then(function(allAssetGroups){
            let inspection ={
                status: constants.INSPECTION_BAY.STATUS.FAIL,
                reasons:[]
            }
            let doesObdStatus = constants.ENGINE_DIAGNOSTICS.STATUS.FAIL;
            let allEquipmentsFound = false;
            let isDriverHasAIssue = false;

            if(inspectionData.groups && inspectionData.groups.length
                ===constants.INSPECTION_BAY.NUMBER_OF_EQUIPMENTS_REQUIRED){
                allEquipmentsFound = true;
            }else{
                allEquipmentsFound = false;
                let groupGroupsFoundByType = _.groupBy(inspectionData.groups,"type");
                let groupFoundKeyData = _.keys(groupGroupsFoundByType);
                let groupAllGroupsByType = _.groupBy(allAssetGroups,"type");
                let allGroupType = _.keys(groupAllGroupsByType);
                let equipmentTypeMissing = _.xor(allGroupType,groupFoundKeyData);
                let equipmentLabel = _.map(equipmentTypeMissing,function(type){
                    return groupAllGroupsByType[type][0].label
                })
                inspection.reasons.push("Equipments Missing :"+equipmentLabel.join(", "));
            }
            if(inspectionData["engineInspection"]) {
                let obd = inspectionData["engineInspection"].carInfo;
                if(obd){
                    if((obd.dtc_p && obd.dtc_p.length>0) || (obd.dtc_s && obd.dtc_s.length>0)){
                        inspection.reasons.push("Found Issue From On Board Diagnostics");
                        doesObdStatus = constants.ENGINE_DIAGNOSTICS.STATUS.FAIL;
                    }else{
                        doesObdStatus = constants.ENGINE_DIAGNOSTICS.STATUS.PASS;
                    }
                }else{
                    doesObdStatus = constants.ENGINE_DIAGNOSTICS.STATUS.PASS;
                }
            }
            else{
                inspection.reasons.push("On-Board Diagnostics Data not Found");
                doesObdStatus = constants.ENGINE_DIAGNOSTICS.STATUS.NOT_FOUND;
            }
            if(inspectionData["driverFeedBack"]){
                let allKeys = _.keys(inspectionData["driverFeedBack"]);
                let driverFeedback = "Driver Has Issue With ";
                let noIssue = true;
                if(allKeys.length===1){
                    if(inspectionData["driverFeedBack"]["receivedTime"]){
                        noIssue = true;
                    }
                }else if(allKeys.length>1){
                    _.each(allKeys,function(key){
                        if(inspectionData["driverFeedBack"][key]==="notWorking"){
                            driverFeedback+=" "+key+" ,";
                            isDriverHasAIssue = true;
                            noIssue = false;
                        }
                    });
                }
                if(!noIssue){
                    inspection.reasons.push(driverFeedback);
                }

            }else{
                isDriverHasAIssue=false;
            }

            if(!allEquipmentsFound || doesObdStatus === constants.ENGINE_DIAGNOSTICS.STATUS.FAIL){
                inspection.status = constants.INSPECTION_BAY.STATUS.FAIL;
                resolve(inspection)
            }
            else if(doesObdStatus === constants.ENGINE_DIAGNOSTICS.STATUS.NOT_FOUND || isDriverHasAIssue){
                inspection.status = constants.INSPECTION_BAY.STATUS.CHECK;
            }
            else{
                inspection.status = constants.INSPECTION_BAY.STATUS.PASS;
            }

            resolve(inspection)
        });
    })
};

function startInspectionTimerTOGetObdData(){
    let maxTimeForInspectionTOGetObdData = 25;
    stopInspectionTimerTOGetObdData();
    console.log("Inspection Timer For Obd Data Started "+new Date());
    logger.log({level:"info",message:"Inspection Timer For Obd Data Started ====> "+JSON.stringify(active)});
    inspectionIntervalTOGetObdData = setTimeout(function(){
        let durationInMinutes = 5;
        lookUpForEngineDiagnostics(durationInMinutes)
        .then(function(allObdData){
            _.each(allObdData,function(obdObj){
                if(obdObj){
                    setObdData(obdObj)
                }
            })
        });
        stopInspectionTimerTOGetObdData();
    },1000*maxTimeForInspectionTOGetObdData)
};
function stopInspectionTimerTOGetObdData(){
    if(inspectionIntervalTOGetObdData){
        clearInterval(inspectionIntervalTOGetObdData)
    }
};

function lookUpForEngineDiagnostics(durationInMinutes){
    return new Promise(function(resolve,reject){
        if(active.car && active.car.vinNumber){
            obdLogDao.getAllWithInGivenMinutesAndVin(durationInMinutes,active.car.vinNumber)
            .then(function(allObdData){
                    resolve(allObdData)
                })
        }
    })
}
function triggerOnInspectionSTart(data){
    mqttConnection.publish(constants.SUBSCRIBER.MQTT.TOPIC.INSPECTION_START, data);
};

let setDriverFeedBack = function(driverFeedBack){
    active.driverFeedBack=driverFeedBack;
    active.driverFeedBack.receivedTime = new Date().getTime();
    setActiveData(active)
};
let setDriverDetails = function(driverDetails){
    active.driverDetails=driverDetails;
};
module.exports = {
    getActiveData:getActiveData,
    setActiveData:setActiveData,
    setObdData:setObdData,
    stopInspectingActiveCar:stopCurrentActiveCarInspection,
    setEngineInspection:setEngineInspection,
    setUvs:setUvs,
    setBodyScan:setBodyScan,
    startNewInspection:startNewInspection,
    active:active,
    pushData:pushData,
    setAvds:setAvds,
    setAvddsImagePath:setAvddsImagePath,
    setDriverFeedBack:setDriverFeedBack,
    setDriverDetails:setDriverDetails
};