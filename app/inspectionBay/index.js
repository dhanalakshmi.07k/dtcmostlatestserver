/**
 * Created by suhas on 24/7/18.
 */

let activeInspection = require("./activeInspection");
let rfIdListener = require("./rfIdListener");
let beaconListener = require("./beaconListner");
let driverFeedBackListener = require("./driverFeedBackListner");
let obdListener = require("./obdListner");
let avddsListener = require("./avdsListner");
let uvisListener = require("./uvsListner");

module.exports = {
    activeInspection:activeInspection,
    driverFeedBackListener:driverFeedBackListener,
    rfIdListener:rfIdListener,
    beaconListener:beaconListener,
    stopInspectingActiveCar:activeInspection.stopInspectingActiveCar,
    listenToRfId:rfIdListener.listenToRfId,
    listenToBeacon:beaconListener.listenToBeacon,
    setDriverFeedBack:driverFeedBackListener.setDriverFeedBack,
    setEngineInspection:activeInspection.setEngineInspection,
    setUvs:activeInspection.setUvs,
    setBodyScan:activeInspection.setBodyScan,
    obdListener:obdListener,
    avddsListener:avddsListener,
    uvisListener:uvisListener
};
