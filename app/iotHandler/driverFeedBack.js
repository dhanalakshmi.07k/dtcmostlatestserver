/**
 * Created by suhas on 19/11/18.
 */
let driverFeedbackLogDao = require("../dao").avds;
let constant = require("../../config/constants");
let inspectionBay = require('../inspectionBay');
let logScanned = function(data){
    console.log("Driver Feedback")
    if(data){
        data = JSON.parse(data);
    }
    if(data){
        inspectionBay.driverFeedBackListener.setDriverDetails(data.driverDetails);
        inspectionBay.driverFeedBackListener.setDriverFeedBack(data.driverFeedBack);
    };
};
module.exports={
    logScanned:logScanned
};