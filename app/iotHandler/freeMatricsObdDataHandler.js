/**
 * Created by suhas on 26/3/19.
 */

let obdDeviceLogDao = require("../dao").obdDeviceGpsModule;
let constant = require("../../config/constants");
let logScanned = function(data){
    let type = constant.ASSETS_TYPES.OBD_DEVICE;
    let data1 = {};
    if(data){
        data = JSON.parse(data);
        data.assetType =type;
        data1 = {
            "deviceId" : data["id"],
            "vin" : data["vin"],
            "ort.yaw":data["ornt.yaw"],
            "ornt.pitch":data["ornt.pitch"],
            "ornt.roll":data["ornt.roll"],
            "acc_X":data["acc_X"],
            "acc_Y":data["acc_Y"],
            "acc_Z":data["acc_Z"]
        };
        if(data["lat"]){data1["latitude"] = parseFloat(data["lat"])}
        if(data["lng"]){data1["longitude"] = parseFloat(data["lng"])}
        if(data["spd"]){data1["speed"] = parseFloat(data["spd"])}
        if(data["alt"]){data1["altitude"] = parseFloat(data["alt"])}
    }
    obdDeviceLogDao.handler(data1)
};

module.exports={
    logScanned:logScanned
};