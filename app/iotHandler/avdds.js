/**
 * Created by suhas on 19/11/18.
 */

let avddsDao = require("../dao").avds;
let inspectionBay = require('../inspectionBay');
let logScanned = function(data){
    if(data){
        data = JSON.parse(data);
    }
    if(data){
        avddsDao.save(data)
            .then(function(res){
                inspectionBay.activeInspection.setAvds(res);
            }).catch(function(err){
            console.warn(err)
        });
    };
};
module.exports={
    logScanned:logScanned
};