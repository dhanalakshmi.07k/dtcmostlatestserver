/**
 * Created by suhas on 21/6/18.
 */
let beaconLogDao = require("../dao").beaconLog;
let beaconDao = require("../dao").beacon;
let constant = require("../../config/constants");
let beaconExceptionList = require("../dao/beaconFilter");
let inspectionBay = require('../inspectionBay');
let logger = require('../../config/logger').nearBy;
let _ = require('lodash');
let logScanned = function(data){
    let type = constant.ASSETS_TYPES.BEACON;
    if(data){
        data = JSON.parse(data);
        data.assetType = type;
    }
    beaconExceptionList.get().then(function(res){
        //console.log(res)
        if(res && res.length>0){
            let allId = _.keys(_.groupBy(res,"beaconId"));
            if(!_.includes(allId,data.beaconId)){
                //console.log(data.assetType);
                saveBeaconsNearBy(data);
            }else{
                logger.log({level:"warn",message:"Found Exception Beacon  ===> "+data.beaconId});
            }

        }else{
            saveBeaconsNearBy(data);
        }
    })
};


let updateBeaconLastSeen = function (beaconId) {
    beaconDao.updateLastSeen(beaconId)
    .then(function(beaconData){
        inspectionBay.listenToBeacon(beaconData);
    }).catch(function(err){});
};
let saveBeaconsNearBy = function (data) {
    logger.log({level:"info",message:"Found Beacons ===> "+JSON.stringify(data)});
    beaconLogDao.save(data)
        .then(function (res) {
            updateBeaconLastSeen(res._doc.beaconId);
        });
};
module.exports={
    logScanned:logScanned
};