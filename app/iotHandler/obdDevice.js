/**
 * Created by suhas on 9/7/18.
 */
let obdDeviceLogDao = require("../dao").obdDeviceLog;
let constant = require("../../config/constants");
let inspectionBay = require('../inspectionBay');
let logger = require("../../config/logger").obd;
let logScanned = function(data){
    let type = constant.ASSETS_TYPES.OBD_DEVICE;
    if(data){
        data = JSON.parse(data);
        data.assetType =type;
    }

    logger.log({level:"info",message:"Found New OBD ===> "+JSON.stringify(data)});
    obdDeviceLogDao.save(data)
        .then(function(savedData){
            inspectionBay.obdListener.listenToObd(data);
        });
};

module.exports={
    logScanned:logScanned
};