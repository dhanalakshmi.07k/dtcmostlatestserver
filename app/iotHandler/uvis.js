/**
 * Created by suhas on 19/11/18.
 */
let uvisDao = require("../dao").uvis;
let inspectionBay = require('../inspectionBay');
let logScanned = function(data){
    if(data){
        data = JSON.parse(data);
    }
    if(data){
        uvisDao.save(data)
            .then(function(res){
                inspectionBay.setUvs(res);
            }).catch(function(err){
        });
    };
};
module.exports={
    logScanned:logScanned
};