/**
 * Created by suhas on 21/6/18.
 */

let rfIdTagLogDao = require("../dao").rfIdLog;
let rfIdTagDao = require("../dao").rfId;
let constant = require("../../config/constants");
let inspectionBay = require('../inspectionBay');
let logger = require("../../config/logger").nearBy;
let logScanned = function(rfId){
    let type = constant.ASSETS_TYPES.RF_ID;
    let data = {
        RFIDId:rfId
    };
    logger.log({level:"info",message:"Found New RFID ===> "+rfId});
    if(rfId){
        data.assetType = type;
        rfIdTagLogDao.save(data)
            .then(function(res){
                updateRfIdLastSeen(res._doc.RFIDId);

            }).catch(function(err){
            console.warn(err)
        });
    };
};

let updateRfIdLastSeen = function (RFIDId) {
    rfIdTagDao.updateLastSeen(RFIDId)
    .then(function(rfIdData){
        inspectionBay.listenToRfId(rfIdData);
    });
};
module.exports={
    logScanned:logScanned
};