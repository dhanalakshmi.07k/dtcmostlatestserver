/**
 * Created by suhas on 21/6/18.
 */
let beaconsDataHandler = require("./beacons"),
    rfIdDataHandler = require("./rfId"),
    obdDeviceDataHandler = require("./obdDevice"),
    healthCheckDataHandler = require("./healthCheck"),
    driverFeedBackDataHandler = require("./driverFeedBack"),
    obdDeviceGpsHandler = require("./obdDeviceGps"),
    freeMatricsObdDataHandler = require("./freeMatricsObdDataHandler"),
    uvisDataHandler = require("./uvis"),
    avddsHandler = require("./avdds");

module.exports={
    beaconsDataHandler:beaconsDataHandler,
    rfIdDataHandler:rfIdDataHandler,
    obdDeviceDataHandler:obdDeviceDataHandler,
    healthCheckDataHandler:healthCheckDataHandler,
    driverFeedBackDataHandler:driverFeedBackDataHandler,
    uvisDataHandler:uvisDataHandler,
    avddsHandler:avddsHandler,
    obdDeviceGpsHandler:obdDeviceGpsHandler,
    freeMatricsObdDataHandler:freeMatricsObdDataHandler

};
