/**
 * Created by suhas on 14/3/19.
 */
let obdDeviceLogDao = require("../dao").obdDeviceGpsModule;
let constant = require("../../config/constants");
let logScanned = function(data){
    let type = constant.ASSETS_TYPES.OBD_DEVICE;
    if(data){
        data = JSON.parse(data);
        data.assetType =type;
    }
    obdDeviceLogDao.handler(data)
};

module.exports={
    logScanned:logScanned
};