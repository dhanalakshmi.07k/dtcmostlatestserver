/**
 * Created by suhas on 13/2/19.
 */
let constant = require("../../config/constants");
let RULES_DEFINED = require("../auth/ability").RULES;
let ROLES_DEFINED = constant.AUTHENTICATION.ROLES;


let mongoose = require('mongoose'),
    config = require('../../config/config'),
    roleModel = require('../models/roles');
let roles = [
    {
        role:ROLES_DEFINED.SUPER_ADMIN,
        landingPage:"asset",
        ability:[
            {
                "subject": RULES_DEFINED.SUBJECT.READ_ROLE,
                "actions": [ROLES_DEFINED.SUPER_ADMIN,ROLES_DEFINED.ADMIN,ROLES_DEFINED.OPERATOR,ROLES_DEFINED.INSPECTION_BAY],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.WRITE_ROLE,
                "actions": [ROLES_DEFINED.SUPER_ADMIN,ROLES_DEFINED.ADMIN,ROLES_DEFINED.OPERATOR,ROLES_DEFINED.INSPECTION_BAY],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.UPDATE_ROLE,
                "actions": [ROLES_DEFINED.ADMIN,ROLES_DEFINED.OPERATOR,ROLES_DEFINED.INSPECTION_BAY],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.DELETE_ROLE,
                "actions": [ROLES_DEFINED.ADMIN,ROLES_DEFINED.OPERATOR,ROLES_DEFINED.INSPECTION_BAY],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.USER,
                "actions": [RULES_DEFINED.ACTION.READ, RULES_DEFINED.ACTION.WRITE, RULES_DEFINED.ACTION.UPDATE, RULES_DEFINED.ACTION.DELETE],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.ASSET,
                "actions": [RULES_DEFINED.ACTION.READ, RULES_DEFINED.ACTION.WRITE, RULES_DEFINED.ACTION.UPDATE, RULES_DEFINED.ACTION.DELETE],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.INSPECTION,
                "actions": [RULES_DEFINED.ACTION.READ, RULES_DEFINED.ACTION.DOWNLOAD, RULES_DEFINED.ACTION.ANALYTICS, RULES_DEFINED.ACTION.LIVE],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.SERVICE,
                "actions": [RULES_DEFINED.ACTION.READ, RULES_DEFINED.ACTION.WRITE, RULES_DEFINED.ACTION.UPDATE, RULES_DEFINED.ACTION.DELETE],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.SETTINGS,
                "actions": [RULES_DEFINED.ACTION.READ, RULES_DEFINED.ACTION.WRITE, RULES_DEFINED.ACTION.UPDATE, RULES_DEFINED.ACTION.DELETE],
                inverted: false
            }
        ]
    },
    {
        role:constant.AUTHENTICATION.ROLES.ADMIN,
        landingPage:"asset",
        ability:[
            {
                "subject": RULES_DEFINED.SUBJECT.READ_ROLE,
                "actions": [ROLES_DEFINED.ADMIN,ROLES_DEFINED.OPERATOR,ROLES_DEFINED.INSPECTION_BAY],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.WRITE_ROLE,
                "actions": [ROLES_DEFINED.ADMIN,ROLES_DEFINED.OPERATOR,ROLES_DEFINED.INSPECTION_BAY],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.UPDATE_ROLE,
                "actions": [ROLES_DEFINED.ADMIN,ROLES_DEFINED.OPERATOR,ROLES_DEFINED.INSPECTION_BAY],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.DELETE_ROLE,
                "actions": [ROLES_DEFINED.OPERATOR,ROLES_DEFINED.INSPECTION_BAY],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.USER,
                "actions": [RULES_DEFINED.ACTION.READ, RULES_DEFINED.ACTION.WRITE, RULES_DEFINED.ACTION.UPDATE, RULES_DEFINED.ACTION.DELETE],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.ASSET,
                "actions": [RULES_DEFINED.ACTION.READ, RULES_DEFINED.ACTION.WRITE, RULES_DEFINED.ACTION.UPDATE, RULES_DEFINED.ACTION.DELETE],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.INSPECTION,
                "actions": [RULES_DEFINED.ACTION.READ, RULES_DEFINED.ACTION.DOWNLOAD, RULES_DEFINED.ACTION.ANALYTICS, RULES_DEFINED.ACTION.LIVE],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.SERVICE,
                "actions": ["read"],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.SETTINGS,
                "actions": [RULES_DEFINED.ACTION.READ],
                inverted: false
            }
        ]
    },
    {
        role:constant.AUTHENTICATION.ROLES.OPERATOR,
        landingPage:"inspectionBay",
        ability:[
            {
                "subject": RULES_DEFINED.SUBJECT.INSPECTION,
                "actions": [RULES_DEFINED.ACTION.READ, RULES_DEFINED.ACTION.ANALYTICS, RULES_DEFINED.ACTION.LIVE],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.SETTINGS,
                "actions": [RULES_DEFINED.ACTION.READ],
                inverted: false
            }]
    },
    {
        role:constant.AUTHENTICATION.ROLES.INSPECTION_BAY,
        landingPage:"inspectionBay",
        ability:[
            {
                "subject": RULES_DEFINED.SUBJECT.INSPECTION,
                "actions": [RULES_DEFINED.ACTION.LIVE],
                inverted: false
            },
            {
                "subject": RULES_DEFINED.SUBJECT.SETTINGS,
                "actions": [RULES_DEFINED.ACTION.READ],
                inverted: false
            }]
    }
];

let db = mongoose.connection;
mongoose.connect(config.mongoConn.url,config.mongoConn.options,function(err,conn){
    /*logger.logger.log({ level: 'error', message: "Mongo DB Connected To "+err});
     logger.logger.log({ level: 'debug', message: "Mongo DB Connected To "+config.mongoConn.url});*/
    console.log(" connection url "+config.mongoConn.url)
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});

function generateAbilities(){
    roleModel.insertMany(roles,function(err,res){
        console.log("Added "+res.length+" Roles ");
        stopExecution();
    })

}
function stopExecution(){
    process.exit(0);
}
function removePreviousData(callBack){
    roleModel.remove({},function(err,result){
        console.log("Deleting previous Roles Config Data");
        generateAbilities(callBack);
    })
}
removePreviousData(function(){
    console.log("Started generating Data")
});
module.exports ={
    run:removePreviousData
};