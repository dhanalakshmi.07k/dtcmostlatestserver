/**
 * Created by suhas on 30/7/18.
 */
let mongoose = require('mongoose'),
    config = require('../../config/config');
let assetModel = require('../models/assets');
let _ = require("lodash");
let fs = require("fs");
let db = mongoose.connection;
mongoose.connect(config.db,function(){
    console.log("MongoDb Connected On  "+config.db);
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});
function generateSensorConfiguration(){
   /* let allNormalCarInfo = fs.readFileSync("./data/dtcCarInfo.json", "utf8");
    let allDtdCarInfo = fs.readFileSync("./data/htdTaxi.json", "utf8");
    let allLimoCarInfo = fs.readFileSync("./data/limDtcCar.json", "utf8");
    let allLimoCarInfo = fs.readFileSync("./data/limDtcCar.json", "utf8");
    allNormalCarInfo = JSON.parse(allNormalCarInfo);
    allDtdCarInfo = JSON.parse(allDtdCarInfo);
    allLimoCarInfo = JSON.parse(allLimoCarInfo);*/
    //let allCars = _.union(allNormalCarInfo,allDtdCarInfo,allLimoCarInfo);

    let carInfo = fs.readFileSync("./data/newCar8Aug2018.json", "utf8");
    carInfo = JSON.parse(carInfo);
    let count=0;
    let validCarCount=0;
   // console.log(carInfo)
    _.each(carInfo,function(carData){
        /*if(carData.MODEL>=2017){*/
            let data =  {
                "vinNumber": carData["Chassis No"],
                "name": carData["Register No"],
                "carNo": carData["Register No"],
                "fleetNo":carData["Vehicle No"],
                "make":carData["Type Of Vehicle"],
                "modelNo":carData["MODEL"],
                "dutyType":carData["Type Of Plat"],
                "assetType":"car",
                "dateOfIssue":carData["Date Of Issue"],
                "dateOfExpire":carData["Date Of Expire"],
                "SL": carData["SL"],
                "typeOfPlate":carData["Type Of Plat"],
            };
            if(carData["Vehicle No"]){
                data.fleetNo=carData["Vehicle No"];
            }
            if(carData["ENG"]){
                data.engineNo=carData["ENG"];
            }
            if(carData["Side no"]){
                data.fleetNo=carData["Side no"];
            }
            if(carData["Register No"]){
                data.registrationNumber= carData["Register No"].toString();
            }else{
                console.log("No Register No For")
                console.log(data)
            }
            validCarCount =validCarCount+1;
            let assetModelInst = new assetModel(data);
            assetModelInst.save({},function(err,res){
                console.log("Added "+count+" Asset Group");
                count=count+1;
               // console.log("loaded car info of reg-no :"+res._doc.registrationNumber);
                if(validCarCount===count){
                    console.log("Total no Of Cars inserted :"+validCarCount);
                    stopExecution();
                }
                /*stopExecution();*/
            })
       /* }*/
    })

}
function stopExecution(){
    process.exit(0);
}

generateSensorConfiguration();