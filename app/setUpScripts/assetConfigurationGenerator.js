/**
 * Created by Suhas on 7/5/2016.
 */
let mongoose = require('mongoose'),
    config = require('../../config/config'),
    constants = require('../../config/constants');
let sensorConfigModel = require('../models/assetConfiguration');

let db = mongoose.connection;
/*mongoose.connect(config.db,function(){
 console.log("MongoDb Connected On  "+config.db);
 });*/
mongoose.connect(config.mongoConn.url,config.mongoConn.options,function(err,conn){
    /*logger.logger.log({ level: 'error', message: "Mongo DB Connected To "+err});
     logger.logger.log({ level: 'debug', message: "Mongo DB Connected To "+config.mongoConn.url});*/
    console.log(" connection url "+config.mongoConn.url)
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});

let sensorConfiguration = [
    {
        "assetType" :{
            "typeId":"car",
            "label":"Car",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            config:{
                assetsLinkable:[constants.ASSETS_TYPES.RF_ID,
                    constants.ASSETS_TYPES.BEACON,
                    constants.ASSETS_TYPES.OBD_DEVICE,
                    constants.ASSETS_TYPES.DRIVER],
                isServiceLinkable:false,
                isBulkUpload:true,
                showStats:false
            },
            order:1
        },
        "configuration" : {
            "make" : {
                "type" : "text",
                "field":"make",
                "description" : "Enter Manufacturer Name",
                "label":"Make",
                "required":false,
                "autoComplete":false
            },
            "modelNo" : {
                "field":"modelNo",
                "type" : "text",
                "description" : "Enter Model Number",
                "label":"Model Number",
                "required":false,
                "autoComplete":false
            },
            "registrationNumber" : {
                "field":"registrationNumber",
                "type" : "text",
                "description" : "Enter Registration Number",
                "label":"Registration Number",
                "required":true,
                "autoComplete":false,
                "assetName":true,
                "showInCard":true
            },
            "vinNumber" : {
                "field":"vinNumber",
                "type" : "text",
                "description" : "Enter VIN Number",
                "label":"VIN Number",
                "required":true,
                "autoComplete":false,
                "showInCard":true
            },
            "fleetNo" : {
                "field":"fleetNo",
                "type" : "text",
                "description" : "Enter Fleet Number",
                "label":"Fleet Number",
                "required":true,
                "autoComplete":false,
                "showInCard":true
            },
            "dateOfIssue" : {
                "field":"dateOfIssue",
                "type" : "text",
                "description" : "Enter Date Of Issue",
                "label":"Date Of Issue (DD/MM/YYYY)",
                "required":true,
                "autoComplete":false
            },
            "dateOfExpire" : {
                "field":"dateOfExpire",
                "type" : "text",
                "description" : "Enter Date Of Expire",
                "label":"Date Of Expire (DD/MM/YYYY)",
                "required":true,
                "autoComplete":false
            },
            "typeOfPlate" : {
                "field":"typeOfPlate",
                "type" : "text",
                "description" : "Enter Type Of Plate",
                "label":"Type Of Plate",
                "required":true,
                "autoComplete":false
            },
            "engineNo" : {
                "field":"engineNo",
                "type" : "text",
                "description" : "Enter Engine No",
                "label":"Engine No",
                "required":false,
                "autoComplete":false
            }
        }
    },
    {
        "assetType" :{
            "typeId":"driver",
            "label":"Driver",
            theme:{
                color:{
                    primary:"#66BB6A",
                    secondary:"#81C784"
                }
            },
            config:{
                assetsLinkable:[constants.ASSETS_TYPES.CAR],
                isServiceLinkable:false,
                isBulkUpload:true,
                showStats:false
            },
            order:2
        },
        "configuration" : {
            "driverId" : {
                "field":"driverId",
                "type" : "text",
                "assetName":true,
                "description" : "Enter Driver Id",
                "label":"Driver Id",
                "required":false,
                "autoComplete":true,
                "showInCard":true
            },
            "nationality" : {
                "field":"nationality",
                "type" : "text",
                "description" : "Enter Nationality",
                "label":"Nationality",
                "required":false,
                "autoComplete":true
            },
            "gender" : {
                "field":"gender",
                "type" : "dropDown",
                "description" : "Select Gender",
                "label":"Gender",
                "required":false,
                "autoComplete":true,
                "value":{
                    "dropDownValues": ["Male","Female"],
                    "defaultValue":"Male"
                }
            },
            "driverName" : {
                "field":"driverName",
                "type" : "text",
                "description" : "Enter Driver Name",
                "label":"Driver Name",
                "required":false,
                "autoComplete":true,
                "showInCard":true
            },
            "department" : {
                "field":"department",
                "type" : "text",
                "description" : "Enter Department",
                "label":"Department",
                "required":false,
                "autoComplete":true
            },
            "dateOfJoin" : {
                "field":"dateOfJoin",
                "type" : "date",
                "description" : "Enter Date Of Join",
                "label":"Date Of Join",
                "required":false,
                "autoComplete":true
            },
            "accommodation" : {
                "field":"accommodation",
                "type" : "date",
                "description" : "Enter Your Accommodation",
                "label":"Accommodation",
                "required":false,
                "autoComplete":true
            },
            "licenceNo" : {
                "field":"licenceNo",
                "type" : "text",
                "description" : "Enter Licence Number",
                "label":"Licence Number",
                "required":false,
                "autoComplete":true
            },
            "mobileNo" : {
                "field":"mobileNo",
                "type" : "number",
                "description" : "Enter Mobile Number",
                "label":"Mobile Number",
                "required":false,
                "autoComplete":true
            },
            "dutyType" : {
                "field":"dutyType",
                "type" : "dropDown",
                "description" : "Enter Duty Type",
                "label":"Duty Type",
                "required":false,
                "value":{
                    "dropDownValues": ["Hatta Taxi","Ladies and Family","Limousine","Off-road","Public Taxi","Special Needs"],
                    "defaultValue":"Hatta Taxi"
                },
                "autoComplete":false
            },
            "terminated" : {
                "field":"terminated",
                "type" : "text",
                "description" : "Is Terminated",
                "label":"Terminated",
                "required":false,
                "autoComplete":true
            },
            "shiftType" : {
                "field":"shiftType",
                "type" : "text",
                "description" : "Enter Shift Type",
                "label":"Shift Type",
                "required":false,
                "autoComplete":true,
                "showInCard":true
            },
            "radioJobs" : {
                "field":"radioJobs",
                "type" : "text",
                "description" : "IS Radio Jobs",
                "label":"Radio Jobs",
                "required":false,
                "autoComplete":true
            },
            "shiftStart" : {
                "field":"shiftStart",
                "type" : "text",
                "description" : "Enter Shift Start",
                "label":"Shift Start",
                "required":false,
                "autoComplete":true,
                "showInCard":true
            }
        }

    },
    {
        "assetType" :{
            "typeId":"rfId",
            "label":"RF ID Tag",
            theme:{
                color:{
                    primary:"#dc786b",
                    secondary:"#d27f72c2"
                }
            },
            config:{
                assetsLinkable:[constants.ASSETS_TYPES.CAR],
                isServiceLinkable:false,
                isBulkUpload:false,
                showStats:false
            },
            order:3
        },
        "configuration" : {
            "make" : {
                "field":"make",
                "type" : "text",
                "description" : "Enter Manufacturer Name",
                "label":"Make",
                "required":false,
                "autoComplete":true
            },
            "modelNo" : {
                "field":"modelNo",
                "type" : "text",
                "description" : "Enter Model Number",
                "label":"Model Number",
                "required":false,
                "autoComplete":true
            },
            "type" : {
                "field":"type",
                "type" : "dropDown",
                "description" : "Enter Model Number",
                "label":"Model Type",
                "required":false,
                "value":{
                    "dropDownValues":["Active","Passive"],
                    "defaultValue":"Active"
                },
                "autoComplete":false
            },
            "RFIDId" : {
                "field":"RFIDId",
                "type" : "text",
                "description" : "Enter Unique Identifier",
                "label":"Id",
                "required":false,
                "autoComplete":false,
                "assetName":true,
                "showInCard":true
            }
        }

    },
    {
        "assetType" :{
            "typeId":"obdDevice",
            "label":"Obd Device",
            theme:{
                color:{
                    primary:"#26A69A",
                    secondary:"#4DB6AC"
                }
            },
            config:{
                assetsLinkable:[constants.ASSETS_TYPES.CAR],
                isServiceLinkable:false,
                isBulkUpload:false,
                showStats:false
            },
            order:6
        },
        "configuration" : {
            "make" : {
                "field":"make",
                "type" : "text",
                "description" : "Enter Manufacturer Name",
                "label":"Make",
                "required":false,
                "autoComplete":true,
                "showInCard":true
            },
            "modelNo" : {
                "field":"modelNo",
                "type" : "text",
                "description" : "Enter Model Number",
                "label":"Model Number",
                "required":false,
                "autoComplete":true
            },
            "uniqueIdentifier" : {
                "field":"uniqueIdentifier",
                "type" : "text",
                "description" : "Unique Identifier",
                "label":"IMEI Number / UId",
                "required":false,
                "autoComplete":false,
                "showInCard":true
            },
            "gsmId" : {
                "field":"gsmId",
                "type" : "text",
                "description" : "Enter GSM Id",
                "label":"GSM Id",
                "required":false,
                "autoComplete":false,
                "assetName":true
            }
        }
    },
    {
        "assetType" :{
            "typeId":"beacon",
            "label":"Beacon",
            theme:{
                color:{
                    primary:"#42A5F5",
                    secondary:"#64B5F6"
                }
            },
            config:{
                assetsLinkable:[constants.ASSETS_TYPES.CAR],
                isServiceLinkable:false,
                isBulkUpload:false,
                showStats:false
            },
            order:5
        },
        "configuration" : {
            "beaconType" : {
                "field":"beaconType",
                "type" : "text",
                "description" : "Enter Beacon Type",
                "label":"Beacon Type",
                "required":false,
                "autoComplete":true,
                "showInCard":true
            },
            "beaconId" : {
                "field":"beaconId",
                "type" : "text",
                "description" : "Enter Beacon Id",
                "label":"Beacon Id",
                "required":false,
                "autoComplete":true,
                "showInCard":true
            },
            "rssi":{
                "field":"rssi",
                "type" : "number",
                "description" : "Enter RSSI Signal(db) ",
                "label":"RSSI Signal(db) ",
                "required":false,
                "autoComplete":false,
                "showInCard":true
            },
        }
    },
    {
        "assetType" : {
            "typeId" : "beaconGateway",
            "label" : "Gateway",
            theme:{
                color:{
                    primary:"#d05fb1",
                    secondary:"#ce84ba"
                }
            },
            config:{
                assetsLinkable:[],
                isServiceLinkable:true,
                isBulkUpload:false,
                showStats:true
            },
            order:7
        },
        "configuration" : {
            "gatewayName" : {
                "field" : "gatewayName",
                "type" : "text",
                "description" : "Enter Gateway Name",
                "label" : "Gateway Name",
                "required" : false,
                "autoComplete" : false,
                "assetName" : true,
                "showInCard":true
            },
            "receiverIpAddress" : {
                "field" : "receiverIpAddress",
                "type" : "text",
                "description" : "Enter Receiver Ip Address",
                "label" : "receiver IP Address",
                "required" : true,
                "autoComplete" : false
            },
            "receiverTopicName" : {
                "field" : "receiverTopicName",
                "type" : "text",
                "description" : "Enter Receiver Topic Name",
                "label" : "Receiver Topic Name",
                "required" : true,
                "autoComplete" : false
            },
            "receiverProtocol" : {
                "field" : "receiverProtocol",
                "type" : "dropDown",
                "description" : "Enter Protocol",
                "label" : "Receiver Protocol",
                "required" : true,
                "autoComplete" : false,
                "value":{
                    "dropDownValues":["mqtt"],
                    "defaultValue":"mqtt"
                },
            },
            "macAddress" : {
                "field" : "macAddress",
                "type" : "text",
                "description" : "Enter Mac Address",
                "label" : "Mac Address",
                "required" : false,
                "autoComplete" : false
            },
            "gatewayIpAddress" : {
                "field" : "gatewayIpAddress",
                "type" : "text",
                "description" : "Enter gateway Ip Address",
                "label" : "IP Address",
                "required" : true,
                "autoComplete" : false,
                "showInCard":true
            },
            "gatewayProtocol" : {
                "field" : "gatewayProtocol",
                "type" : "text",
                "description" : "Enter gateway Protocol",
                "label" : "Protocol",
                "required" : true,
                "autoComplete" : false
            }
        }
    },
    {
        "assetType" : {
            "typeId" : "rfidGateway",
            "label" : "RFID Gateway",
            theme:{
                color:{
                    primary:"#1d6ad0",
                    secondary:"#5D8ED0"
                }
            },
            config:{
                assetsLinkable:[],
                isServiceLinkable:true,
                isBulkUpload:false,
                showStats:true
            },
            order:8
        },
        "configuration" : {
            "gatewayName" : {
                "field" : "gatewayName",
                "type" : "text",
                "description" : "Enter Gateway Name",
                "label" : "Name",
                "required" : false,
                "autoComplete" : false,
                "assetName" : true,
                "showInCard":true
            },
            "receiverIpAddress" : {
                "field" : "receiverIpAddress",
                "type" : "text",
                "description" : "Enter Receiver Ip Address",
                "label" : "receiver IP Address",
                "required" : true,
                "autoComplete" : false
            },
            "receiverTopicName" : {
                "field" : "receiverTopicName",
                "type" : "text",
                "description" : "Enter Receiver Topic Name",
                "label" : "Receiver Topic Name",
                "required" : true,
                "autoComplete" : false
            },
            "receiverProtocol" : {
                "field" : "receiverProtocol",
                "type" : "dropDown",
                "description" : "Enter Protocol",
                "label" : "Receiver Protocol",
                "required" : true,
                "autoComplete" : false,
                "value":{
                    "dropDownValues":["mqtt"],
                    "defaultValue":"mqtt"
                },
            },
            "gatewayIpAddress" : {
                "field" : "gatewayIpAddress",
                "type" : "text",
                "description" : "Enter Ip Address",
                "label" : "gateway IP Address",
                "required" : true,
                "autoComplete" : false,
                "showInCard":true
            },
            "gatewayProtocol" : {
                "field" : "gatewayProtocol",
                "type" : "text",
                "description" : "Enter Protocol",
                "label" : "gateway Protocol",
                "required" : true,
                "autoComplete" : false
            }
        }
    },
    {
        "assetType" : {
            "typeId" : "server",
            "label" : "Server",
            theme:{
                color:{
                    primary:"#d09800",
                    secondary:"#d0cc53"
                }
            },
            config:{
                assetsLinkable:[],
                isServiceLinkable:true,
                isBulkUpload:false,
                showStats:true
            },
            order:9
        },
        "configuration" : {
            "name" : {
                "field" : "name",
                "type" : "text",
                "description" : "Enter Server Name",
                "label" : "Name",
                "required" : true,
                "autoComplete" : false,
                "showInCard":true
            },
            "gatewayIpAddress" : {
                "field" : "gatewayIpAddress",
                "type" : "text",
                "description" : "Enter Ip Address",
                "label" : "IP Address",
                "required" : true,
                "autoComplete" : false,
                "showInCard":true
            }
        }
    },
    /*{
        "assetType" : {
            "typeId" : "gateway",
            "label" : "gateway",
            theme:{
                color:{
                    primary:"#312dd0",
                    secondary:"#8899d0"
                }
            },
            config:{
                assetsLinkable:[],
                isServiceLinkable:true,
                isBulkUpload:false,
                showStats:true
            },
            order:9
        },
        "configuration" : {
            "name" : {
                "field" : "name",
                "type" : "text",
                "description" : "Enter Server Name",
                "label" : "Name",
                "required" : true,
                "autoComplete" : false,
                "showInCard":true
            },
            "gatewayIpAddress" : {
                "field" : "gatewayIpAddress",
                "type" : "text",
                "description" : "Enter Ip Address",
                "label" : "IP Address",
                "required" : true,
                "autoComplete" : false,
                "showInCard":true
            }
        }
    }*/
];
function generateSensorConfiguration(){
    sensorConfigModel.insertMany(sensorConfiguration,function(err,res){
        console.log("Added "+res.length+" Asset Configuration");
        stopExecution();
    })

}
function stopExecution(){
    process.exit(0);
}
function removePreviousData(callBack){
    sensorConfigModel.remove({},function(err,result){
        console.log("Deleting previous Asset Config Data");
        generateSensorConfiguration(callBack);
    })
}
removePreviousData(function(){
    console.log("Started generating Data")
});
module.exports ={
    run:removePreviousData
};

