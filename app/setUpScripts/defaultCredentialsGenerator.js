/**
 * Created by suhas on 4/8/18.
 */
let mongoose = require('mongoose'),
    config = require('../../config/config');
let userModel = require('../models/userDetails');
let _ = require("lodash");
let fs = require("fs");
let db = mongoose.connection;

mongoose.connect(config.mongoConn.url,config.mongoConn.options,function(){
    console.log("Mongo DB Connected To "+config.mongoConn.url);
});

db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});

function saveDefaultCredentials(){
    let data =  {
        "username": "admindtc",
        "password": "$2a$10$/YAHf4oyE6kfbmLLVxN57OsxKS32P/peEUYMFbhk.2FDLBcMMPRC.",
        "email":"admin@gmail.com",
        "firstName":"admin",
        "lastName":"dtc",
        "roles":["admin"]
    };
    let operator = {
        "username": "operatordtc",
        "password": "$2a$10$fDQM8ouFtDtluJqm3ylr3u1ck1/teAYJ6JNL20878Ek6ivUuSBIX.",
        "firstName": "operator",
        "lastName": "dtc",
        "email": "operator@gmail.com",
        "roles":["operator"]
    };
    let superAdmin = {
        "username": "superadmin",
        "password": "$2a$10$/YAHf4oyE6kfbmLLVxN57OsxKS32P/peEUYMFbhk.2FDLBcMMPRC.",
        "firstName": "operator",
        "lastName": "dtc",
        "email": "superadmin@gmail.com",
        "roles":["superadmin"]
    }

    let userModelInst = new userModel(operator);
    userModelInst.save({},function(err,res){
        stopExecution();
    })

}
function stopExecution(){
    process.exit(0);
}

saveDefaultCredentials();