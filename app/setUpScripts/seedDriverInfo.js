/**
 * Created by suhas on 4/8/18.
 */
let mongoose = require('mongoose'),
    config = require('../../config/config');
let assetModel = require('../models/assets');
let _ = require("lodash");
let fs = require("fs");
let db = mongoose.connection;
mongoose.connect(config.db,function(){
    console.log("MongoDb Connected On  "+config.db);
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});
function saveDriverInfo(){
    let allDriverInfo = fs.readFileSync("./data/driverInfo.json", "utf8");
    allDriverInfo = JSON.parse(allDriverInfo);
    let count=0;
    let validCarCount=0;
    _.each(allDriverInfo,function(driverData,index){
        let driverName = "driver"+index;
            let data =  {
                "assetName": /*driverData["Driver Name"]*/driverName,
                "terminated": driverData["Terminated"],
                "shiftType":driverData["Shift Type"],
                "radioJobs":driverData["Radio Jobs"],
                "shiftStart":driverData["Shift Start"],
                "driverName":/*driverData["Driver Name"]*/driverName,
                "driverId":driverData["Driver ID"].toString(),
                "assetType":"driver",
            };
            validCarCount =validCarCount+1;
            let assetModelInst = new assetModel(data);
            assetModelInst.save({},function(err,res){
                console.log("Added "+count+" Asset");
                count=count+1;
                console.log("loaded Driver info of reg-no :"+res.driverId);
                if(validCarCount===count){
                    console.log("Total no Of Cars inserted :"+validCarCount);
                    stopExecution();
                }
                /*stopExecution();*/
            })
    })

}
function stopExecution(){
    process.exit(0);
}

saveDriverInfo();