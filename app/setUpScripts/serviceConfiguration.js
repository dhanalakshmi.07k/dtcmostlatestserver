/**
 * Created by suhas on 12/10/18.
 */
/**
 * Created by Suhas on 7/5/2016.
 */
let mongoose = require('mongoose'),
    config = require('../../config/config');
let serviceAssetModel = require('../models/serviceAssetsConfig');

let db = mongoose.connection;
/*mongoose.connect(config.db,function(){
 console.log("MongoDb Connected On  "+config.db);
 });*/
mongoose.connect(config.mongoConn.url,config.mongoConn.options,function(err,conn){
    /*logger.logger.log({ level: 'error', message: "Mongo DB Connected To "+err});
     logger.logger.log({ level: 'debug', message: "Mongo DB Connected To "+config.mongoConn.url});*/
    console.log(" connection url "+config.mongoConn.url)
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});

let assetserviceConfiguration = [
    {
        "serviceType" : {
            "typeId" : "general",
            "label" : "General",
            theme:{
                color:{
                    primary:"#d024a3",
                    secondary:"#d06cc1"
                }
            },
            config:{
                assetsLinkable:true,
                management:{
                    actions:[{label:"Start",value:"start"},
                        {label:"Restart",value:"restart"},
                        {label:"Stop",value:"stop"}]
                }
            },
            order:1
        },
        "configuration" : {
            "name" : {
                "field" : "name",
                "type" : "text",
                "description" : "Enter Service Name",
                "label" : "Name",
                "required" : false,
                "autoComplete" : false,
                "assetName" : true,
                "showInCard":true
            },
            "port" : {
                "field" : "port",
                "type" : "text",
                "description" : "Enter Port Number",
                "label" : "Port Number",
                "required" : true,
                "autoComplete" : false,
                "showInCard":true
            },
            "endPoint" : {
                "field" : "endPoint",
                "type" : "text",
                "description" : "Enter EndPoint",
                "label" : "End Point",
                "required" : true,
                "autoComplete" : false,
                "default":"healthCheck"
            },
            "description" : {
                "field" : "description",
                "type" : "textarea",
                "description" : "Description",
                "label" : "Description",
                "required" : true,
                "autoComplete" :false
            },
            "servedBy" : {
                "field" : "servedBy",
                "type" : "text",
                "description" : "Served By",
                "label" : "Served By",
                "required" : true,
                "autoComplete" : false
            },
            "networkType" : {
                "field":"networkType",
                "type" : "dropDown",
                "description" : "Select Network Type",
                "label":"Network Type",
                "required":false,
                "autoComplete":true,
                "value":{
                    "dropDownValues": ["local","remote"],
                    "defaultValue":"local"
                }
            },
            "identifier" : {
                "field" : "identifier",
                "type" : "text",
                "description" : "Identifier should be unique",
                "label" : "identifier",
                "required" : true,
                "autoComplete" : false
            }
        }
    }
];
function generateAssetServiceConfiguration(){
    serviceAssetModel.insertMany(assetserviceConfiguration,function(err,res){
        console.log("Added "+res.length+" Service Asset Configuration");
        stopExecution();
    })

}
function stopExecution(){
    process.exit(0);
}
function removePreviousData(callBack){
    serviceAssetModel.remove({},function(err,result){
        console.log("Deleting previous Service Asset Config Data");
        generateAssetServiceConfiguration(callBack);
    })
}
removePreviousData(function(){
    console.log("Started generating Data")
});
module.exports ={
    run:removePreviousData
};
