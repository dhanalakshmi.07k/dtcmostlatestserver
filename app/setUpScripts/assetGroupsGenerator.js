/**
 * Created by suhas on 27/6/18.
 */

let mongoose = require('mongoose'),
    config = require('../../config/config');
let assetGroupModel = require('../models/assetGroup');
let _ = require("lodash");
let db = mongoose.connection;
mongoose.connect(config.db,function(){
    console.log("MongoDb Connected On  "+config.db);
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});

let assetGroupData =[
        {
            "label": "Fire Extinguisher",
            "type": "fireExtinguisher",
            "color": "#ff6d00"
        },
        {
            "label": "Medical Kit",
            "type": "medicalKit",
            "color": "#00c853"
        },
        {
            "label": "Spare Tyre",
            "type": "spareTyre",
            "color": "#1C2331"
        },
    {
        "label": "Warning Triangle",
        "type": "emergencyWarningTriangle",
        "color": "#dd2c00"
    },
    {
        "label": "Cash Machine",
        "type": "cashMachine",
        "color": "#433aff"
    }
];
function generateSensorConfiguration(){
    assetGroupModel.insertMany(assetGroupData,function(err,res){
        console.log("Added "+res.length+" Asset Group");
        stopExecution();
    })

}
function stopExecution(){
    process.exit(0);
}
function removePreviousData(callBack){
    assetGroupModel.remove({},function(err,result){
        console.log("Deleting previous Asset Group Data");
        generateSensorConfiguration(callBack);
    })
}
removePreviousData(function(){
    console.log("Started generating Data")
});
module.exports ={
    run:removePreviousData
};

