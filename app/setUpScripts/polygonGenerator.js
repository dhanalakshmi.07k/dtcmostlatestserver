/**
 * Created by suhas on 28/3/19.
 */

let constant = require("../../config/constants");
let _ = require("lodash");


let mongoose = require('mongoose'),
    config = require('../../config/config'),
    geoFenceModel = require('../models/geoFence');
var geoFenceData = [{
    label:"Parking Lot",
    name:"parkingLot",
    location:{
        "coordinates":[[
            [55.41901946067811,25.277384916082053],
            [55.41605293750764,25.27942222709375,],
            [55.4164659976959,25.279950952217398,],
            [55.41941642761231,25.27904387192114, ],
            [55.41972756385804,25.277952456158587],
            [55.41901946067811,25.277384916082053],
        ]],type:"Polygon"},
    configuration:{
        "lineColor":"black",
        "fillColor":"#6a95c1",
        "fillOpacity":0.3
    },
},{
    label:"Inspection",
    name:"inspection",
    location:{
        "coordinates":[[
            [55.415382385253906,25.277064764098515],
            [55.41510879993439,25.277307303557578],
            [55.41541457176209,25.277642007214865],
            [55.41569888591767,25.277389766863195],
            [55.415382385253906,25.277064764098515],
            [55.415382385253906,25.277064764098515]
        ]],
        type:"Polygon"},
    configuration:{
        "lineColor":"black",
        "fillColor":"#d09186",
        "fillOpacity":0.3
    },
},{
    label:"Maintenance",
    name:"maintenance",
    location:{
        "coordinates":[[
            [55.41563987731934, 25.27597333053333],
            [55.4150390625,25.276419606777512],
            [55.41589200496674,25.277239392557906],
            [55.41647136211396, 25.276885284586722],
            [55.41563987731934, 25.27597333053333]
        ]],type:"Polygon"},
    configuration:{
        "lineColor":"black",
        "fillColor":"#d0cc53",
        "fillOpacity":0.3
    },
},{
    label:"Fuelling",
    name:"fuelling",
    location:{
    "coordinates":[[
        [55.41441142559052, 25.27752073788046],
        [55.415081977844245, 25.278233799827714],
        [55.41555941104889,25.27788939628119],
        [55.41491568088532,25.2771617799403],
        [55.41441142559052, 25.27752073788046]
    ]],type:"Polygon"},
    configuration:{
        "lineColor":"black",
        "fillColor":"#66c170",
        "fillOpacity":0.3
    },
},{
    label:"Car Wash",
    name:"CarWash",
    location:{
        "coordinates":[[
            [55.4147493839264,25.276977449774588],
            [55.414314866065986,25.277307303557578],
            [55.41440606117249,25.277394617644134],
            [55.414813756942756,25.277074465686194],
            [55.4147493839264,25.276977449774588]
        ]],type:"Polygon"},
    configuration:{
        "lineColor":"black",
        "fillColor":"#2c42c1",
        "fillOpacity":0.3
    },
},{
    label:"Meter Cash",
    name:"meterCash",
    location:{
    "coordinates":[[
            [55.41504979133606, 25.278238650574913],
            [55.41441142559052, 25.277554693306318],
            [55.41441142559052,25.277632305672558],
            [55.41443288326264,25.277671111837087],
            [55.41496396064759,25.278253202815314],
            [55.41504442691803,25.27824350132191],
            [55.41504979133606, 25.278238650574913],
        ]],type:"Polygon"},
    configuration:{
        "lineColor":"black",
        "fillColor":"#66c170",
        "fillOpacity":0.3
    },
}]

let db = mongoose.connection;
mongoose.connect(config.mongoConn.url,config.mongoConn.options,function(err,conn){
    console.log(" connection url "+config.mongoConn.url)
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});

function generateGeoFence(){
    geoFenceModel.insertMany(geoFenceData,function(err,res){
        console.log(err);
        stopExecution();
    })

}
function stopExecution(){
    process.exit(0);
}
function removePreviousData(callBack){
    geoFenceModel.remove({},function(err,result){
        console.log("Deleting previous GeoFence Config Data");
        generateGeoFence(callBack);
    })
}
removePreviousData(function(){
    console.log("Started generating Data")
});
module.exports ={
    run:removePreviousData
};