/**
 * Created by suhas on 22/10/18.
 */
let mongoose = require('mongoose'),
    config = require('../../config/config');
let licenceDetailsModel = require('../models/licenceDetails');

let db = mongoose.connection;
/*mongoose.connect(config.db,function(){
 console.log("MongoDb Connected On  "+config.db);
 });*/
mongoose.connect(config.mongoConn.url,config.mongoConn.options,function(err,conn){
    console.log(" connection url "+config.mongoConn.url)
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});

let licenceDetailsModelData = [
    {
        "licenseMessage":" Software License has expired. For uninterrupted access please renew your License immediately.",
        "messageOverride":true
    }
];
function generateAssetServiceConfiguration(){
    licenceDetailsModel.insertMany(licenceDetailsModelData,function(err,res){
        console.log("Added "+res.length+" Licence Data Asset Configuration");
        stopExecution();
    })

}
function stopExecution(){
    process.exit(0);
}
function removePreviousData(callBack){
    licenceDetailsModel.remove({},function(err,result){
        console.log("Deleting previous Licence Data");
        generateAssetServiceConfiguration(callBack);
    })
}
removePreviousData(function(){
    console.log("Started generating Data")
});
module.exports ={
    run:removePreviousData
};

