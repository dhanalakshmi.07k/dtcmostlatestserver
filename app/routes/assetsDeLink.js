/**
 * Created by suhas on 28/6/18.
 */

let express = require('express');
let router = express.Router();
let assetDeLinkDAO = require("../dao").assetsDeLink;

router.post('/', function(req, res, next) {
    let assetLinkObj = req.body;
    assetDeLinkDAO.deLink(assetLinkObj)
    .then(function(result){
        res.send(result);
    }).catch(function(err){
        res.status(500).send(err)
    });
});

module.exports=router;