/**
 * Created by suhas on 5/11/18.
 */

let express = require('express');
let router = express.Router();
let constants = require('../../config/constants');
let config = require('../../config/config');

/*Import Assets Aata*/
let multer = require('multer');
let assetDao = require("../dao/assets");


let xlstojson = require("xls-to-json-lc");
let xlsxtojson = require("xlsx-to-json-lc");/*

let excelToJson = require('convert-excel-to-json');*/


let storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, config.bulkUploadFileStorage.path)
    },
    filename: function (req, file, cb) {
        let datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
    }
});
let upload = multer({ //multer settings
    storage: storage,
    fileFilter : function(req, file, callback) { //file filter
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length-1]) === -1) {
            return callback(new Error('Wrong extension type'));
        }
        callback(null, true);
    }
}).single('file');
router.post('/', function(req, res) {
    let exceltojson;
    upload(req,res,function(err){
        if(err){
            res.json({error_code:1,err_desc:err});
            return;
        }
        /** Multer gives us file info in req.file object */
        if(!req.file){
            res.json({error_code:1,err_desc:"No file passed"});
            return;
        }
        /** Check the extension of the incoming file and
         *  use the appropriate module
         */
        if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx'){
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }
        try {
            let type = req.query.type;
            if(type){
                let fields = constants.BULK_UPLOAD[type];
                if(fields){
                    let fieldsByType =fields.FIELDS;
                    assetDao.bulkImport(req,null,fieldsByType,type)
                        .then(function(data){
                            res.send(data);
                        }).catch(function(err){
                        res.send(err);
                    })
                }else{
                    res.json({error_code:1,err_desc:"Bulk Upload Is not Available For Asset Of type "+type});
                }
            }else{
                res.json({error_code:1,err_desc:"type not defined"});
            }
        } catch (e){
            res.json({error_code:1,err_desc:"Corupted excel file"});
        }
    })
});


router.get('/sample', function(req, res) {
    let type = req.query.type;
    if(type){
        let sampleData = constants.BULK_UPLOAD[type];
        if(sampleData){
            assetDao.getSampleBulkUploadData(req,res,type)
        }else{
            res.json({error_code:1,err_desc:"Sample Data For Specified Type Not Found"});
        }
    }else{
        res.json({error_code:1,err_desc:"type not defined"});
    }
});
module.exports = router;