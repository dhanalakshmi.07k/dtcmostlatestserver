/**
 * Created by suhas on 25/7/18.
 */

let express = require('express');
let router = express.Router();
let inspectionBayDataAccess = require("../dao").inspectionBay;
let driverDataAccess = require("../dao").driver;
let inspectionBayActive = require("../inspectionBay");
let config = require("../../config/config");
let constant = require("../../config/constants");
const _ = require('lodash');
router.get('/active/', function(req, res, next) {
   /* inspectionBayActive.activeInspection.getActiveData().then(function(result){
            res.send(result);
        })*/
   let activeInspectionData = inspectionBayActive.activeInspection.getActiveData();
    res.send(activeInspectionData);
});
router.get('/stop', function(req, res, next) {
    inspectionBayActive.activeInspection.stopInspectingActiveCar()
    .then(function(result){
        res.send(result);
    })
    .catch(function(err){
        res.send(err);
    })
});
router.post('/driverFeedBack', function(req, res, next) {
    let driverFeedBack = req.body;
    let stopInspection = true;
    if(req.query.stopInspection || req.query.stopInspection==="false"){
        stopInspection = false;
    }
    /*inspectionBayActive.activeInspection.getActiveData()
        .then(function(result){*/
            let result = inspectionBayActive.activeInspection.getActiveData();
            if(result.car){
                inspectionBayActive.driverFeedBackListener.setDriverFeedBack(driverFeedBack);
                if(stopInspection){
                    inspectionBayActive.activeInspection.stopInspectingActiveCar()
                    .then(function(inspectionDataSaved){
                        res.send(inspectionDataSaved);
                    })
                }else{
                    result.driverFeedBack=driverFeedBack;
                    res.send(result);
                }
            }else{
                res.send({msg:"No Car In Inspection Bay"})
            }
       /* })
        .catch(function(err){
            res.send({msg:"No Car In Inspection Bay"})
        })*/
});
router.post('/engineInspection', function(req, res, next) {
    let result = inspectionBayActive.activeInspection.getActiveData();
    if(result.car){
        inspectionBayActive.activeInspection.setEngineInspection()
        .then(function(activeData){
            res.send(activeData)
        });
    }else{
        res.send({msg:"No Car In Inspection Bay"})
    }
});
router.post('/bodyScan', function(req, res, next) {
    let result = inspectionBayActive.activeInspection.getActiveData();
    if(result.car){
        inspectionBayActive.activeInspection.setBodyScan()
            .then(function(activeData){
                res.send(activeData)
            });
    }else{
        res.send({msg:"No Car In Inspection Bay"})
    }
});
router.post('/driver/login', function(req, res, next) {
    let driverId = req.body.id;
    let activeInspectionDetails = inspectionBayActive.activeInspection.getActiveData();
    if(activeInspectionDetails.car){
        if(driverId){
            driverDataAccess.getByDriverId(driverId)
                .then(function(result){
                    if(result){
                        inspectionBayActive.driverFeedBackListener.setDriverDetails(result);
                        res.send(result);
                    }else {
                        res.status(405).send({msg:"Driver Id Invalid"});
                    }

                })
        }else{
            res.status(405).send({msg:"Driver Id Required"});
        }
    }else{
        res.status(405).send({msg:"No Car In Inspection Bay"});
    }
});
router.get('/count', function(req, res, next) {
    let search = req.query.search;
    inspectionBayDataAccess.getCount(search)
        .then(function(result){
            if(result){
                res.send(result);
            }else{
                res.send({count:0});
            }
        })
        .catch(function(err){
            res.status(405).send(err);
        })
});
router.get('/', function(req, res, next) {
    let skip = req.query.skip;
    if(skip){
        skip = parseInt(skip);
    }else{
        skip = 0
    }
    let limit = req.query.limit;
    if(limit){
        limit = parseInt(limit);
    }else{
        limit = 12;
    }
    let search = req.query.search;
    let min = req.query.min;
    inspectionBayDataAccess.getAll(skip,limit,search,min)
    .then(function(result){
        res.send(result);
    })
});
router.get('/fields/avdds/:inspectionId', function(req, res, next) {
    let inspectionId = req.params.inspectionId;
    inspectionBayDataAccess.getFieldsByInspectionId(inspectionId,"avdds")
    .then(function(data){
        res.send(data)
    })

});
router.get('/fields/uvis/:inspectionId', function(req, res, next) {
    let inspectionId = req.params.inspectionId;
    inspectionBayDataAccess.getFieldsByInspectionId(inspectionId,"uvis")
        .then(function(data){
            res.send(data)
        })

});
router.get('/fields/engineInspection/:inspectionId', function(req, res, next) {
    let inspectionId = req.params.inspectionId;
    inspectionBayDataAccess.getFieldsByInspectionId(inspectionId,"engineInspection")
        .then(function(data){
            res.send(data)
        })

});
router.get('/id/:inspectionId', function(req, res, next) {
    let inspectionId = req.params.inspectionId;
    inspectionBayDataAccess.getByInspectionId(inspectionId)
        .then(function(data){
            res.send(data)
        })

});
/* GET home page. */
router.get('/reports/:inspectionId', function(req, res, next) {
    let reportId = req.params["inspectionId"];
    inspectionBayDataAccess.getReports(reportId,res)
});
router.get('/analytics/counts/:startDate/:endDate', function(req, res, next) {
    let startDate = req.params["startDate"];
    let endDate = req.params["endDate"];
    let series = req.query.series;
    let carRegNo = req.query.carRegNo;
    let status = req.query.status;
    let validSeries = constant.TIME_SERIES.INSPECTION_REPORT;
    if(!series){
        series = "day";
    }else if(!_.includes(validSeries,series)){
        series = "day";
    }
    inspectionBayDataAccess.getCountByDays(startDate,endDate,series,status,carRegNo)
    .then(function(data){
        res.send(data)
    }).catch(function(err){
        res.send(err)
    })
});
router.get('/analytics/download/:startDate/:endDate', function(req, res, next) {
    let startDate = req.params["startDate"];
    let endDate = req.params["endDate"];
    let series = req.query.series;
    let carRegNo = req.query.carRegNo;
    let status = req.query.status;
    let validSeries = constant.TIME_SERIES.INSPECTION_REPORT;
    if(!series){
        series = "day";
    }else if(!_.includes(validSeries,series)){
        series = "day";
    }
    inspectionBayDataAccess.downloadInspectionData(startDate,endDate,series,status,carRegNo,res)
});
router.get('/sample', function(req, res, next) {
    let noOfSample = req.query["noOfSample"];
    inspectionBayDataAccess.getSample(noOfSample)
        .then(function(data){
            res.send(data)
        }).catch(function(err){
        res.send(err)
    })
});

module.exports = router;