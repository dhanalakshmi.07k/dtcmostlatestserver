/**
 * Created by suhas on 6/7/18.
 */

let express = require('express');
let router = express.Router();
let assetDAO = require("../dao").assets;
/*router.get('/:type/:search', function(req, res, next) {
 let search = req.params.search;
 let limit = req.query.search;
 let skip = req.query.search;
 let type = req.params.type;
 assetDAO.search(type,search,limit,skip)
 .then(function(result){
 res.send(result);
 })
 .catch(function(err){
 res.status(500).send(err)
 });
 });*/
router.get('/:search', function(req, res, next) {
    let search = req.params.search;
    let limit = req.query.limit;
    let skip = req.query.skip;
    let type = req.query.type;
    assetDAO.search(type,search,skip,limit)
        .then(function(result){
            res.send(result);
        })
        .catch(function(err){
            res.status(500).send(err)
        });
});
router.get('/count/:search', function(req, res, next) {
    let search = req.params.search;
    let type = req.query.type;
    assetDAO.searchCount(type,search)
        .then(function(result){
            res.send(result);
        })
        .catch(function(err){
            res.status(500).send(err)
        });
});


module.exports=router;