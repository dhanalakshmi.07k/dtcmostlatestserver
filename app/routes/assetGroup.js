/**
 * Created by suhas on 27/6/18.
 */
let express = require('express');
let router = express.Router();
let assetGroupDAO = require("../dao").assetGroup;

router.post('/', function(req, res, next) {
    let assetGroup = req.body;
    assetGroupDAO.save(assetGroup)
    .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.post('/multiple', function(req, res, next) {
    let assetGroups = req.body;
    assetGroupDAO.saveMultiple(assetGroups)
    .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.post('/linkToAsset', function(req, res, next) {
    let assetGroup = req.body;
    assetGroupDAO.linkToAsset(assetGroup)
    .then(function(result){
        res.send(result);
    }).catch(function(err){
        res.send(err);
    })
});
router.post('/deLinkToAsset', function(req, res, next) {
    let assetGroup = req.body;
    assetGroupDAO.deLinkToAsset(assetGroup)
        .then(function(result){
            res.send(result);
        })
        .catch(function(err){
            res.status(500).send(err)
        });
});


router.get('/', function(req, res, next) {
    assetGroupDAO.getAll()
    .then(function(allAssets){
        res.send(allAssets);
    })
});
router.get('/id/:id', function(req, res, next) {
    assetGroupDAO.getById(req.params.id)
        .then(function(allAssets){
            res.send(allAssets);
        })
});

router.put('/id/:id', function(req, res, next) {
    assetGroupDAO.update(req.params.id,req.body)
    .then(function(allAssets){
            res.send(allAssets);
        })
});

module.exports=router;