/**
 * Created by suhas on 27/3/19.
 */

let express = require('express');
let router = express.Router();

let geoFenceDao = require("../dao").geoFenceModule;
/* GET home page. */
router.get('/', function(req, res, next) {
    geoFenceDao.getAll()
        .then(function(result){
            res.send(result);
        }).catch(function (err) {
        res.status(500).send({msg:err})
    })
});
router.get('/id/:id', function(req, res, next) {
    let id = req.params.id;
    geoFenceDao.getById(id)
        .then(function(result){
            res.send(result);
        }).catch(function (err) {
        res.status(500).send({msg:err})
    })
});

router.post('/', function(req, res, next) {
    let body = req.body;
    geoFenceDao.save(body)
        .then(function(result){
            res.send(result);
        }).catch(function (err) {
        res.status(500).send({msg:err})
    })
});

router.put('/id/:id', function(req, res, next) {
    let id = req.params.id;
    let body = req.body;
    geoFenceDao.updateById(id,body)
        .then(function(result){
            res.send(result);
        }).catch(function (err) {
        res.status(500).send({msg:err})
    })
});
router.delete('/id/:id', function(req, res, next) {
    let id = req.params.id;
    geoFenceDao.removeById(id)
        .then(function(result){
            res.send(result);
        }).catch(function (err) {
        res.status(500).send({msg:err})
    })
});

module.exports = router;