/**
 * Created by suhas on 15/10/18.
 */
let express = require('express');
let router = express.Router();
let ServiceAssetDAO = require("../dao").assetServiceModule;
let _ = require("lodash");

router.post('/', function(req, res, next) {
    let assetLinkObj = req.body;
    ServiceAssetDAO.save(assetLinkObj)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
/*router.put('/', function(req, res, next) {
    let assetLinkObj = req.body;
    ServiceAssetDAO.save(assetLinkObj)
    .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});*/

router.put('/:id', function(req, res, next) {
    let serviceObj = req.body;
    let id = req.params.id;
    ServiceAssetDAO.updateById(id,serviceObj)
    .then(function(updatedAssets){
        res.send(updatedAssets);
    })
});

router.get('/', function(req, res, next) {
    let skip = req.query.skip;
    let limit = req.query.limit;
    let order = req.query.order;
    let type = req.query.type;
    if(type){
        type=_.split(type,",");
    }
    if(skip){
        skip = parseInt(skip)
    }
    if(limit){
        limit = parseInt(limit)
    }
    ServiceAssetDAO.getAll(skip,limit,order,type)
    .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.get('/count', function(req, res, next) {
    let type = req.query.type;
    if(type){
        type=_.split(type,",");
    }
    ServiceAssetDAO.getCount(type)
        .then(function(assetCount){
            res.send({count:assetCount});
        })
        .catch(function(err){
            res.status(400).send({msg:err});
        })
});
router.get('/id/:id', function(req, res, next) {
    let id = req.params.id;
    ServiceAssetDAO.getById(id)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.post('/link', function(req, res, next) {
    let assetLinkObj = req.body;
    ServiceAssetDAO.linkGateway(assetLinkObj)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.post('/deLink', function(req, res, next) {
    let linkableObject = req.body;
    ServiceAssetDAO.deLinkGateway(linkableObject)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.delete('/id/:id', function(req, res, next) {
    let serviceAssetId = req.params.id;
    ServiceAssetDAO.delete(serviceAssetId)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});/*
router.get('/config/type', function(req, res, next) {
    let serviceAssetType = req.params.type;
    ServiceAssetDAO.getConfig(serviceAssetType)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.get('/config', function(req, res, next) {
    let serviceAssetType = req.params.type;
    ServiceAssetDAO.getConfig(serviceAssetType)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});*/

router.post('/manage', function(req, res, next) {
    let serviceAssetId = req.body.id;
    let action = req.body.action;
    ServiceAssetDAO.serviceActions(serviceAssetId,action)
    .then(function(result){
            res.send(result);
    }).catch(function(err){
        res.status(500).send(err)
    });
});

router.get('/status/:startDate/:endDate/:id', function(req, res, next) {
    let serviceId = req.params.id;
    let startDate = req.params.startDate;
    let endDate = req.params.endDate;
    ServiceAssetDAO.getStatus(startDate,endDate,serviceId)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.get('/status/:startDate/:endDate', function(req, res, next) {
    let startDate = req.params.startDate;
    let endDate = req.params.endDate;
    ServiceAssetDAO.getStatus(startDate,endDate,null)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});



router.get('/historical/stats/:startDate/:endDate/:id', function(req, res, next) {
    let serviceId = req.params.id;
    let startDate = req.params.startDate;
    let endDate = req.params.endDate;
    ServiceAssetDAO.getHistoricalServiceStats(startDate,endDate,serviceId)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});

module.exports=router;