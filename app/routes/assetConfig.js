/**
 * Created by suhas on 27/6/18.
 */
let express = require('express');
let router = express.Router();
let assetConfigDAO = require("../dao").assetConfig;

router.get('/types', function(req, res, next) {
    assetConfigDAO.getAllTypes()
        .then(function(allAssets){
            res.send(allAssets);
        })
});

router.get('/', function(req, res, next) {
    let methodToBeExec =null;
    if(req.query.type){
        methodToBeExec = assetConfigDAO.getByType(req.query.type);
    }else{
        methodToBeExec = assetConfigDAO.getAll();
    }
    methodToBeExec.then(function(allAssets){
        res.send(allAssets);
    })
});

module.exports=router;