let express = require('express');
let router = express.Router();
let request = require('request');
let _ = require('lodash');
let socketConnection = require("../ws/socketIo");
/* GET home page. */
router.get('/', function(req, res, next) {
  let payLoad = {
    status:" IOTZEN    >>>>>    RUNNING    >>>>    SUCCESSFULLY   "
  };
  res.send(payLoad);
});
router.get('/carDetail/:vinNo', function(req, res, next) {
  let vinNo = req.params.vinNo;
    request('https://vpic.nhtsa.dot.gov/api/vehicles/decodevin/'+vinNo+'?format=json', function (error, response, body) {
        let carDetails  = JSON.parse(body);// Print the HTML for the Google homepage.
        let groupedData = _.groupBy(carDetails.Results,"letiable");
        res.send(groupedData);
    });
});

router.get('/socketTest', function(req, res, next) {
    let topic = "test";
    if(req.query.topic){
        topic=req.query.topic;
    }
    let conn = socketConnection.getSocketIoConnection();
    conn.emit(topic,"TestTest");
});
router.get('/error', function(req, res, next) {
    throw new Error('Broke!');
});
module.exports = router;
