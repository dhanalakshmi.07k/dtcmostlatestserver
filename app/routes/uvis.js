/**
 * Created by suhas on 27/8/18.
 */
var express = require('express');
var router = express.Router();
var uviDao = require('../dao').uvis;

/* GET home page. */
router.get('/image', function(req, res, next) {
    let imagePath = req.query.imageName;
    uviDao.getUvisImage(imagePath,res)
    .then(function(){

    })
});

module.exports = router;