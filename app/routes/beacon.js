/**
 * Created by suhas on 21/6/18.
 */
let express = require('express');
let router = express.Router();
let beaconLogDataAccess = require("../dao").beaconLog;
let beaconDataAccess = require("../dao").beacon;

/* GET home page. */
router.get('/nearBy', function(req, res, next) {
    let type = req.query.beaconType;
    let duration = req.query.duration;
    beaconLogDataAccess.getAll(type,duration)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.get('/isRegistered/:id', function(req, res, next) {
    let id = req.params.id;
    beaconDataAccess.isBeaconRegistered(id)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});


module.exports = router;