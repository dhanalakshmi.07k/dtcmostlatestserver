/**
 * Created by suhas on 17/10/18.
 */
let express = require('express');
let router = express.Router();
let serviceAssetConfigDAO = require("../dao").serviceAssetConfigServiceModule;

router.get('/types', function(req, res, next) {
    serviceAssetConfigDAO.getAllTypes()
        .then(function(allAssets){
            res.send(allAssets);
        })
});

router.get('/', function(req, res, next) {
    let methodToBeExec =null;
    if(req.query.type){
        methodToBeExec = serviceAssetConfigDAO.getByType(req.query.type);
    }else{
        methodToBeExec = serviceAssetConfigDAO.getAll();
    }
    methodToBeExec.then(function(allAssets){
        res.send(allAssets);
    })
});

module.exports=router;