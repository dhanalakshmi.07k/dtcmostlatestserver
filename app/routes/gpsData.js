/**
 * Created by suhas on 20/3/19.
 */


let express = require('express');
let router = express.Router();

let gpsDao = require("../dao").gpsModule;
/* GET home page. */
router.get('/historical/:startDate/:endDate/:vinNumber', function(req, res, next) {
    let startDate = req.params.startDate;
    let endDate = req.params.endDate;
    let vinNumber = req.params.vinNumber;
    gpsDao.getHistoricalData(startDate,endDate,vinNumber)
    .then(function(result){
        res.send(result);
    }).catch(function (err) {
        res.status(500).send({msg:err})
    })
 });

module.exports = router;