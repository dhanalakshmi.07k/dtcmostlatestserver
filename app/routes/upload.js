/**
 * Created by suhas on 26/6/18.
 */

let express = require('express');
let router = express.Router();
let fs = require('fs');
let Upload = require('../models/upload');
let config = require('../../config/config');

/*var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp/my-uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})*/
let multer = require('multer');
console.log("Upload Path");
console.log(config.imageUploadFolder);
let upload = multer({dest:config.imageUploadFolder});

/**
 * Gets the list of all files from the database
 */
router.get('/', function (req, res, next) {
    Upload.find({},  function (err, uploads) {
        if (err) next(err);
        else {
            res.send(uploads);
        }
    });
});

/**
 * Gets a file from the hard drive based on the unique ID and the filename
 */
router.get('/:assetType/:type/:id', function (req, res, next) {
    console.log(req.params);

    let subType = req.params.type;
    let id = req.params.id;
    let assetType = req.params.assetType;
    let filename = assetType+""+subType+""+id;
    Upload.findOne({'name': filename}, function (err, upload) {

        if (err) next(err);
        else {
            if(upload && upload.file.path){
                res.set({
                    "Content-Disposition": 'attachment; filename="' + upload.file.originalname + '"',
                    "Content-Type": upload.file.mimetype
                });
                fs.createReadStream(upload.file.path).pipe(res);
            }else{
                res.send("No Image Found")
            }
        }
    });
});

/**
 * Create's the file in the database
 */
router.post('/', upload.single('file'), function (req, res, next) {
    console.log(req.body);
    let subType = req.body.type;
    let id = req.body.id;
    let assetType = req.body.assetType;
    let filename = assetType+""+subType+""+id;
    let newUpload = {
        name: filename,
        created: Date.now(),
        file: req.file
    };
    Upload.findOne({'name': filename}, function (err, upload) {

        if (err) next(err);
        else {
            if(upload){
                upload.set(newUpload);
                upload.save(function (err, updatedUpload) {
                    if (err) {
                        reject(err)
                    }
                    else{
                        res.send(updatedUpload);
                    }
                });
            }else{
                Upload.create(newUpload, function (err, next) {
                    if (err) {
                        next(err);
                    } else {
                        res.send(newUpload);
                    }
                });
            }
        }
    });
});


module.exports = router;