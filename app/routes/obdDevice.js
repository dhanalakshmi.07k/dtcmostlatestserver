/**
 * Created by suhas on 15/6/18.
 */

var express = require('express');
var router = express.Router();
var constants = require('../../config/constants');

/* GET home page. */
router.post('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

module.exports = router;
