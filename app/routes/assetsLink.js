/**
 * Created by suhas on 28/6/18.
 */
let express = require('express');
let router = express.Router();
let assetLinkDAO = require("../dao").assetLink;

router.post('/', function(req, res, next) {
    let assetLinkObj = req.body;
    assetLinkDAO.link(assetLinkObj)
    .then(function(result){
        res.send(result);
    }).catch(function(err){
        res.status(500).send(err)
    });
});

module.exports=router;