/**
 * Created by suhas on 18/9/18.
 */
let express = require('express');
let router = express.Router();
let licenceValidator = require("../stringHandler").validator;
let _ = require("lodash");
let moment = require("moment");


router.get('/validity', function(req, res, next) {
    licenceValidator.validity().then(function(data){
        res.send(data);
    })
    /*let validity = licenceValidator.validity();
    res.send(validity)*/
});
module.exports = router;