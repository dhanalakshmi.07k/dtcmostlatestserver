let express = require('express');
let router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
let authentication_controller = require('../controllers/authentication.controller');

router.post('/login', function(req, res){

    let username = req.body.username;
    let pwd = req.body.password;

    authentication_controller.verify_user(username, pwd).then(function (status) {

        if(status){
            res.json(status);
        }
        else{
            res.status(400).send("Something went wrong")
        }

    }).catch((err)=>{
        res.status(400).send(err);
    });

})

module.exports = router;
