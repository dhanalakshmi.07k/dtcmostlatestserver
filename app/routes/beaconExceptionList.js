/**
 * Created by suhas on 31/7/18.
 */
let express = require('express');
let router = express.Router();
let beaconExceptionDataAccess = require("../dao").beaconException;

/* GET home page. */
router.post('/', function(req, res, next) {
    console.log(req.body);
    let id = req.body.id;
    beaconExceptionDataAccess.save(id)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.get('/', function(req, res, next) {
    beaconExceptionDataAccess.get()
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.delete('/:id', function(req, res, next) {
    let id = req.params.id;
    beaconExceptionDataAccess.delete(id)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});

module.exports = router;