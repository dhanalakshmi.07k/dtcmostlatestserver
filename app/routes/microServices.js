/**
 * Created by suhas on 8/10/18.
 */
let express = require('express');
let router = express.Router();
let assetDataAccess = require("../dao").assets;
let _ = require("lodash");

router.get('/', function(req, res, next) {
    let type = req.query.types;
    let ipAddress = req.query.ipAddress;
    if(type){
        type=_.split(type,",");
    }
    assetDataAccess.getAllMicroService(type,ipAddress)
        .then(function(allAssets){
            res.send(allAssets);
        })
});


module.exports = router;