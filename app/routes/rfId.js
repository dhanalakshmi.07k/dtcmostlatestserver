/**
 * Created by suhas on 30/6/18.
 */
let express = require('express');
let router = express.Router();
let rfIdLogDataAccess = require("../dao").rfIdLog;

/* GET home page. */
router.get('/nearBy', function(req, res, next) {
    //let type = req.query.beaconType;
    let duration = req.query.duration;
    rfIdLogDataAccess.getAll(duration)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});


module.exports = router;