/**
 * Created by suhas on 14/6/18.
 */
let express = require('express');
let router = express.Router();
let assetDataAccess = require("../dao").assets;
let _ = require("lodash");


/** API path that will upload the files */
router.post('/', function(req, res, next) {
    var asset = req.body;
    assetDataAccess.save(asset)
    .then(function(result){
        res.send(result);
    }).catch(function(err){
        res.status(405).send(err)
    });
});
router.post('/multiple', function(req, res, next) {
    let assets = req.body;
    assetDataAccess.saveMultiple(assets)
    .then(function(result){
        res.send(result);
    })
    .catch(function(err){
        res.status(408).send(err)
    });
});
router.get('/count', function(req, res, next) {
    let type = req.query.type;
    if(type){
        type=_.split(type,",");
    }
    assetDataAccess.getCount(type)
    .then(function(assetCount){
        res.send({count:assetCount});
    })
    .catch(function(err){
        res.status(400).send({msg:err});
    })
});
router.get('/countByType', function(req, res, next) {
    assetDataAccess.getAllCountByTypes()
        .then(function(assetCount){
            res.send({count:assetCount});
        })
});
router.get('/:id', function(req, res, next) {
    var id = req.params.id;
    assetDataAccess.getById(id)
        .then(function(allAssets){
            res.send(allAssets);
        })
});
router.get('/', function(req, res, next) {

    let skip = req.query.skip;
    let limit = req.query.limit;
    let order = req.query.order;
    let type = req.query.type;
    if(type){
        type=_.split(type,",");
    }
    if(skip){
        skip = parseInt(skip)
    }
    if(limit){
        limit = parseInt(limit)
    }
    assetDataAccess.getAll(skip,limit,order,type)
    .then(function(allAssets){
            res.send(allAssets);
        })
});
router.delete('/:id', function(req, res, next) {
    var assetId = req.params.id;
    assetDataAccess.delete(assetId)
        .then(function(deletedAssets){
            res.send(deletedAssets);
        })
});
router.put('/:id', function(req, res, next) {
    var assetObj = req.body;
    var id = req.params.id;
    assetDataAccess.update(id,assetObj)
        .then(function(updatedAssets){
            res.send(updatedAssets);
        })
});

router.get('/group', function(req, res, next) {
    let type = req.query.type;
    if(type){
        type=_.split(type,",");
    }
    assetDataAccess.getCount(type)
    .then(function(assetCount){
        res.send({count:assetCount});
    })
});
router.get('/linkedAssets/:id', function(req, res, next) {
    let primaryAssetId = req.params.id;
    assetDataAccess.getAllAssetsLinkedDetails(primaryAssetId)
        .then(function(assetsLinkedDetails){
            res.send(assetsLinkedDetails);
        })
});


router.get('/stats/:startDate/:endDate/:id', function(req, res, next) {
    let assetId = req.params.id;
    let startDate = req.params.startDate;
    let endDate = req.params.endDate;
    let seriesType = req.query.seriesType;
    assetDataAccess.getStats(startDate,endDate,assetId,seriesType)
        .then(function(assetsLinkedDetails){
            res.send(assetsLinkedDetails);
        }).catch(function(assetsLinkedDetails){
        res.status(500).send(assetsLinkedDetails);
    })
});
router.get('/stats/:startDate/:endDate', function(req, res, next) {
    let assetId = req.params.id;
    let startDate = req.params.startDate;
    let endDate = req.params.endDate;
    let seriesType = req.query.seriesType;
    assetDataAccess.getAllStats(startDate,endDate,assetId,seriesType)
        .then(function(assetsLinkedDetails){
            res.send(assetsLinkedDetails);
        }).catch(function(assetsLinkedDetails){
        res.status(500).send(assetsLinkedDetails);
    })
});

router.get('/historical/stats/:startDate/:endDate/:id', function(req, res, next) {
    let assetId = req.params.id;
    let startDate = req.params.startDate;
    let endDate = req.params.endDate;
    let seriesType = req.query.seriesType;
    let fields = req.query.fields;
    assetDataAccess.getHistoricalStatsById(startDate,endDate,assetId,seriesType,fields)
    .then(function(assetsLinkedDetails){
            res.send(assetsLinkedDetails);
        }).catch(function(assetsLinkedDetails){
        res.status(500).send(assetsLinkedDetails);
    })
});



module.exports = router;
