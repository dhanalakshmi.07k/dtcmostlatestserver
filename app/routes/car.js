/**
 * Created by suhas on 15/6/18.
 */

let express = require('express');
let router = express.Router();
let carDataAccess = require("../dao").car;

/* GET home page. */
router.get('/search/:searchText', function(req,res,next) {
    let searchText = req.params.searchText;
    let limit = req.query.limit;
    let skip = req.query.skip;
    carDataAccess.search(searchText,skip,limit)
    .then(function(result){
        res.send(result);
    })
});

module.exports = router;
