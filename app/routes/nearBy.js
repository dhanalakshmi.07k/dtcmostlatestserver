/**
 * Created by suhas on 2/7/18.
 */
let express = require('express');
let router = express.Router();
let nearByDataAccess = require("../dao").nearByLog;
let _ = require("lodash");


router.get('/', function(req, res, next) {
    let type = req.query.type;
    let duration = req.query.duration;
    let registered = req.query.registered;
    nearByDataAccess.getAllNearBy(type,duration,registered)
    .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
router.get('/assetsByRfId', function(req, res, next) {
    let type = req.query.type;
    let duration = req.query.duration;
    nearByDataAccess.getAllAssetsLinkedToNearByRfId(duration)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});
module.exports = router;