/**
 * Created by suhas on 11/2/19.
 */

let express = require('express');
let router = express.Router();
let userService = require("../service/userDetailsService");
let ROLES_CONSTANT = require("../auth/ability").RULES;


router.get('/', function (req, res, next) {
    req.ability.throwUnlessCan(ROLES_CONSTANT.ACTION.READ,ROLES_CONSTANT.SUBJECT.USER);
    let ability = req.ability;
    userService.getAllUsers(ability)
    .then(function (allUsers) {
        if(allUsers){
            res.json(allUsers);
        }
        else{
            res.status(400).send("Failed to get Users")
        }
    }).catch((err)=>{
        res.status(404).send(err)
    })
});
router.post('/', function(req, res, next) {
    let ability = req.ability;
    let body = req.body;
    ability.throwUnlessCan(ROLES_CONSTANT.ACTION.WRITE,ROLES_CONSTANT.SUBJECT.USER);
    userService.createUser(body, ability)
        .then(function (success) {
            if(success){
                res.send(success);
            }
            else{
                res.status(400).send("Unsuccessful");
            }
        })
        .catch((err)=>{
            res.status(400).send(err)
        })
});


//to get the user details by his id
router.get('/profile/:id', function(req, res, next){
    req.ability.throwUnlessCan(ROLES_CONSTANT.ACTION.READ,ROLES_CONSTANT.SUBJECT.USER);
    let id = req.params.id;
    if(!id){
        res.status(400).send("Id required")
    }else{
        userService.getUserById(id)
            .then(function(updatedUserObject){
                res.send(updatedUserObject)
            }).catch(function(err){
            res.status(400).send({msg:err})
        });
    }
})
//update by the authorized person
router.put('/profile/:id', function (req, res, next) {
    req.ability.throwUnlessCan(ROLES_CONSTANT.ACTION.UPDATE,ROLES_CONSTANT.SUBJECT.USER);
    let id = req.params.id;
    let updatedUserObj = req.body;
    if(!id){
        res.status(400).send("Id required")
    }else if(!updatedUserObj){
        res.status(400).send("User Object required")
    }else{
        userService.updateUserProfile(id,updatedUserObj)
        .then(function(updatedUserObject){
            res.send(updatedUserObject)
        }).catch(function(err){
            res.status(400).send({msg:err})
        });
    }

});
//authority to delete profile
router.delete('/profile/:id', function(req, res, next) {
    req.ability.throwUnlessCan(ROLES_CONSTANT.ACTION.DELETE,ROLES_CONSTANT.SUBJECT.USER);
    let userId=req.params.id;
    let ability = req.ability;
    userService.delete(userId,ability)
        .then(function (response) {
            if(response){
                res.send(response);
            }
        }).catch((err)=>{
        res.status(400).send(err);
    });
});


/*To get Roles */
router.get('/rolesDefined', function(req, res, next) {
    req.ability.throwUnlessCan(ROLES_CONSTANT.ACTION.READ,ROLES_CONSTANT.SUBJECT.USER);
    let roles = req.ability.rules;
    userService.getRoles(roles).then(function(response) {
        if(response){
            res.send(response)
        }
        else{
            res.status(400).send("Action not possible")
        }
    }).catch((err)=>{
        reject(err);
    })

});
//check the entered username exists or not
router.get('/isUserNameExist/:username', function(req, res, next){
    let userName = req.params.username;
    userService.isUserNameAlreadyUsed(userName)
    .then(function(status){
        if(status){
            res.send(status);
        }
        else{
            res.status(400).send(status);
        }
    }).catch((err)=>{
        res.status(404).send(err)
    });
})





router.get('/self/profile', function (req, res, next) {
    let id = req.myProfile._id;
    if(!id){
        res.status(400).send(" !Token Expired , Please Try To Update After Login")
    }
    userService.getUserById(id)
        .then(function (userDetails) {
            if(userDetails){
                res.send(userDetails);

            }
            else{
                res.status(400).send(userDetails);
            }
        }).catch((err)=>{
        res.status(404).send(err);
    })

})
//update profile details by the user of his own account
router.put('/self/profile', function(req, res, next) {
    let id = req.myProfile._id;
    let updatedUserObj = req.body;
    if(!id){
        res.status(400).send(" !Token Expired , Please Try To Update After Login")
    }else if(!updatedUserObj){
        res.status(400).send("User Object required")
    }else{
        userService.updateUserProfile(id,updatedUserObj)
            .then(function(updatedUserObject){
                res.send(updatedUserObject)
            }).catch(function(err){
            res.status(400).send({msg:err})
        });
    }
});
//to change the password of his own
router.put('/self/profile/password', function(req, res, next) {
    let newPassword = req.body.newPassword;
    let userProfile = req.myProfile;
    userService.updatePassword(newPassword,userProfile).then(function(){
        res.send(req.myProfile);
    }).catch(function(err){

        res.status(400).send(err)
    });

});


module.exports = router;
