/**
 * Created by suhas on 19/11/18.
 */

let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let nearByLogInfoSchema =  new Schema({
    nearby:String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{capped:1500},{collection: "driverFeedback"});
nearByLogInfoSchema.index({created:1});
let driverFeedbackLog = mongoose.model('driverFeedback',nearByLogInfoSchema);
nearByLogInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});

module.exports = driverFeedbackLog;