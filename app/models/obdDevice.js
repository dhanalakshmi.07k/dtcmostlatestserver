/**
 * Created by suhas on 14/6/18.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var obdInfoSchema =  new Schema({
    "uniqueIdentifier" : Number,
    "type" : String,
    "make" : String,
    "assetType" :String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}
},{collection: "asset"});
obdInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = mongoose.model('obd',obdInfoSchema);