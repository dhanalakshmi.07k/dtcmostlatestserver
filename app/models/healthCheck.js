/**
 * Created by suhas on 10/10/18.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let healthCheckLogSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }},{strict:false},{collection: "healthCheckLog"});
let healthCheckLogModel = mongoose.model('healthCheckLog',healthCheckLogSchema);
healthCheckLogSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
healthCheckLogSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
module.exports = healthCheckLogModel;