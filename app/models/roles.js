/**
 * Created by suhas on 13/2/19.
 */

let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let rolesSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "roles"});
let rolesConfiguration = mongoose.model('roles',rolesSchema);
rolesSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = rolesConfiguration;