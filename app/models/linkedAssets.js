/**
 * Created by suhas on 26/6/18.
 */
var mongoose = require('mongoose'),assetModel=require("./assets");
    Schema = mongoose.Schema;
var assetInfoSchema =  new Schema({
    assetsLinked: [assetModel],
    assetTypeLinked: [String],
    updated: { type: Date, default: Date.now },
    created: { type: Date}
    },{strict:false},{collection: "asset"});


var asset = mongoose.model('linkedAssets',assetInfoSchema);
assetInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = asset;