/**
 * Created by Suhas on 2/12/2016.
 */
var mongoose = require('mongoose'),
        Schema = mongoose.Schema;
var carInfoSchema =  new Schema({
    "vinNumber" : String,
    "registrationNumber" : String,
    "fleetNo" : String,
    "carNo" : String,
    "make" : String,
    "modelNo" : String,
    "dutyType" : String,
    "assetName" : String,
    "assetType" :String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}
},{strict:true},{collection: "asset"});
carInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
carInfoSchema.index({
    fleetNo: 'text',
    registrationNumber: 'text',
    vinNumber: 'text'});

var car = mongoose.model('car',carInfoSchema);

module.exports = car;