let mongoose = require('mongoose');

let UploadSchema = mongoose.Schema({
  name: String,
  file: Object,
  updated: { type: Date, default: Date.now },
  created: { type: Date}
},{strict:false},{collection: "upload"});

module.exports = mongoose.model('upload', UploadSchema);