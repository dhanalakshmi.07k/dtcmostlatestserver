/**
 * Created by suhas on 25/2/19.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let activeInspectionBaySchema =  new Schema({
    nearby:String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "activeInspectionBayLog"});
activeInspectionBaySchema.index({created:1});
let ActiveInspectionBay = mongoose.model('activeInspectionBayLog',activeInspectionBaySchema);
activeInspectionBaySchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
//const pipeline = [{ $match: { 'ns.db': 'test', 'ns.coll': 'Price' } }];
ActiveInspectionBay.watch().
on('change', async (data) => {
    const doc = data.fullDocument;

});
module.exports = ActiveInspectionBay;