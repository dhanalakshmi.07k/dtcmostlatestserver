/**
 * Created by suhas on 13/8/18.
 */

let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let obdLogSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }},{strict:false},{collection: "obdLog"});
let obdLog = mongoose.model('obdLog',obdLogSchema);
obdLogSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
obdLogSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
module.exports = obdLog;