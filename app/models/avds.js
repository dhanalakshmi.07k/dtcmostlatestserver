/**
 * Created by suhas on 9/8/18.
 */

let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let avdsSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }},{strict:false},{collection: "avds"});
let avds = mongoose.model('avds',avdsSchema);
avdsSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
avdsSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
module.exports = avds;