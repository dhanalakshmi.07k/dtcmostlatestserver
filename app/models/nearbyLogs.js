/**
 * Created by suhas on 9/7/18.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let nearByLogInfoSchema =  new Schema({
    nearby:String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{capped:1500},{collection: "nearByLog"});
nearByLogInfoSchema.index({created:1});
let nearByLog = mongoose.model('nearByLog',nearByLogInfoSchema);
nearByLogInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});

module.exports = nearByLog;