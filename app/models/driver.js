/**
 * Created by suhas on 14/6/18.
 */
/**
 * Created by Suhas on 2/12/2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var driverInfoSchema =  new Schema({
    "accommodation" : String,
    "dateOfJoin" : String,
    "mobileNumber" : String,
    "licenceNo" : String,
    "jdeDepartment" : String,
    "department" : String,
    "gender" : String,
    "driverName" : String,
    "assetType" :String,
    "rfid" :String,
    "driverId" :String,
    "badgeNo" :String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}
},{collection: "asset"});
driverInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = mongoose.model('driver',driverInfoSchema);