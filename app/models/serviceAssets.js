/**
 * Created by suhas on 11/10/18.
 */

let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let serviceAssetLogSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }},{strict:false},{collection: "serviceAssets"});
let serviceAssetModel = mongoose.model('serviceAssets',serviceAssetLogSchema);
serviceAssetLogSchema.pre('save', function (next) {
    this.updated = new Date();
    //publishServiceAsset(this);
    next();
});
serviceAssetLogSchema.pre('update', function (next) {
    this.updated = new Date();
    //publishServiceAsset(this);
    next();
});

serviceAssetLogSchema.post('remove', function(doc) {
    //publishServiceAsset(this);
});
module.exports = serviceAssetModel;