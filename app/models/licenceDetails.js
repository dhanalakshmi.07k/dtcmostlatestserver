/**
 * Created by suhas on 22/10/18.
 */


let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let licenceDetailsSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }},{strict:false},{collection: "licenceDetails"});
let licenceDetailsModel = mongoose.model('licenceDetails',licenceDetailsSchema);
licenceDetailsSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
licenceDetailsSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});

module.exports = licenceDetailsModel;