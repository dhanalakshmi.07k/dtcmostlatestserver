/**
 * Created by suhas on 14/3/19.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let obdDeviceGpsSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }},{strict:false},{collection: "obdDeviceGps"});
let obdDeviceGps = mongoose.model('obdDeviceGps',obdDeviceGpsSchema);
obdDeviceGpsSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
obdDeviceGpsSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
module.exports = obdDeviceGps;