/**
 * Created by suhas on 12/10/18.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let assetServiceConfigurationInfoSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "serviceAssetConfiguration"});
let serviceAssetConfiguration = mongoose.model('serviceAssetConfiguration',assetServiceConfigurationInfoSchema);
assetServiceConfigurationInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = serviceAssetConfiguration;