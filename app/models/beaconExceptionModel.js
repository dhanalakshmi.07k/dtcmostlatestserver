/**
 * Created by suhas on 31/7/18.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let beaconExceptionSchema =  new Schema({
    beaconId:String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{capped:1500},{collection: "beaconException"});
beaconExceptionSchema.index({created:1});
beaconExceptionSchema.index({
    beaconId: 'text'});
let beaconException = mongoose.model('beaconException',beaconExceptionSchema);
beaconExceptionSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});

module.exports = beaconException;