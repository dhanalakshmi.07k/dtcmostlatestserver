/**
 * Created by suhas on 27/3/19.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;


const geoFenceLogsSchema = new Schema({
    name: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now },
    location: {
        type: { type: String },
        coordinates: [[[Number]]],
    }
},{strict:false},{collection: "geoFenceLogs"});
//geoFenceLogsSchema.index({ "coordinates": "2dsphere" });

const geoFenceLogs = mongoose.model('geoFenceLogs',geoFenceLogsSchema);

module.exports = geoFenceLogs;