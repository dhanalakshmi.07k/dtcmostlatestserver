/**
 * Created by suhas on 9/8/18.
 */

let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let uvisSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }},{strict:false},{collection: "uvis"});
let uvis = mongoose.model('uvis',uvisSchema);
uvisSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
uvisSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
module.exports = uvis;