/**
 * Created by suhas on 25/7/18.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let inspectionBayLogInfoSchema =  new Schema({
    nearby:String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "inspectionBayLog"});
inspectionBayLogInfoSchema.index({created:1});
let inspectionBayLog = mongoose.model('inspectionBayLog',inspectionBayLogInfoSchema);
inspectionBayLogInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});

module.exports = inspectionBayLog;