/**
 * Created by suhas on 23/6/18.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var baconInfoSchema =  new Schema({
    "beaconId" : String,
    "beaconType" : String,
    "assetName" : String,
    "assetType" :String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}
},{strict:true},{collection: "asset"});
baconInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
var car = mongoose.model('beacon',baconInfoSchema);

module.exports = car;