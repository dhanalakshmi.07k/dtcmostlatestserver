let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let userDetailsSchema =  new Schema({
    username: String,
    password: String,
    email: String,
    firstName: String,
    lastName: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "userDetails"});
let userDetails = mongoose.model('userDetails',userDetailsSchema);
userDetailsSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = userDetails;
