/**
 * Created by suhas on 20/7/18.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let rfIdInfoSchema =  new Schema({
    "RFIDId" : String,
    "assetName" : String,
    "assetType" :String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}
},{strict:true},{collection: "asset"});
rfIdInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
let rfId = mongoose.model('rfId',rfIdInfoSchema);

module.exports = rfId;