/**
 * Created by suhas on 18/6/18.
 */

let mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    logger = require("../../config/logger"),
    dataLimit = require("../../app/stringHandler").validator;

let ValidationError = mongoose.Error.ValidationError,
    ValidatorError = mongoose.Error.ValidatorError;
let constant = require("../../config/constants");
let assetInfoSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }},{strict:false},{collection: "asset"});
assetInfoSchema.index({
    fleetNo: 'text',
    registrationNumber: 'text',
    vinNumber: 'text'});
let asset = mongoose.model('asset',assetInfoSchema);
assetInfoSchema.pre('save', function (next) {
    this.updated = new Date();
    console.log("Saved");
    let assetType = this.assetType;
    if(assetType===constant.ASSETS_TYPES.BEACON || assetType===constant.ASSETS_TYPES.RF_ID){
        getCount(assetType).then(function(count){
            if(count){
                count = parseInt(count);
            }
            let assetLimitCount = parseInt(dataLimit.getFileData().data[assetType]);
            if(assetLimitCount>count){
                next();
            }else{
                let errors = new ValidationError();
                const properties = {
                    type: 'Limit Exceeded',
                    message:"Limit Exceeded For "+assetType+" , Current Limit For "+assetType+"  = "+assetLimitCount,
                    path: 'Contact System Admin',
                    value: assetType
                };
                errors.errors.keywords = new ValidatorError(properties);
                next(new ValidatorError(properties));
            }
        });
    }else{
        next();
    }
});
assetInfoSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
assetInfoSchema.pre('findOneAndUpdate', function (next) {
    this.updated = new Date();
    next();
});
assetInfoSchema.post('remove', function(doc) {
    logger.assets.log({ level: 'warn', message: 'REMOVED FOLLOWING ASSET'+doc});
});
function getCount(assetType){
    return new Promise(function(resolve,reject){
        asset.count({'assetType': assetType}, function (err, docs) {
            resolve(docs)
        });
    })
};
module.exports = asset;