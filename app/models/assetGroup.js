/**
 * Created by suhas on 27/6/18.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let assetGroupInfoSchema =  new Schema({
    label:String,
    type:String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assetGroup"});
let assetGroup = mongoose.model('assetGroup',assetGroupInfoSchema);
assetGroupInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assetGroup;