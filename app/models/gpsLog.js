/**
 * Created by suhas on 14/3/19.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;


const gpsLogsSchema = new Schema({
    name: String,
    updated: { type: Date, default: Date.now ,index: true },
    created: { type: Date, default: Date.now ,index: true },
    location: {
        type: { type: String },
        coordinates: [Number],
    }
},{strict:false},{collection: "gpsLogs"});
gpsLogsSchema.index({ "coordinates": "2dsphere" });

const gpsLogs = mongoose.model('gpsLogs',gpsLogsSchema);

module.exports = gpsLogs;
