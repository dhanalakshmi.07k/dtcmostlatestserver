/**
 * Created by suhas on 21/6/18.
 */
let mqtt = require('mqtt');
let _ = require('lodash');
let client=null;
let mqttConnect = function (brokerUrl) {
    establishConnection(brokerUrl);
    function establishConnection(brokerUrl) {
        client = mqtt.connect(brokerUrl);
        client.on('connect', function () {
            console.log("MQTT CONNECTION TO BROKER "+brokerUrl+" ESTABLISHED");
        })
    }
}

let  publishData = function(topic,data){
    if(client){
        client.publish(topic, JSON.stringify(data));
    }
}

module.exports = {
    connect: mqttConnect,
    publish:publishData
}
