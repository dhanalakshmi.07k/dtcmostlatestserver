/**
 * Created by suhas on 25/7/18.
 */
/**
 * Created by rranjan on 1/15/16.
 */
"use strict";
let config = require("../../config/config");
let socketConn=null;
let _=require("lodash");
let httpServer;
let t = null;
let connection = [];
let init = function(server){
    httpServer=server;
    let socketIoServer = require('socket.io')(server);

    socketIoServer.on('connection', function (socketConnection) {
        socketConn = socketConnection;
        connection.push(socketConnection);
        console.log('No Of Client Connected...'+connection.length);
        socketConnection.on('disconnect', function () {
            if(socketConnection && socketConnection.id){
                let socketConnRemoved = _.remove(connection,function(conn){
                    return conn.id===socketConnection.id;
                });
                console.log('No Of Client Connected...'+connection.length);

            }
        });
    });

    if(t){
        clearInterval(t);
    }
    t=setInterval(function(){
        emitToAllSocketConnection("keepAlive",new Date().getTime());
    }, config.SOCKET.KEEP_ALIVE_TIME*1000);
};
let getSocketIoConnection = function(){
        if(!socketConn){
            init(httpServer)
        }
        return socketConn;
};
let emitToAllSocketConnection = function (topic,message) {
    _.each(connection, function (conn) {
        if(conn){
            conn.emit(topic,message)
        }
    });
};
module.exports={
    init:init,
    getSocketIoConnection:getSocketIoConnection,
    emitToAllSocketConnection:emitToAllSocketConnection
};
