/**
 * Created by suhas on 21/6/18.
 */

let topics = require('../../config/constants').SUBSCRIBER.MQTT.TOPIC;
let socketContant = require('../../config/constants').PUBLISHER.SOCKET;
let beaconDataHandler = require("../iotHandler").beaconsDataHandler;
let rfIdDataHandler = require("../iotHandler").rfIdDataHandler;
let obdDataHandler = require("../iotHandler").obdDeviceDataHandler;
let healthCheckHandler = require("../iotHandler").healthCheckDataHandler;
let driverFeedBackDataHandler = require("../iotHandler").driverFeedBackDataHandler;
let uvisDataHandler = require("../iotHandler").uvisDataHandler;
let avddsDataHandler = require("../iotHandler").avddsHandler;
let ObdDeviceGpsDataHandler = require("../iotHandler").obdDeviceGpsHandler;
let freeMatricsObdDataHandler = require("../iotHandler").freeMatricsObdDataHandler;
let socketConn = require("../ws/socketIo");
function processData(topic,message){
    try{

        if (topic.toString() ===topics.BEACON) {
            beaconDataHandler.logScanned(message);
        }
        if (topic.toString() ===topics.RF_ID_TAG) {
            rfIdDataHandler.logScanned(message);
        }
        if (topic.toString() ===topics.OBD_DEVICE) {
            obdDataHandler.logScanned(message);
        }
        if (topic.toString() ===topics.HEALTH_CHECK) {
            healthCheckHandler.logScanned(message);
        }
        if (topic.toString() ===topics.DRIVER_FEEDBACK) {
            driverFeedBackDataHandler.logScanned(message);
        }
        if (topic.toString() ===topics.HEALTH_CHECK_MANAGE) {
            if(message){
                message = JSON.parse(message);
                socketConn.emitToAllSocketConnection(socketContant.HEALTH_MANAGEMENT_RESPONSE,message);
            }
        }
        if (topic.toString() ===topics.UVIS) {
            uvisDataHandler.logScanned(message);
        }
        if (topic.toString() ===topics.AVDDS) {
            avddsDataHandler.logScanned(message);
        }

        if (topic.toString() ===topics.OBD_DEVICE_GPS) {
            // console.log("gps")
            ObdDeviceGpsDataHandler.logScanned(message);
        }

        if (topic.toString() ===topics.FREEMATRICS_OBD_DEVICE) {
            // console.log("gps")
            freeMatricsObdDataHandler.logScanned(message);
        }
    }
    catch(err){
        console.log(err)
    }
}

module.exports = {
    processData:processData
};