/**
 * Created by suhas on 21/6/18.
 */
let mqtt = require('mqtt');
let _ = require('lodash');
let dataHandler = require("./processSubscribedData");
let logger = require("../../config/logger").logger;
let subscriber = function (brokerUrl,subscribeTopic) {
    startPulling(brokerUrl,subscribeTopic);
    function startPulling(brokerUrl,subscribeTopic) {
        let client = mqtt.connect(brokerUrl);
        client.on('connect', function () {
            logger.log({level:"debug",message:"MQTT CONNECTION TO BROKER "+brokerUrl+" ESTABLISHED"});
            _.each(subscribeTopic,function(topic){
                logger.log({level:"debug",message:"MQTT SUBSCRIBED TO FOLLOWING  "+topic});
                client.subscribe(topic);
            })
        });
        client.on('message', function(topic, payload) {
            if(payload){
                if(payload && payload.toString()){
                    processData(topic,payload.toString())
                   // console.log(payload.toString())
                }
            }
        });
    }
    function processData(topic,message) {
        dataHandler.processData(topic,message)
    }
};


module.exports = {subscriber: subscriber};
