/**
 * Created by suhas on 14/2/19.
 */

let RULES ={
    SUBJECT:{
        USER:"User",
        ASSET:"Asset",
        INSPECTION:"Inspection",
        SERVICE:"Service",
        SETTINGS:"Settings",
        READ_ROLE:"read_role",
        WRITE_ROLE:"write_role",
        DELETE_ROLE:"delete_role",
        UPDATE_ROLE:"update_role"
    },
    ACTION:{
        READ:"read",
        WRITE:"write",
        UPDATE:"update",
        DELETE:"delete",
        MANAGE:"manage",
        ANALYTICS:"analytics",
        DOWNLOAD:"download",
        LIVE:"live"
    }
};

module.exports ={
    RULES:RULES
};