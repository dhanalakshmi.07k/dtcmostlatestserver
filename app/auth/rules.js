/**
 * Created by suhas on 8/2/19.
 */
const { AbilityBuilder, Ability } = require('casl');
let _  =require("lodash");
let constants  =require("../../config/constants");
let ROLES_DEFINED = constants.AUTHENTICATION.ROLES;
let abilityDao = require("../dao/roles");
function defineAbilitiesFor(user) {
    return new Promise(function(resolve,reject){

        const { rules, can } = AbilityBuilder.extract();
        if (user) {
            if(user.roles && user.roles.length>0){
                let userRole = user.roles[0];
                abilityDao.getByRole(userRole).then(function(roleInfo){
                    //let parseRules = parseJSON(ability,null)
                   /* _.each(abilities,function(ability){
                        can(ability.subject,ability.actions)
                    });*/
                    resolve(new Ability(roleInfo.ability))
                });
                /**/
            }
        }else{
            resolve(new Ability(rules))
        }
    })
    /*const { rules, can } = AbilityBuilder.extract();
    if(_.includes(user.roles,ROLES_DEFINED.SUPER_ADMIN)){
        can('read_user_role',[ROLES_DEFINED.ADMIN,ROLES_DEFINED.OPERATOR]);
        can('create_user_role',[ROLES_DEFINED.ADMIN,ROLES_DEFINED.OPERATOR]);
        can('write',"User");
        can('read',"User");
    }else if(_.includes(user.roles,ROLES_DEFINED.ADMIN)){
        can('read_user_role',[ROLES_DEFINED.OPERATOR]);
        can('create_user_role',[ROLES_DEFINED.OPERATOR]);
        can('add',"User");
        can('read',"User");
    }else if(_.includes(user.roles,ROLES_DEFINED.OPERATOR)){
    }

    return new Ability(rules)*/
}
function parseJSON(template, variables) {
    return JSON.parse(template, (key, rawValue) => {
        if (rawValue[0] !== '$') {
            return rawValue
        }

        const name = rawValue.slice(2, -1)
        const value = getByPath(variables, name)

        if (typeof value === 'undefined') {
            throw new ReferenceError(`Variable ${name} is not defined`)
        }

        return value
    })
}
const ANONYMOUS_ABILITY = defineAbilitiesFor(null)

module.exports = function createAbilities(req, res,user, next) {
    //req.ability = user.roles ? defineAbilitiesFor(user) : ANONYMOUS_ABILITY;
    defineAbilitiesFor(user).then(function(ability){
        req.ability = ability;
        req.myProfile = user;
        next()
    })
}