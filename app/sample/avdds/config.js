/**
 * Created by suhas on 24/1/19.
 */
let path = require('path');
let config = {
    dtc_binary: {
            imageBasePath:path.join(__dirname, 'images/avdds/'),
            host:"192.168.50.51",
            port:"8090",
            allowAccess:true

    },
    local: {
            imageBasePath:path.join(__dirname, 'images/avdds/'),
            host:"192.168.50.51",/*19890:192.168.0.198:8090*/
            port:"8090",
            allowAccess:true
    },
};

module.exports = {
    config:config.local
};