/**
 * Created by suhas on 24/1/19.
 */

let Q = require("q");
let _ = require("lodash")
let http = require("http");
let fs = require('fs');
const fse = require('fs-extra');
let parser = require('xml2json');
let eventNumber = process.env.Event_no;
let sampleData = {"image":{}};
let objectLength = 0;
var options = {
    "method": "POST",
    "hostname": "localhost",
    "port": "19890",
    "path": "/",
    "headers": {
        "content-type": "application/xml",
        "cache-control": "no-cache",
    }
};
function retrieve(eventNo){

    let promises = [];
    let image = [{type: "uvis", string: ""}, {type: "front", string: ""}, {type: "plate", string: ""}]
    try{
        let array = [1,2,3,5,7,9,11];
        for (let i = 0; i < array.length; i++) {
            let cameraId = array[i];
            let promise= newRequest(eventNo,cameraId).then(function (data) {
                return Q(data);
            });
            promises.push(promise);
        }

        Q.all(promises).then(function (imagePath) {
            let groupByCameraName = _.groupBy(imagePath,"camera.name");
            let cameraNames = _.keys(_.groupBy(imagePath,"camera.name"));
            let data ={eventNo:null,allImageInfo:{}};
            _.each(cameraNames,function(obj){
                /*let data = {
                 location:userUploadedFeedMessagesLocation,
                 fileName:imagePath,
                 camera:{
                 name:cameraName,
                 cameraId:cameraId
                 },data:imageData,eventNo:eventNo
                 }*/
                let imageInfo = groupByCameraName[obj][0];
                data.eventNo = imageInfo.eventNo;
                delete imageInfo.location;
                delete imageInfo.data;
                data.allImageInfo[obj]=imageInfo
            });

            console.log("Received All image And Saved To local For Avdds of event NO "+data.eventNo);
            /*mapImageLocationToAvddsData(data)*/
        }).catch(function (err) {
            console.log(err.message)
        });
    }
    catch(e){
        logger.log({level:"error",message:e.message});
        console.log(e.message)
    }
}
function newRequest(eventNo,cameraId){
    return new Promise(function(resolve,reject){
        try{

            let req = http.request(options, function (res) {
                let chunks = [];
                res.on("error", function (chunk) {
                    //console.log(chunk.length)
                    //console.log(chunk[8]+'')
                    throw "Parameter is not a number!";
                });
                res.on("data", function (chunk) {
                    //console.log(chunk.length)
                    //console.log(chunk[8]+'')
                    chunks.push(chunk);
                });

                res.on("end", function () {
                    let body = Buffer.concat(chunks);

                    let currentReadLocation=0
                    let endReadLocation = 8

                    //Read XML length
                    currentReadLocation += endReadLocation
                    endReadLocation += 16
                    let xmlLength = parseInt(body.slice(currentReadLocation,endReadLocation));

                    //Read XML data
                    currentReadLocation = endReadLocation
                    endReadLocation  += xmlLength
                    let xmlData = body.slice(currentReadLocation,endReadLocation).toString()

                    //Read XML name of the image
                    currentReadLocation = endReadLocation
                    endReadLocation  += 16
                    let imageName = body.slice(currentReadLocation,endReadLocation).toString()
                    /*console.log(`imageName = ${imageName}`);*/

                    //Read image size
                    currentReadLocation = endReadLocation
                    endReadLocation  += 8
                    let imageNumber = body.slice(currentReadLocation,endReadLocation).toString()
                    let imageIntNumber = parseInt(imageNumber)
                    /* console.log(`imageNumber = ${imageNumber}`)
                     console.log(`imageIntNumber = ${imageIntNumber}`)*/

                    //read actual image
                    currentReadLocation = endReadLocation
                    endReadLocation  += imageIntNumber
                    let imageData = body.slice(currentReadLocation,endReadLocation);
                    let xmlDataVal = JSON.parse(parser.toJson(xmlData));
                    let dateObj = new Date();
                    let month = dateObj.getUTCMonth() + 1; //months from 1-12
                    let day = dateObj.getUTCDate();
                    let year = dateObj.getUTCFullYear();
                    let cameraName = xmlDataVal.imagereply.camera.name;
                    let imagePath = year+"/"+month+"/"+day+"/"+eventNo+"/"+cameraId+"/"+"image.jpeg";
                    let userUploadedFeedMessagesLocation = __dirname;
                    let data = {
                        location:userUploadedFeedMessagesLocation,
                        fileName:imagePath,
                        camera:{
                            name:cameraName,
                            cameraId:cameraId
                        },data:imageData,eventNo:eventNo
                    };
                    objectLength = Object.keys(sampleData.image).length;
                    sampleData.image[cameraName]={
                        "fileName":imagePath,
                        "camera": {
                            "name": "Front LPR",
                            "cameraId": 1
                        },
                        eventNo:eventNo
                    };
                    saveToNewFile(data).then(function(response){
                        response.data = null;
                        if(objectLength===6){
                            console.log(sampleData.image);
                        }
                        resolve(response)
                    });
                    //mapImageLocationToAvddsData(imagePath,eventNo,xmlDataVal);
                    //console.log("The file was saved!");
                });

            });
            req.write(`
      <?xml version="1.0" encoding="iso-8859-1"?>
      <imagerequest>
        <ovt>avdds</ovt>
        <eventnumber>${eventNo}</eventnumber>
        <cameraid>${cameraId}</cameraid>
        <imagenumber></imagenumber>
      </imagerequest>
  `);
            req.end();
        }
        catch(e){
            throw "Parameter is not a number!";
        }
    });
}

let saveToNewFile = function (info) {
    return new Promise(function(resolve,reject){
        //console.log(info.fileName)
        fse.outputFile(info.fileName,info.data, err => {
            if (err) {
                throw err
            }else{
                resolve(info)
            }
        })
    })
};




if(eventNumber){
    retrieve(parseInt(eventNumber));
}

