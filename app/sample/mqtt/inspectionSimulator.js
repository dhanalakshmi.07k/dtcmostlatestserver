/**
 * Created by suhas on 16/11/18.
 */


let sample = [
    {
        rfId:"000000000000000000000142",
        baeacon:["c28a8ccc4a40","d7efa24a9a1f","dfe00afdd7a8","da1ef13bc089","c5f91f1fd997"]},
    {
        rfId:"000000000000000000000038",
        baeacon:["eaeb682b7604","fa77c61447b0","c61eb2940b48","eb5de91cc844","f8070d219ca1"]},
    {
        rfId:"000000000000000000000492",
        baeacon:["d53a0ceccd7c","c113bb601689","f4dd6697aa9a","cc68aff2353e","c5aee255972a"]},
]

let mqttBroker212 = 'mqtt://212.47.249.75';
let mqttBrokerLocal = 'mqtt://localhost';
let mqtt = require('mqtt');
client = mqtt.connect(mqttBroker212);
client.on('connect', function () {
    //publish();
});
function publish(data) {
    console.log(data);
    client.publish("beaconPresence",JSON.stringify(data));
    count =count+1;
}
let carCount = 0;
let beaconCount = 0;
let beaconIntervalId = null;
function startPushingCar(){
    let RfId = sample[carCount].rfId;
    client.publish("rfIdPresence",RfId);

    beaconCount = 0;
    if(beaconIntervalId){
        clearInterval(beaconIntervalId)
    }
    console.log("car"+RfId)
    beaconIntervalId = setInterval(function(){
        if(sample[carCount].baeacon[beaconCount]){

            console.log("beacon"+sample[carCount].baeacon[beaconCount])
            let data = { uuid: sample[carCount].baeacon[beaconCount],
                rssi: -65,
                state: 'disconnected',
                address: '30:3a:64:b2:f2:f9',
                beaconId: sample[carCount].baeacon[beaconCount] };
            publishBeacon(data);
        }
    },1000*5);
}
function  publishBeacon(data) {
    beaconCount = beaconCount+1;
    client.publish("beaconPresence",JSON.stringify(data));
}
startPushingCar(sample[carCount]);
setInterval(function(){
    if(sample[carCount]){
        carCount=carCount+1;
        startPushingCar(sample[carCount]);
    }
},1000*60*2);