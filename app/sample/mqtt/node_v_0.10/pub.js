var spawn = require('child_process').spawn;
var zmq = require('zmq')
    , sock = zmq.socket('push');

var interval =900; //in ms
var command = '/home/debian/mercuryapi-1.29.0.63/c/src/api/read';
var commandParm =['tmr://localhost','--ant','1'];
var host = '52.207.160.1';
var port = '52001';
var pubPort = 'rfData';

sock.connect("tcp://"+host+":"+port);

console.log("sending Data to Host="+ host +",  port="+ port + ", command=" + command +", commandParm="
    + commandParm +" @interval = " + interval);

setInterval(function(){
    getNewData()
}, interval);


var getNewData = function(){
    var cmd = spawn(command,commandParm);

    cmd.stdout.on('data', function(data){
        console.log("new Rf Read : "+ data );
        //sock.send(['rfData', data]);
        sock.send(data);
    });

    cmd.stderr.on('data', function(data){
        console.log("stderr: " + data);
    });

    cmd.on('close', function(code){
        console.log("child process exited with code "+ code);
    });
};
