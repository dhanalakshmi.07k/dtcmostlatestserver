/**
 * Created by suhas on 13/8/18.
 *//*31313*/
let constant = require("../../../config/constants");
let mqttBroker212 = 'mqtt://212.47.249.75';
let mqttBrokerLocal = 'mqtt://localhost';
let obd = {
    sample1:{
    carInfo:{
        "rpm": 0,
        "speed": "",
        "maf": 0,
        "obd_mac": "",
        "dtc_s": "",
        "dtc_p": "",
        "eng_ld": "",
        "cool_tmp": "",
        "fuel_tnk": "",
        "fuel_lvl": "",
        "eng_runt": "",
        "battery_remain": "",
    },
    "vin": "1G1JC5444R7252367",
},
    sample2:{
    carInfo:{
        "rpm": 0,
        "speed": "",
        "maf": 0,
        "obd_mac": "",
        "dtc_s": "",
        "dtc_p": "",
        "eng_ld": "",
        "cool_tmp": "",
        "fuel_tnk": "",
        "fuel_lvl": "",
        "eng_runt": "",
        "battery_remain": "",
    },
    "vin": "1G1JC5444R7252367",
}};
let mqtt = require('mqtt');
client = mqtt.connect(mqttBroker212);
client.on('connect', function () {
});
function publish(data) {
    console.log(data);
    client.publish(constant.SUBSCRIBER.MQTT.TOPIC.OBD_DEVICE, JSON.stringify(data));
}
publish(obd.sample1);