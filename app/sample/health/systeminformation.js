/**
 * Created by suhas on 11/12/18.
 */
const si = require('systeminformation');

async function systemStats() {
        try {
            const cpu = await si.cpu();
            const mem = await si.mem();
            const osInfo = await si.osInfo();
            const fsSize = await si.fsSize();
            const fullLoad = await si.fullLoad();
            const currentLoad = await si.currentLoad();
            const temperature = await si.cpuTemperature();
            const fsStats = await  si.fsStats();
            return {cpu:cpu, mem:mem, osInfo:osInfo, fsSize:fsSize, fullLoad:fullLoad, currentLoad:currentLoad, temperature:temperature, fsStats:fsStats}
        }
        catch (e) {
            console.log(e);
        }
}
module.exports={
    systemStats:systemStats
};
