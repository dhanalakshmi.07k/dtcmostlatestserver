const licenseFile = require('nodejs-license-file');

try {
  const template = [
    '====BEGIN LICENSE====',
    '{{&licenseVersion}}',
    '{{&applicationVersion}}',
    '{{&firstName}}',
    '{{&lastName}}',
    '{{&email}}',
    '{{&expirationDate}}',
    '{{&serial}}',
    '=====END LICENSE====='
  ].join('\n');

  const data = licenseFile.parse({
    publicKeyPath: './public_key.pem',
    licenseFilePath: './licence.lic',
    template
  });

  console.log(data);

} catch (err) {

  console.log(err);
}