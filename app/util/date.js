/**
 * Created by suhas on 26/9/18.
 */
let moment = require('moment');
let isValidDate = function (str) {
    let d = moment(str,'DD/MM/YYYY');
    if(d === null || !d.isValid()) return false;
    return str.indexOf(d.format('D/M/YYYY')) >= 0
        || str.indexOf(d.format('DD/MM/YYYY')) >= 0
        || str.indexOf(d.format('D/M/YY')) >= 0
        || str.indexOf(d.format('DD/MM/YY')) >= 0;
};

module.exports = {
    isValidDate:isValidDate
};