let express = require('express'),
    config = require('./config/config'),
    constant = require('./config/constants'),
    glob = require('glob'),
    mongoose = require('mongoose'),
    db = mongoose.connection,
    logger = require("./config/logger"),
    fs = require('fs');
    mongoose.connect(config.mongoConn.url,config.mongoConn.options,function(err,conn){
        logger.logger.log({ level: 'error', message: "Mongo DB Connected To "+err});
        logger.logger.log({ level: 'debug', message: "Mongo DB Connected To "+config.mongoConn.url});
    });

    let app = express();
    require('./config/express')(app);
    db.on('error', function (err){
        logger.logger.log({ level: 'error', message: "Mongo DB Connected ERROR "+err});
        throw new Error('unable to connect to database at ' +config.db);
    });
    let mqttTopicsToBeSubscribed = [constant.SUBSCRIBER.MQTT.TOPIC.BEACON,
        constant.SUBSCRIBER.MQTT.TOPIC.RF_ID_TAG,
        constant.SUBSCRIBER.MQTT.TOPIC.OBD_DEVICE,
        constant.SUBSCRIBER.MQTT.TOPIC.HEALTH_CHECK,
        constant.SUBSCRIBER.MQTT.TOPIC.HEALTH_CHECK_MANAGE,
        constant.SUBSCRIBER.MQTT.TOPIC.DRIVER_FEEDBACK,
        constant.SUBSCRIBER.MQTT.TOPIC.UVIS,
        constant.SUBSCRIBER.MQTT.TOPIC.AVDDS,
        constant.SUBSCRIBER.MQTT.TOPIC.OBD_DEVICE_GPS,
        constant.SUBSCRIBER.MQTT.TOPIC.FREEMATRICS_OBD_DEVICE
    ];
    require('./app/subscribe/mqtt').subscriber(config.mqtt.broker,mqttTopicsToBeSubscribed);
    require('./app/publisher/mqtt').connect(config.mqtt.broker);
    //logger("Application Started");
    logger.logger.log({ level: 'info', message: 'APPLICATION STARTED' });

if (!fs.existsSync(config.bulkUploadFileStorage.path)){
    console.log("DIRECTORY NOT FOUND "+config.bulkUploadFileStorage.path);
    console.log("CREATING");
    fs.mkdirSync(config.bulkUploadFileStorage.path);
}
    module.exports = app;